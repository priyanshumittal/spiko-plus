=== Spiko Plus ===

Contributors: spicethemes
Requires at least: 4.5
Tested up to: 6.7.1
Stable tag: 1.3.2
Requires PHP: 5.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Enhance Spiko WordPress Themes functionality.

== Description ==

Spiko WordPress Theme is lightweight, elegant, fully responsive and translation-ready theme that allows you to create stunning blogs and websites. The theme is well suited for companies, law firms,ecommerce, finance, agency, travel, photography, recipes, design, arts, personal and any other creative websites and blogs. Theme is developed using Bootstrap 4 framework. It comes with a predesigned home page, good looking header designs and number of content sections that you can easily customize. It also has lots of customization options (banner, services, testimonial, etc) that will help you create a beautiful, unique website in no time. Spiko is  compatible with popular plugins like WPML, Polylang, Woo-commerce  and Conact Form 7.This  theme is fully GDPR-compliant and doesn’t use any external Google Fonts. All fonts are locally hosted. Spiko theme comes with various popular locales.(DEMO: https://spiko-pro.spicethemes.com) 


== Changelog ==

@Version 1.3.2
* Updated freemius directory.

@Version 1.3.1
* Updated font-awesome library and freemius directory.

@Version 1.3
* Updated freemius directory.

@Version 1.2.1
* Added  google font locally feature.

@Version 1.2
* Added freemius directory

@Version 1.1
* Added banner image setting for the shop page. 
* Fixed preloader, portfolio, escaping and some style issues.
* Removed unnecessary code in our plugin.

@Version 1.0
* Added One Click Demo Import feature.
* Removed Google Plus social icon.

@Version 0.2
* Fixed the issues with PHP 8.

@Version 0.1
* release

== External resources ==

Owl Carousel: 
Copyright: (c) David Deutsch
License: MIT license
Source: https://cdnjs.com/libraries/OwlCarousel2/2.2.1

Alpha color picker Control:
Copyright: (c) 2016 Codeinwp cristian-ungureanu
License: MIT License
Source: https://github.com/Codeinwp/customizer-controls/tree/master/customizer-alpha-color-picker

Repeater Control:
Copyright: (c) 2016 Codeinwp cristian-ungureanu
License: MIT license
Source: https://github.com/Codeinwp/customizer-controls/tree/master/customizer-repeater

Custom control - Image Radio Button Custom Control:
Copyright: Anthony Hortin
License: GNU General Public License v2 or later
Source: https://github.com/maddisondesigns/customizer-custom-controls

* Images on /images folder
Copyright (C) 2024, spicethemes and available as [GPLv2](https://www.gnu.org/licenses/gpl-2.0.html)