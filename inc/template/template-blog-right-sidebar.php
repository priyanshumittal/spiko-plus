<?php
/**
 * Template Name: Blog right sidebar
 */
get_header();
?>
<section class="section-space blog bg-default">
    <div class="container<?php echo esc_html(spiko_blog_post_container());?>">
        <div class="row">
            <div class="col-lg-8 col-md-7 col-sm-12">	
                <?php
                if (get_theme_mod('post_nav_style_setting', 'pagination') == 'pagination') {
                    if (get_query_var('paged')) {
                        $paged = get_query_var('paged');
                    } elseif (get_query_var('page')) {
                        $paged = get_query_var('page');
                    } else {
                        $paged = 1;
                    }
                    $args = array('post_type' => 'post', 'paged' => $paged);
                    $loop = new WP_Query($args);
                    if ($loop->have_posts()):
                        while ($loop->have_posts()): $loop->the_post();
                            include(SPIKOP_PLUGIN_DIR.'/inc/template-parts/content-blog-template.php');
                        endwhile;
                     else:
                        get_template_part('template-parts/content','none');
                    endif;
                    // pagination function
                    echo '<div class="row justify-content-center">';
                    $obj = new spiko_plus_pagination();
                    $obj->spiko_plus_page($loop);
                    echo '</div>';
                } else {
                    echo do_shortcode('[ajax_posts]');
                }
                ?>		
            </div>	

            <div class="col-lg-4 col-md-5 col-sm-12">
                <div class="sidebar">
                    <div class="right-sidebar">
<?php dynamic_sidebar('sidebar-1'); ?>
                    </div>								
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>