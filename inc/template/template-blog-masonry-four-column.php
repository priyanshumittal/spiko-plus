<?php
/**
 *  Template Name: Blog Masonry 4 Column
 */
get_header();
$exc_length = get_theme_mod('spiko_blog_content', 'excerpt'); ?>
<section class="section-space blog bg-default blog-masonry <?php
if ($exc_length != 'excerpt') {
    echo 'fourmasonary-full-width';
}
?>">
    <div class="container<?php echo esc_html(spiko_blog_post_container());?>">
        <?php
        if (get_theme_mod('post_nav_style_setting', 'pagination') == "pagination") {
            ?>
            <div class="grid">
                <?php
                if (get_query_var('paged')) {
                    $paged = get_query_var('paged');
                } elseif (get_query_var('page')) {
                    $paged = get_query_var('page');
                } else {
                    $paged = 1;
                }
                $args = array('post_type' => 'post', 'paged' => $paged);
                $loop = new WP_Query($args);
                if ($loop->have_posts()):
                    while ($loop->have_posts()): $loop->the_post();
                        ?>
                        <div class="grid-item col-md-3">
                            <?php include(SPIKOP_PLUGIN_DIR.'/inc/template-parts/content-blog-template.php');
                 ?>
                        </div>
                        <?php
                    endwhile;
                endif;
                ?>
            </div>
            <!-- pagination function -->
            <div class="row justify-content-center">
                <?php
                echo '<div class="row justify-content-center">';
                $obj = new spiko_plus_pagination();
                $obj->spiko_plus_page($loop);
                echo '</div>';
                ?>
            </div>
            <?php
        }
        if (get_theme_mod('post_nav_style_setting', 'pagination') != "pagination") {
            echo do_shortcode('[ajax_posts]');
        }
        ?>
    </div>
</section>	
<?php get_footer(); ?>