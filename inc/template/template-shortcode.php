<?php
/**
 * Template Name: Shortcode Page
 *
 * @package astika
 */
get_header(); 
// show shortcode
the_post();
the_content();

get_footer();?>