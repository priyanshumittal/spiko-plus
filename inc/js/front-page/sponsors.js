//RLT check function
if (!jQuery.bol_return) {
    jQuery.extend({
        bol_return: function (tmp_vl) {
            if (tmp_vl == 1) {
                return true;
            }
            return false;
        }
    });
}

jQuery(document).ready(function () {
    jQuery("#clients-carousel").owlCarousel({
        rtl:sponsorssettings.rtl,
        navigation: false, // Show next and prev buttons		
        autoplay: true,
        autoplayTimeout: 2000,
        autoplayHoverPause: true,
        smartSpeed: 700,

        loop: true, // loop is true up to 1199px screen.
        nav: false, // is true across all sizes
        margin: 30, // margin 10px till 960 breakpoint

        responsiveClass: true, // Optional helper class. Add 'owl-reponsive-' + 'breakpoint' class to main element.
        items: sponsorssettings.client_items,
        dots: true,
        navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
        responsive: {
            100: {items: 1},
            500: {items: 2},
            768: {items: 3},
            1000: {items: sponsorssettings.client_items}
        },
        rtl: jQuery.bol_return(sponsorssettings.rtl)
    });

});