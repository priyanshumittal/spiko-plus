// OWL SLIDER CUSTOM JS

jQuery(document).ready(function () {

    /* Preloader */
  jQuery(window).on('load', function() {
     setTimeout(function(){
        jQuery('body').addClass('loaded');
      }, 1500);
  }); 


    /* ---------------------------------------------- /*
     * Home section height
     /* ---------------------------------------------- */

      jQuery(".search-icon").click(function(e){
            e.preventDefault();
            //console.log();
            jQuery('.header-logo .search-box-outer ul.dropdown-menu').toggle('addSerchBox');
         });
    function buildHomeSection(homeSection) {
        if (homeSection.length > 0) {
            if (homeSection.hasClass('home-full-height')) {
                homeSection.height(jQuery(window).height());
            } else {
                homeSection.height(jQuery(window).height() * 0.85);
            }
        }
    }


    /* ---------------------------------------------- /*
     * Scroll top
     /* ---------------------------------------------- */

    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() > 100) {
            jQuery('.scroll-up').fadeIn();
        } else {
            jQuery('.scroll-up').fadeOut();
        }
    });

    jQuery('a[href="#totop"]').click(function () {
        jQuery('html, body').animate({scrollTop: 0}, 'slow');
        return false;
    });

    // Tooltip Js
    jQuery(function () {
        jQuery('[data-toggle="tooltip"]').tooltip()
    });

    // Accodian Js
    function toggleIcon(e) {
        jQuery(e.target)
                .prev('.panel-heading')
                .find(".more-less")
                .toggleClass('fa-plus-square-o fa-minus-square-o');
    }
    jQuery('.panel-group').on('hidden.bs.collapse', toggleIcon);
    jQuery('.panel-group').on('shown.bs.collapse', toggleIcon);

    jQuery('.grid').masonry({
        itemSelector: '.grid-item',
        transitionDuration: '0.2s',
        horizontalOrder: true,
    });


    //Homepage Blog Slider
   
    
});

 (function ($){

  // remove box on click 
    $("a").keypress(function() {
     this.blur();
     this.hideFocus = false;
     this.style.outline = null;
      });
      $("a").mousedown(function() {
           this.blur();
           this.hideFocus = true;
           this.style.outline = 'none';
      });
      
      $.fn.counter = function() {
        const $this = $(this),
        numberFrom = parseInt($this.attr('data-from')),
        numberTo = parseInt($this.attr('data-to')),
        delta = numberTo - numberFrom,
        deltaPositive = delta > 0 ? 1 : 0,
        time = parseInt($this.attr('data-time')),
        changeTime = 10;
        
        let currentNumber = numberFrom,
        value = delta*changeTime/time;
        var interval1;
        const changeNumber = () => {
          currentNumber += value;
          
          (deltaPositive && currentNumber >= numberTo) || (!deltaPositive &&currentNumber<= numberTo) ? currentNumber=numberTo : currentNumber;
          this.text(parseInt(currentNumber));
          currentNumber == numberTo ? clearInterval(interval1) : currentNumber;  
        }

        interval1 = setInterval(changeNumber,changeTime);
      }
         }(jQuery));
       /* var a=0; 
        jQuery(window).scroll(function() {
         var oTop = jQuery('.funfact-inner').offset().top - window.innerHeight;
           if (a == 0 && jQuery(window).scrollTop() > oTop) { 
          jQuery('.count1').counter();
           jQuery('.count2').counter();
            jQuery('.count3').counter();
             jQuery('.count4').counter();
        
        setTimeout(function () {
          jQuery('.count5').counter();
        }, 3000);
         a = 1;
            }  
        });*/


  jQuery(document).ready(function(){

    jQuery('.count-up').counter();
    
    /*new WOW().init();*/
    
    setTimeout(function () {
      jQuery('.count5').counter();
    }, 3000);
  });