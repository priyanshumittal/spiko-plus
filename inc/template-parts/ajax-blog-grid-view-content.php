<div class="col-lg-4 col-md-6 col-sm-12">
    <article class="post"  id="post-<?php the_ID(); ?>" <?php post_class(); ?>> 
    <?php
    if(has_post_thumbnail()):?>
    <figure class="post-thumbnail">
        <?php the_post_thumbnail('full',array('class'=>'img-fluid','alt'=>'blog-image'));?>         
    </figure>   
    <?php endif;?>  
    <div class="post-content">
        <?php if(get_theme_mod('spiko_enable_blog_date',true) || get_theme_mod('spiko_enable_blog_author',true) || get_theme_mod('spiko_enable_blog_category',true) || get_theme_mod('spiko_enable_blog_tag',true)) { ?>
        <?php if(get_theme_mod('spiko_enable_blog_date',true)==true):?>
        <?php if(has_post_thumbnail()) { ?> <div class="entry-date"><span class="date"><?php echo esc_html(get_the_date()); ?></span><?php }
    else{ ?><div class="remove-image"><span class="date"><?php echo esc_html(get_the_date()); ?></span> <?php  }?></div>
        <?php endif;?>
        <?php if(get_theme_mod('spiko_enable_blog_author',true) || get_theme_mod('spiko_enable_blog_category',true) || get_theme_mod('spiko_enable_blog_tag',true)){ ?>
        <div class="entry-meta"> 
            <?php 
            $spiko_blog_meta_sort=get_theme_mod( 'spiko_blog_meta_sort', array('blog_author','blog_category','blog_tag'));       
            if ( ! empty( $spiko_blog_meta_sort ) && is_array( $spiko_blog_meta_sort ) ) :
                foreach ( $spiko_blog_meta_sort as $spiko_blog_meta_sort_key => $spiko_blog_meta_sort_val ) :
                    if(get_theme_mod('spiko_enable_blog_author',true)==true):
                        if ( 'blog_author' === $spiko_blog_meta_sort_val ) :?>
            <a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>" alt="tag"><i class="fa fa-user"></i><span class="author"><?php echo get_the_author();?></span></a>
            <?php endif;
                endif;
            if(get_theme_mod('spiko_enable_blog_category',true)==true):
                    if ( 'blog_category' === $spiko_blog_meta_sort_val ) :
                        if ( has_category() ) : 
                        echo '<i class="fa fa-folder-open"></i><span class="cat-links" alt="Categories">';
                        the_category( ', ' );
                        echo '</span>';
                        endif;
                    endif;
                endif;
            if (get_theme_mod('spiko_enable_blog_tag', true) == true):
                    if ( 'blog_tag' === $spiko_blog_meta_sort_val ) :                       
                    $tag_list = get_the_tag_list();
                        if (!empty($tag_list)) {
                            ?>
                            <i class="fa fa-tag"></i>
                            <span class="cat-links posttag"><?php the_tags('', ', ', ''); ?></span>
                        <?php } 
                    endif;
                endif;
            endforeach;
        endif;?> 
        </div>
        <?php } 
     } ?>
        <header class="entry-header">
            <h4 class="entry-title">
                <a class="rm-h4" href="<?php the_permalink();?>"><?php the_title();?></a>
            </h4> 
        </header>
        <div class="entry-content">
        <?php spiko_posted_content();?>
         <?php
         $button_show_hide=get_theme_mod('spiko_blog_content','excerpt');
         if($button_show_hide=="excerpt")
         {
         if(get_theme_mod('spiko_enable_blog_read_button',true)==true):
         spiko_button_title();
         endif;
        } ?>
    </div>
    </div>
</article>
</div>