<?php
/**
 * Shortcode Articles Template
 */
$customizer_url = admin_url() . 'customize.php' ;
?>

<div id="shortcode_articles" class="spiko-plus-tab-pane active" style="display: none;">
	<div class="container-fluid">			
		<div class="row" style="margin-top: 20px; margin-bottom: 20px;">
			<h3 class="spiko-plus-info-title text-center">Shortcodes</h3>
			<div class="col-md-4" style="padding-left: 50px;">

				<h4>Service Section Shortcode</h4>
				<h4><b><?php echo esc_html__('[spiko_service]','spiko-plus'); ?></b></h4><br>

				<h4>CTA Section Shortcode</h4>
				<h4><b><?php echo esc_html__('[spiko_cta]','spiko-plus'); ?></b></h4><br>
				
				<h4>Portfolio Section Shortcode</h4>
				<h4><b><?php echo esc_html__('[spiko_portfolio]','spiko-plus'); ?></b></h4><br>

				<h4>Testimonial Section Shortcode</h4>
				<h4><b><?php echo esc_html__('[spiko_testimonial]','spiko-plus'); ?></b></h4><br>

				<h4>Team Section Shortcode</h4>
				<h4><b><?php echo esc_html__('[spiko_team]','spiko-plus'); ?></b></h4><br>

				<h4>Funfact Section Shortcode</h4>
				<h4><b><?php echo esc_html__('[spiko_funfact]','spiko-plus'); ?></b></h4><br>

				<h4>Client Section Shortcode</h4>
				<h4><b><?php echo esc_html__('[spiko_client]','spiko-plus'); ?></b></h4><br>

				<h4>Contact Section Shortcode</h4>
				<h4><b><?php echo esc_html__('[spiko_contact]','spiko-plus'); ?></b></h4><br>		
				
			</div>
			<div class="col-lg-8">
				<img src="<?php echo SPIKOP_PLUGIN_URL.'/inc/admin/assets/img/shortcode-image.png';?>"  width="100%">
			</div>
		</div>
		<div class="row" style="margin: 20px;">
			<div style="background-color: #d4edda; padding:10px;">
					<h4><a href="https://helpdoc.spicethemes.com/spiko-plus/how-to-use-shortcodes-in-spiko-plus/" target="_blank"><?php echo esc_html__('Click Here','spiko-plus');?></a> To know about the attributes with the shortcodes.</h4>
			</div>
		</div>
	</div>
</div>	