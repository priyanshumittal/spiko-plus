<?php
/**
 * Getting Started Template
 */
?>
<div id="getting_started" class="spiko-plus-tab-pane active">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<h1 class="spiko-plus-info-title text-center"><?php echo esc_html__('Spiko Plus Configuration','spiko-plus'); ?><?php if( !empty($spiko['Version']) ): ?> <sup id="spiko-plus-theme-version"><?php echo esc_html( $spiko['Version'] ); ?> </sup><?php endif; ?></h1>
			</div>
		</div>
		<div class="row">
			
			<div class="col-md-12">			
			    <div class="spiko-plus-page">
			    	<div class="mockup">
			    		<img src="<?php echo SPIKOP_PLUGIN_URL.'/inc/admin/assets/img/mockup-pro.png';?>"  width="100%">
			    	</div>				
				</div>	
			</div>	

		</div>

		<div class="row" style="margin-top: 20px;">			

			<div class="col-md-6">
				<div class="spiko-plus-page">
					<div class="spiko-plus-page-top"><?php esc_html_e( 'Links to Customizer Settings', 'spiko-plus' ); ?></div>
					<div class="spiko-plus-page-content">
						<ul class="spiko-plus-page-list-flex">
							<li class="">
								<a class="spiko-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[section]=title_tagline' ) ); ?>" target="_blank"><?php esc_html_e('Site Logo','spiko-plus'); ?></a>
							</li>
							<li class="">
								<a class="spiko-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=spiko_theme_panel' ) ); ?>" target="_blank"><?php esc_html_e('Blog options','spiko-plus'); ?></a>
							</li>
							<li class="">
								<a class="spiko-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=nav_menus' ) ); ?>" target="_blank"><?php esc_html_e('Menus','spiko-plus'); ?></a>
							</li>
							<li class="">
								<a class="spiko-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[section]=theme_style' ) ); ?>" target="_blank"><?php esc_html_e('Layout & Color scheme','spiko-plus'); ?></a>
							</li>
							<li class="">
								<a class="spiko-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=widgets' ) ); ?>" target="_blank"><?php esc_html_e('Widgets','spiko-plus'); ?></a>
							</li>
							<li class="">
								<a class="spiko-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=spiko_general_settings' ) ); ?>" target="_blank"><?php esc_html_e('General settings','spiko-plus'); ?></a><?php esc_html_e(' ( Preloader, After Menu, Header Presets, Sticky Header, Container settings, Post Navigation Styles) ','spiko-plus' ); ?>
							</li>
                            <li class="">
								<a class="spiko-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=section_settings' ) ); ?>" target="_blank"><?php esc_html_e('Homepage sections','spiko-plus'); ?></a>
							</li>
                            <li class="">
								<a class="spiko-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=spiko_template_settings' ) ); ?>" target="_blank"><?php esc_html_e('Page template settings','spiko-plus'); ?></a>
							</li>
			
							<li class="">
								<a class="spiko-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=spiko_typography_setting' ) ); ?>" target="_blank"><?php esc_html_e('Typography','spiko-plus'); ?></a>
							</li>
							<li class="">
								<a class="spiko-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'themes.php?page=spiko_plus_Hooks_Settings' ) ); ?>" target="_blank"><?php esc_html_e('Hooks','spiko-plus'); ?></a>
							</li>
							
							<li class="">
								<a class="spiko-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[section]=frontpage_layout' ) ); ?>" target="_blank"><?php esc_html_e('Sections order manager','spiko-plus'); ?></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="spiko-plus-page">
					<div class="spiko-plus-page-top spiko-plus-demo-import"><?php esc_html_e( 'One Click Demo Import', 'spiko_plus' ); ?></div>
					<p style="padding:10px 20px; font-size: 16px;"><?php _e( 'To import the demo data, you need to activate the <b>One Click Demo Import</b> and <b>Spiko Demo Importer</b> plugins. <a class="spiko-plus-custom-class" href="#one_click_demo" target="_self">Click Here</a>', 'spiko_plus' ); ?></p>
				</div> 
				<div class="spiko-plus-page">
					<div class="spiko-plus-page-top"><?php esc_html_e( 'Useful Links', 'spiko-plus' ); ?></div>
					<div class="spiko-plus-page-content">
						<ul class="spiko-plus-page-list-flex">
							<li class="">
								<a class="spiko-plus-page-quick-setting-link" href="<?php echo 'https://spiko-pro.spicethemes.com/'; ?>" target="_blank"><?php echo esc_html__('Spiko Plus Demo','spiko-plus'); ?></a>
							</li>

							<li class="">
								<a class="spiko-plus-page-quick-setting-link" href="<?php echo 'https://spicethemes.com/spiko-plus'; ?>" target="_blank"><?php echo esc_html__('Spiko Plus Details','spiko-plus'); ?></a>
							</li>
							
							<li class="">
								<a class="spiko-plus-page-quick-setting-link" href="<?php echo 'https://support.spicethemes.com/index.php?p=/categories/spiko-pro'; ?>" target="_blank"><?php echo esc_html__('Spiko Plus Support','spiko-plus'); ?></a>
							</li>
							
						    <li class=""> 
								<a class="spiko-plus-page-quick-setting-link" href="<?php echo 'https://helpdoc.spicethemes.com/category/spiko-plus/'; ?>" target="_blank"><?php echo esc_html__('Spiko Plus Documentation','spiko-plus'); ?></a>
							</li>
							
						    <li> 
								<a class="spiko-page-quick-setting-link" href="<?php echo esc_url('https://spicethemes.com/spiko-free-vs-plus/'); ?>" target="_blank"><?php echo esc_html__('Free vs Plus','spiko-plus'); ?></a>
							</li> 

							<li> 
								<a class="spiko-page-quick-setting-link" href="<?php echo esc_url('https://spicethemes.com/spiko-plus-changelog/'); ?>" target="_blank"><?php echo esc_html__('Changelog','spiko-plus'); ?></a>
							</li> 
						</ul>
					</div>
				</div>
			</div>		
		</div>
	</div>
</div>	