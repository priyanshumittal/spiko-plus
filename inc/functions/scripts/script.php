<?php

function spiko_plus_enqueue_script() {
    $theme = wp_get_theme();
    $suffix = ( defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ) ? '' : '.min';
    wp_enqueue_style('animate', SPIKOP_PLUGIN_URL . '/inc/css/animate.css');
    wp_enqueue_style('owl', SPIKOP_PLUGIN_URL . '/inc/css/owl.carousel.css');
    wp_enqueue_style('lightbox', SPIKOP_PLUGIN_URL . '/inc/css/lightbox.css');
    if (get_theme_mod('custom_color_enable') == true) {
            add_action('wp_footer', 'spiko_plus_custom_light');
        } else {
             if('Spiko Dark' == $theme->name){
                $class = get_theme_mod('theme_color', 'ruddy-brown.css');
                wp_enqueue_style('spikop-dark-default', SPIKOP_PLUGIN_URL . '/inc/css/' . $class);
            }
            else{
                $class = get_theme_mod('theme_color', 'default.css');
                wp_enqueue_style('spikop-default', SPIKOP_PLUGIN_URL . 'inc/css/' . $class);
            }
        }
    require_once('custom_style.php');
    if(get_theme_mod('spiko_color_skin','light')=='dark')
    {
    wp_enqueue_style('customize-css', SPIKOP_PLUGIN_URL . '/inc/css/dark.css');
    }
    //js file
    wp_enqueue_script('owl', SPIKOP_PLUGIN_URL . '/inc/js/owl.carousel' . $suffix . '.js', array('jquery'), '', true);
    wp_enqueue_script('spiko-mp-masonry-js', SPIKOP_PLUGIN_URL . '/inc/js/masonry/mansory.js');
    wp_enqueue_script('grid-masonary', SPIKOP_PLUGIN_URL.'/inc/js/grid-mansory.js', array('jquery'), '', true);
    wp_enqueue_script('spiko-custom-js', SPIKOP_PLUGIN_URL . '/inc/js/custom.js', array('jquery'), '', true);
    wp_enqueue_script('spiko-main-js', SPIKOP_PLUGIN_URL . '/inc/js/main.js', array('jquery'), '', true);
    wp_enqueue_script('lightbox', SPIKOP_PLUGIN_URL . '/inc/js/lightbox/lightbox-2.6.min.js', array('jquery'), '', true);
    wp_enqueue_script('imgLoad', SPIKOP_PLUGIN_URL.'/inc/js/img-loaded.js', array('jquery'), '', true);
    //wp_enqueue_script('spiko-plus-video-slider-js', SPIKOP_PLUGIN_URL. '/inc/js/jquery.mb.YTPlayer.js');
    

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'spiko_plus_enqueue_script');

function spiko_plus_admin_enqueue_scripts() {
    wp_enqueue_style('customize-css', SPIKOP_PLUGIN_URL . '/inc/css/customize.css');
}

add_action('customize_controls_enqueue_scripts', 'spiko_plus_admin_enqueue_scripts');

/* slider heading */

function spiko_plus_sliderHeading() {
    $themeCor = get_theme_mod('theme_color', 'default.css');
    if (explode('.', $themeCor)[0] == 'default') {
        $clrsldr = '#22a2c4';
    } elseif (explode('.', $themeCor)[0] == 'green') {
        $clrsldr = '#82b440';
    } elseif (explode('.', $themeCor)[0] == 'red') {
        $clrsldr = '#ce1b28';
    } elseif (explode('.', $themeCor)[0] == 'purple') {
        $clrsldr = '#6974ea';
    } elseif (explode('.', $themeCor)[0] == 'orange') {
        $clrsldr = '#ee591f';
    } elseif (explode('.', $themeCor)[0] == 'yellow') {
        $clrsldr = '#ffba00';
    }
}

add_action('wp_head', 'spiko_plus_sliderHeading');
/* slider heading */

// slider custom script
function spiko_plus_add_theme_scripts() {
    $animation = get_theme_mod('animation', '');

    if ($animation == '') {
        $animate_In = '';
        $animate_Out = '';
    } else {
        $animate_In = 'fadeIn';
        $animate_Out = 'fadeOut';
    }
    $slider_autoplay = get_theme_mod('slider_autoplay', true);
    $slider_loop = get_theme_mod('slider_loop', true);
    $slider_rewind = get_theme_mod('slider_rewind', true);
    $animation_speed = get_theme_mod('animation_speed', 3000);
    $slider_smooth_speed = get_theme_mod('slider_smooth_speed', 1000);
    $slider_nav_style = get_theme_mod('slider_nav_style', 'navigation');
    $isRTL = (is_rtl()) ? (bool) true : (bool) false;
    $settings = array('animateIn' => $animate_In, 'animateOut' => $animate_Out, 'animationSpeed' => $animation_speed, 'smoothSpeed' => $slider_smooth_speed,'slideAutoplay'=>$slider_autoplay,'slideLoop'=>$slider_loop,'slideRewind'=>$slider_rewind, 'slider_nav_style' => $slider_nav_style, 'rtl' => $isRTL);

    wp_register_script('spiko-slider', SPIKOP_PLUGIN_URL . '/inc/js/front-page/slider.js', array('jquery'));
    wp_localize_script('spiko-slider', 'slider_settings', $settings);
    wp_enqueue_script('spiko-slider');
}

add_action('wp_enqueue_scripts', 'spiko_plus_add_theme_scripts');


add_action('wp_footer', 'spiko_plus_custom_script');

function spiko_plus_custom_script() {
    $col = 6;

    if (is_page_template('template/template-blog-masonry-two-column.php')) {

        $col = 6;
    } elseif (is_page_template('template/template-blog-masonry-three-column.php')) {

        $col = 4;
    } elseif (is_page_template('template/template-blog-masonry-four-column.php')) {

        $col = 3;
    }
 }   

//Load script at admin side
function spiko_plus_admin_scripts() {
 wp_enqueue_script( 'spiko-plus-admin-script', SPIKOP_PLUGIN_URL . 'inc/js/admin.js', array('jquery'));
}
add_action( 'customize_controls_enqueue_scripts', 'spiko_plus_admin_scripts');