<?php
//call the action for the news section
add_action('spiko_plus_news_action','spiko_plus_news_section');
//function for the news section
function spiko_plus_news_section()
{
$latest_news_section_enable = get_theme_mod('latest_news_section_enable', true);
if ($latest_news_section_enable != false) {
	$theme = wp_get_theme();
	if('Spiko Dark' == $theme->name) {
	    $sd_newz_design=get_theme_mod('home_news_design_layout', 3);
	}
	else{
	    $sd_newz_design=get_theme_mod('home_news_design_layout', 1);
	}
    $news_layout=$sd_newz_design;
	include_once(SPIKOP_PLUGIN_DIR.'/inc/inc/home-section/news-content'.$news_layout.'.php');
}
}