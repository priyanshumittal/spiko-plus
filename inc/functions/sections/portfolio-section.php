<?php
//call the action for the portfolio section
add_action('spiko_plus_portfolio_action','spiko_plus_portfolio_section');
//function for the portfolio section
function spiko_plus_portfolio_section()
{
$spiko_plus_home_project_enabled = get_theme_mod('portfolio_section_enable', true);
if ($spiko_plus_home_project_enabled != false) {
    $spiko_plus_project_section_title = get_theme_mod('home_portfolio_section_title', __('Look at our projects', 'spiko-plus'));
    $home_portfolio_section_subtitle = get_theme_mod('home_portfolio_section_subtitle', __('Our Work', 'spiko-plus'));
    $portfolio_col = get_theme_mod('home_portfolio_column_laouts', 4);
    $no_portfolio = get_theme_mod('home_portfolio_numbers_options', 3);
    $team_animation_speed = get_theme_mod('team_animation_speed', 3000);
    $team_smooth_speed = get_theme_mod('team_smooth_speed', 1000);
    $portfolio_nav_style = get_theme_mod('portfolio_nav_style', 'navigation');
    $isRTL = (is_rtl()) ? (bool) true : (bool) false;
    $portfoliosettings = array('portfolio_nav_style' => $portfolio_nav_style, 'rtl' => $isRTL);
    wp_register_script('spiko-plus-portfolio', SPIKOP_PLUGIN_URL . 'inc/js/front-page/portfolio.js', array('jquery'));
    wp_localize_script('spiko-plus-portfolio', 'portfolio_settings', $portfoliosettings);
    wp_enqueue_script('spiko-plus-portfolio');

    $post_type = 'spiko_portfolio';
    $tax = 'portfolio_categories';
    $term_args = array('hide_empty' => true, 'orderby' => 'id');
    $posts_per_page = get_theme_mod('home_portfolio_numbers', 3);
    $tax_terms = get_terms($tax, $term_args);
    $defualt_tex_id = get_option('spiko_default_term_id');
    $j = 1;
    $k = 1;
    ?>
    <section class="section-space portfolio bg-default" id="portfolio">
        <div class="spiko-portfolio-container container">
            <?php if (!empty($spiko_plus_project_section_title) || !empty($home_portfolio_section_subtitle)) {
                ?>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="section-header">
                            <?php if (!empty($home_portfolio_section_subtitle)): ?><p class="section-subtitle"><?php echo $home_portfolio_section_subtitle; ?></p><?php endif; ?>
                            <?php if (!empty($spiko_plus_project_section_title)): ?><h2 class="section-title home_project_title "><?php echo $spiko_plus_project_section_title; ?></h2><div class="section-separator border-center"></div><?php endif; ?>
                            
                        </div>
                    </div>
                </div>
                <?php
            }
            if (!empty($tax_terms)) {
                ?>

                <div class="row">
                    <div class="col-md-12">
                        <ul id="tabs" class="nav md-pills flex-center flex-wrap mx-0" role="tablist">
                            <?php foreach ($tax_terms as $tax_term) { ?>
                                <li rel="tab" class="nav-item" >
                                    <a id="tab-<?php echo rawurldecode($tax_term->slug); ?>" href="#<?php echo rawurldecode($tax_term->slug); ?>" data-toggle="tab" role="tab"  class="text-uppercase nav-link <?php
                                    if ($j == 1) {
                                        echo 'active';
                                        $j = 2;
                                    }
                                    ?>"><?php echo $tax_term->name; ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            <?php } else { ?>

                <div class="row">
                    <div class="col-lg-12">
                        <ul id="tabs" class="nav md-pills flex-center flex-wrap mx-0" role="tablist">
                            <li class="nav-item"><a id="tab-A" href="#all" class="text-uppercase nav-link active" data-toggle="tab" role="tab"><?php _e('Show All', 'busiprof'); ?></a></li>
                            <li class="nav-item"><a id="tab-B" href="#bussiness" class="text-uppercase nav-link" data-toggle="tab" role="tab"><?php _e('Bussiness', 'busiprof'); ?></a></li>
                            <li class="nav-item"><a id="tab-C" href="#branding" class="text-uppercase nav-link" data-toggle="tab" role="tab"><?php _e('Branding', 'busiprof'); ?></a></li>
                        </ul>
                    </div>
                </div>
            <?php } if (!empty($tax_terms)) { ?>

                <div id="content" class="tab-content" role="tablist">
                    <?php
                    $is_active = true;
                    if ($tax_terms) {
                        foreach ($tax_terms as $tax_term) {
                            $args = array(
                                'post_type' => $post_type,
                                'post_status' => 'publish',
                                'portfolio_categories' => $tax_term->slug,
                                'posts_per_page' => $no_portfolio,
                                'orderby' => 'menu_order',
                            );
                            $portfolio_query = null;
                            $portfolio_query = new WP_Query($args);
                            if ($portfolio_query->have_posts()):
                                ?>
                                <div id="<?php echo rawurldecode($tax_term->slug); ?>" class="tab-pane fade show in <?php
                                if ($k == 1) {
                                    echo 'active';
                                    $k = 2;
                                }
                                ?>" role="tabpanel" aria-labelledby="tab-<?php echo rawurldecode($tax_term->slug); ?>">
                                    <div class="row">
                                        <?php
                                        while ($portfolio_query->have_posts()) : $portfolio_query->the_post();
                                            $portfolio_target = sanitize_text_field(get_post_meta(get_the_ID(), 'portfolio_target', true));
                                            $portfolio_sub_title = sanitize_text_field(get_post_meta(get_the_ID(), 'portfolio_sub_title', true));
                                            $portfolio_title_description = sanitize_text_field(get_post_meta(get_the_ID(), 'portfolio_title_description', true));
                                            if (get_post_meta(get_the_ID(), 'portfolio_link', true)) {
                                                $portfolio_link = get_post_meta(get_the_ID(), 'portfolio_link', true);
                                            } else {
                                                $portfolio_link = '';
                                            }

                                            $portfolio_description = get_the_content();
                                            ?>
                                            <div class="col-lg-<?php echo $portfolio_col; ?> col-md-<?php echo $portfolio_col; ?>">   
                                                <!--<article class="post">-->
                                                <figure class="portfolio-thumbnail">
                                                    <?php
                                                    the_post_thumbnail('full', array('class' => 'card-img-top img-fluid'));
                                                    if (has_post_thumbnail()) {
                                                        $post_thumbnail_id = get_post_thumbnail_id();
                                                        $post_thumbnail_url = wp_get_attachment_url($post_thumbnail_id);
                                                    }

                                                    if (!empty($portfolio_link)) {
                                                        $tagt = (!empty($portfolio_target)) ? 'target="_blank"' : '';

                                                        $portlink = '<a href=' . "$portfolio_link" . ' title=' . get_the_title() . ' ' . $tagt . ' >' . get_the_title() . '</a>';
                                                    } else {
                                                        $tagt = (!empty($portfolio_target)) ? 'target="_blank"' : '';

                                                        $portlink ='<a href="#" title=' . get_the_title() . ' ' . $tagt . ' >' . get_the_title() . '</a>';
                                                    }
                                                    $modelId = get_the_ID() . '_model'.rawurldecode($tax_term->slug);
                                                    ?>
                                                    <figcaption>
                                                    <?php if(!empty($portlink)):?>
                                                        <div class="entry-header">
                                                            <h4 class="entry-title">
                                                                <?php echo $portlink; ?>
                                                            </h4>
                                                        </div>
                                                    <?php endif;
                                                    $tax_string=implode(" ",get_the_taxonomies());
                                                    $tax_cat=str_replace( array( 'Categories:'), ' ', $tax_string);?>
                                                        <p class="taxonomy-list"><?php  echo $tax_cat;?></p>
                                                    </figcaption>       
                                                    <a data-toggle="modal" data-target="#<?php echo $modelId; ?>"><i>+</i></a>

                                                </figure>   
                                                <!--</article>-->
                                            </div>

                                            <div class="modal fade" id="<?php echo $modelId; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-body p-0">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>

                                                            <!-- Grid row -->
                                                            <div class="row">
                                                                <?php
                                                                if ($post_thumbnail_url) {
                                                                    if (!$portfolio_description) {
                                                                        $ImgGridColumn = 'col-md-12';
                                                                    } else {
                                                                        $ImgGridColumn = 'col-md-6';
                                                                    }
                                                                    if ($portfolio_description) {
                                                                        ?>
                                                                        <!-- Grid column -->
                                                                        <div class="col-md-6 py-5 pl-5">

                                                                            <article class="post text-center">
                                                                                <div class="entry-header">
                                                                                    <h2 class="entry-title">                                                                
                                                                                        <?php echo $portlink; ?>
                                                                                    </h2>

                                                                                </div>
                                                                                <div class="entry-content">
                                                                                    <p><?php echo $portfolio_description; ?></p>
                                                                                </div>
                                                                            </article>

                                                                        </div>
                                                                        <!-- Grid column -->
                                                                    <?php } ?>
                                                                    <!-- Grid column -->
                                                                    <div class="<?php echo $ImgGridColumn; ?> port-view">

                                                                        <div class="view rounded-right">
                                                                            <img class="img-fluid" src="<?php echo $post_thumbnail_url; ?>" alt="Sample image">
                                                                        </div>

                                                                    </div>
                                                                    <!-- Grid column -->
                                                                <?php } ?>
                                                            </div>
                                                            <!-- Grid row -->

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        endwhile;
                                        ?>
                                    </div>
                                </div>
                                <?php
                                wp_reset_query();
                            else:
                                ?>
                                <div id="<?php echo rawurldecode($tax_term->slug); ?>" class="tab-pane fade in <?php
                                     if ($tab == '') {
                                         if ($is_active == true) {
                                             echo 'active';
                                         }$is_active = false;
                                     } else if ($tab == rawurldecode($tax_term->slug)) {
                                         echo 'active';
                                     }
                                     ?>"></div>
                                 <?php
                                 endif;
                             }
                         }
                         ?> 
                </div>
                <?php
//                <?php
            } else {
                do_action('spiko_plus_dummy_portfolio_layout', $portfolio_col);
            }
            ?>

            <?php if(get_theme_mod('home_portfolio_text','Load More')!=''):?>
                <?php if(empty(get_theme_mod('home_portfolio_link_target')))
                {
                    $p_targt='_self';
                }
                else
                {
                    $p_targt='_blank';
                }?>
            <div class="portfolio-btn text-center">
                     <a target="<?php echo $p_targt;?>" href="<?php echo esc_url(get_theme_mod('home_portfolio_link','#'));?>" class="btn-small btn-default" alt="Check-it-out">
                        <?php echo get_theme_mod('home_portfolio_text','Load More');?>
                        <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                 </div>
            <?php endif;?>     

        </div>
    </section>
<?php } 
}