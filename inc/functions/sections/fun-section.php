<?php
//call the action for the fun-acts section
add_action('spiko_plus_fun_action','spiko_plus_fun_section');
//function for the fun-acts section
function spiko_plus_fun_section()
{
	$funfact_section_enabled = get_theme_mod('funfact_section_enabled', true);
	if($funfact_section_enabled != false)
	{
		// Funfact Callback
            $atts=array(
                      'col' => '4',
                    );

            $funfact_section=spiko_funfact_callback($atts);
            echo $funfact_section;
	}
}