<?php
//call the action for the testimonial section
add_action('spiko_plus_testimonial_action','spiko_plus_testimonial_section');
//function for the testimonial section
function spiko_plus_testimonial_section()
{	
	$theme = wp_get_theme();
	if('Spiko Dark' == $theme->name) {
	    $sd_testi_design=get_theme_mod('home_testimonial_design_layout',4);
	}
	else{
	    $sd_testi_design=get_theme_mod('home_testimonial_design_layout',1);
	}
	$testimonial_section_enable = get_theme_mod('testimonial_section_enable', true);
		if($testimonial_section_enable != false)
		{	
			// Testimonial Callback
            $atts=array(
            		'format' => 'slide',
			      	'style' => $sd_testi_design,
			      	'items' => get_theme_mod('home_testimonial_slide_item',1),
			      	'col' =>'3',
                    );

            $testimonial_section=spiko_testimonial_callback($atts);
            echo $testimonial_section;			
		} 
}