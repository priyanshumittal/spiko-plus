<?php
//call the action for the cta2 section
add_action('spiko_plus_cta_action','spiko_plus_cta_section');
//function for the cta2 section
function spiko_plus_cta_section()
{
	$cta2_section_enable  = get_theme_mod('cta2_section_enable', true);
	if($cta2_section_enable != false) 
	{ 
		//CTA Section
		$cta_section=spiko_cta_callback();
		echo $cta_section;
	}
}