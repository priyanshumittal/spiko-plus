<?php
//call the action for the client section
add_action('spiko_plus_client_action','spiko_plus_client_section');
//function for the client section
function spiko_plus_client_section()
{
    $client_section_enable = get_theme_mod('client_section_enable', true);
        if($client_section_enable != false)
        {
            // Client Callback
            $atts=array(
                      'items' => '5',
                    );

            $client_section=spiko_client_callback($atts);
            echo $client_section;
        }
}