<?php
//call the action for the services section
add_action('spiko_plus_services_action','spiko_plus_services_section');
//function for the services section
function spiko_plus_services_section()
{
	$theme = wp_get_theme();
	if('Spiko Dark' == $theme->name) {
	    $sd_service_design= get_theme_mod('home_serive_design_layout',2);
	}
	else{
	    $sd_service_design= get_theme_mod('home_serive_design_layout',1);
	}
	$spiko_home_service_enabled = get_theme_mod('home_service_section_enabled', true);
		if($spiko_home_service_enabled != false)
		{
			// Service Callback
            $atts=array(
                      'style' => $sd_service_design,
                      'col'	=> get_theme_mod('home_service_col',3),
                    );

            $service_section=spiko_service_callback($atts);
            echo $service_section;
		}
}