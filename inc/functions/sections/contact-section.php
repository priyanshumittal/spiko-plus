<?php
//call the action for the contact section
add_action('spiko_plus_contact_action','spiko_plus_contact_section');
//function for the contact section
function spiko_plus_contact_section(){ 
$home_contact_section_enabled  = get_theme_mod('home_contact_section_enabled', true);
if($home_contact_section_enabled != false) 
{ 
include_once(SPIKOP_PLUGIN_DIR.'/inc/inc/home-section/contact-content.php');
}
}?>