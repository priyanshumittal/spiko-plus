<?php
//call the action for the team section
add_action('spiko_plus_team_action','spiko_plus_team_section');
//function for the services section
function spiko_plus_team_section()
{	
	$theme = wp_get_theme();
	if('Spiko Dark' == $theme->name) {
	    $sd_team_design=get_theme_mod('home_team_design_layout',2);
	}
	else{
	    $sd_team_design=get_theme_mod('home_team_design_layout',1);
	}
	$team_section_enable = get_theme_mod('team_section_enable', true);
		if ($team_section_enable != false) {
			// Team Callback
            $atts=array(
            		'format' => 'slide',
			      	'style' => $sd_team_design,
			      	'items' =>'3',
			      	'col' =>'3',
                    );

            $team_section=spiko_team_callback($atts);
            echo $team_section;
		}
}