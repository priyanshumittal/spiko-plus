<?php
/************* Home portfolio custom post type ************************/		
function spiko_plus_project_type()
		{	register_post_type( 'spiko_portfolio',
				array(
				'labels' => array(
				'name' => __('Projects', 'spiko-plus'),
				'add_new' => __('Add New', 'spiko-plus'),
                'add_new_item' => __('Add New Project','spiko-plus'),
				'edit_item' => __('Add New','spiko-plus'),
				'new_item' => __('New Link','spiko-plus'),
				'all_items' => __('All Projects','spiko-plus'),
				'view_item' => __('View Link','spiko-plus'),
				'search_items' => __('Search Links','spiko-plus'),
				'not_found' =>  __('No Links found','spiko-plus'),
				'not_found_in_trash' => __('No Links found in Trash','spiko-plus'), 
				
			),
				'supports' => array('title','thumbnail','editor','excerpt'),
				'show_in_nav_menus' => false,
				'public' => true,
				'rewrite' => array('slug' => 'spiko_portfolio'),
				'menu_position' => 11,
				'public' => true,
				'menu_icon' => SPIKOP_PLUGIN_URL . '/inc/images/portfolio.png',
			)
	);
}
add_action( 'init', 'spiko_plus_project_type' );
?>