<?php
/**
 * Helper functions.
 *
 * @package spiko
 */


/**
 * Get Footer widgets
 */
if (!function_exists('spiko_plus_footer_widget_area')) {

    /**
     * Get Footer Default Sidebar
     *
     * @param  string $sidebar_id   Sidebar Id..
     * @return void
     */
    function spiko_plus_footer_widget_area($sidebar_id) {

        if (is_active_sidebar($sidebar_id)) {
            dynamic_sidebar($sidebar_id);
        } elseif (current_user_can('edit_theme_options')) {

            global $wp_registered_sidebars;
            $sidebar_name = '';
            if (isset($wp_registered_sidebars[$sidebar_id])) {
                $sidebar_name = $wp_registered_sidebars[$sidebar_id]['name'];
            }
            ?>
            <div class="widget ast-no-widget-row">
                <h2 class='widget-title'><?php echo esc_html($sidebar_name); ?></h2>

                <p class='no-widget-text'>
                    <a href='<?php echo esc_url(admin_url('widgets.php')); ?>'>
                        <?php esc_html_e('Click here to assign a widget for this area.', 'spiko-plus'); ?>
                    </a>
                </p>
            </div>
            <?php
        }
    }

}

/**
 * Function to get Footer Menu
 */
if (!function_exists('spiko_plus_footer_bar_menu')) {

    /**
     * Function to get Footer Menu
     *
     */
    function spiko_plus_footer_bar_menu() {

        ob_start();

        if (has_nav_menu('footer_menu')) {
            wp_nav_menu(
                    array(
                        'theme_location' => 'footer_menu',
                        'menu_class' => 'nav-menu',
                        'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        'depth' => 1,
                    )
            );
        } else {
            if (is_user_logged_in() && current_user_can('edit_theme_options')) {
                ?>
                <a href="<?php echo esc_url(admin_url('/nav-menus.php?action=locations')); ?>"><?php esc_html_e('Assign Footer Menu', 'spiko-plus'); ?></a>
                <?php
            }
        }

        return ob_get_clean();
    }

}

if (!function_exists('spiko_plus_widget_layout')):

    function spiko_plus_widget_layout() {

        $spiko_plus_footer_widget = get_theme_mod('footer_widgets_section', 4);
        switch ($spiko_plus_footer_widget) {
            case 1:
                include_once(SPIKOP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-1.php');
                break;

            case 2:
                include_once(SPIKOP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-2.php');
                break;

            case 3:
                include_once(SPIKOP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-3.php');
                break;

            case 4:
                include_once(SPIKOP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-4.php');
                break;

            case 5:
                include_once(SPIKOP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-5.php');
                break;

            case 6:
                include_once(SPIKOP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-6.php');
                break;

            case 7:
                include_once(SPIKOP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-7.php');
                break;

            case 8:
                include_once(SPIKOP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-8.php');
                break;

            case 9:
                include_once(SPIKOP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-9.php');
                break;
        }
    }

endif;

/* Footer Widget Layout section */
if (!function_exists('spiko_plus_footer_widget_layout_section')) {

    function spiko_plus_footer_widget_layout_section() {
            /**
             * Get Footer widgets
             */
            if (!function_exists('spiko_plus_footer_widget_area')) {

                /**
                 * Get Footer Default Sidebar
                 *
                 * @param  string $sidebar_id   Sidebar Id..
                 * @return void
                 */
                function spiko_plus_footer_widget_area($sidebar_id) {

                    if (is_active_sidebar($sidebar_id)) {
                        dynamic_sidebar($sidebar_id);
                    } elseif (current_user_can('edit_theme_options')) {

                        global $wp_registered_sidebars;
                        $sidebar_name = '';
                        if (isset($wp_registered_sidebars[$sidebar_id])) {
                            $sidebar_name = $wp_registered_sidebars[$sidebar_id]['name'];
                        }
                        ?>
                        <div class="widget ast-no-widget-row">
                            <h2 class='widget-title'><?php echo esc_html($sidebar_name); ?></h2>

                            <p class='no-widget-text'>
                                <a href='<?php echo esc_url(admin_url('widgets.php')); ?>'>
                                    <?php esc_html_e('Click here to assign a widget for this area.', 'spiko-plus'); ?>
                                </a>
                            </p>
                        </div>
                        <?php
                    }
                }

            }
            /* Function for widget sectons */
            spiko_plus_widget_layout();
        /* Function for widget sectons */

        
    }

}
/* Footer Widget Layout section */

/* Footer Bar layout section */
if (!function_exists('spiko_plus_footer_bar_section')) {

    function spiko_plus_footer_bar_section() {
        if (get_theme_mod('ftr_bar_enable', true) == true):
            $advance_footer_bar_section = get_theme_mod('advance_footer_bar_section', 1);
            switch ($advance_footer_bar_section) {
                case 1:
                    include_once(SPIKOP_PLUGIN_DIR.'/inc/inc/footer-bar/layout-1.php');
                    break;

                case 2:
                    include_once(SPIKOP_PLUGIN_DIR.'/inc/inc/footer-bar/layout-2.php');
                    break;
            }
        endif;
        }

}
/* Footer Bar layout section */

if (!function_exists('spiko_plus_custom_navigation')) :

    function spiko_plus_custom_navigation() {
        if(get_theme_mod('post_nav_style_setting','pagination')=='pagination'){
            echo '<div class="row justify-content-center">';
                    $obj = new spiko_plus_pagination();
                    $obj->spiko_plus_page();
                    echo '</div>';
        }else{
            echo do_shortcode('[ajax_posts]');
        }
    }
endif;
add_action('spiko_plus_post_navigation', 'spiko_plus_custom_navigation');

function spiko_plus_comment($comment, $args, $depth) {
    $tag = 'div';
    $add_below = 'comment';
    ?>
    <div class="media comment-box 1">
        <span class="pull-left-comment">
    <?php echo get_avatar($comment, 100, null, 'comments user', array('class' => array('img-fluid comment-img'))); ?>
        </span>
        <div class="media-body">
            <div class="comment-detail">
                <h5 class="comment-detail-title"><?php esc_html(comment_author()); ?><time class="comment-date"><?php printf(esc_html__('%1$s  %2$s', 'spiko-plus'), esc_html(get_comment_date()), esc_html(get_comment_time())); ?></time></h5>
    <?php comment_text(); ?>

                <div class="reply">
    <?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                </div>
            </div>


        </div>      

    </div>
    <?php
}

if (!function_exists('spiko_plus_posted_content')) :

    /**
     * Content
     *
     */
    function spiko_plus_posted_content() {
        $blog_content = get_theme_mod('spiko_blog_content', 'excerpt');
        $excerpt_length = get_theme_mod('spiko_blog_content_length', 30);

        if ('excerpt' == $blog_content) {
            $excerpt = spiko_the_excerpt(absint($excerpt_length));
            if (!empty($excerpt)) :
                ?>


                <?php
                echo wp_kses_post(wpautop($excerpt));
                ?>


            <?php endif;
        } else {
            ?>

            <?php the_content(); ?>

        <?php }
        ?>
    <?php
    }

endif;



if (!function_exists('spiko_plus_the_excerpt')) :

    /**
     * Generate excerpt.
     *
     */
    function spiko_plus_the_excerpt($length = 0, $post_obj = null) {

        global $post;

        if (is_null($post_obj)) {
            $post_obj = $post;
        }

        $length = absint($length);

        if (0 === $length) {
            return;
        }

        $source_content = $post_obj->post_content;

        if (!empty($post_obj->post_excerpt)) {
            $source_content = $post_obj->post_excerpt;
        }

        $source_content = preg_replace('`\[[^\]]*\]`', '', $source_content);
        $trimmed_content = wp_trim_words($source_content, $length, '&hellip;');
        return $trimmed_content;
    }

endif;

if (!function_exists('spiko_plus_button_title')) :

    /**
     * Display Button on Archive/Blog Page 
     */
    function spiko_plus_button_title() {
        if (get_theme_mod('spiko_enable_blog_read_button', true) == true):
            $blog_button = get_theme_mod('spiko_blog_button_title', 'READ MORE');

            if (empty($blog_button)) {
                return;
            }
            echo '<p><a href = "' . esc_url(get_the_permalink()) . '" class="more-link">' . esc_html($blog_button) . ' <i class="fa fa-long-arrow-right"></i></a></p>';

        endif;
    }

endif;


/*  Related posts
  /* ------------------------------------ */
if (!function_exists('spiko_plus_related_posts')) {

    function spiko_plus_related_posts() {
        wp_reset_postdata();
        global $post;

        // Define shared post arguments
        $args = array(
            'no_found_rows' => true,
            'update_post_meta_cache' => false,
            'update_post_term_cache' => false,
            'ignore_sticky_posts' => 1,
            'orderby' => 'rand',
            'post__not_in' => array($post->ID),
            'posts_per_page' => 10
        );
        // Related by categories
        if (get_theme_mod('spiko_related_post_option') == 'categories') {

            $cats = get_post_meta($post->ID, 'related-cat', true);

            if (!$cats) {
                $cats = wp_get_post_categories($post->ID, array('fields' => 'ids'));
                $args['category__in'] = $cats;
            } else {
                $args['cat'] = $cats;
            }
        }
        // Related by tags
        if (get_theme_mod('spiko_related_post_option') == 'tags') {

            $tags = get_post_meta($post->ID, 'related-tag', true);

            if (!$tags) {
                $tags = wp_get_post_tags($post->ID, array('fields' => 'ids'));
                $args['tag__in'] = $tags;
            } else {
                $args['tag_slug__in'] = explode(',', $tags);
            }
            if (!$tags) {
                $break = true;
            }
        }

        $query = !isset($break) ? new WP_Query($args) : new WP_Query;
        return $query;
    }

}

/**
 * Displays the author name
 */
function spiko_plus_get_author_name($post) {

    $user_id = $post->post_author;
    if (empty($user_id)) {
        return;
    }

    $user_info = get_userdata($user_id);
    echo esc_html($user_info->display_name);
}

function spiko_plus_footer_section_fn(){
spiko_plus_before_footer_section_trigger();?>
<footer class="site-footer" <?php if (!empty(get_theme_mod('ftr_wgt_background_img'))): ?> style="background-image: url(<?php echo get_theme_mod('ftr_wgt_background_img'); ?>);" <?php endif; ?>>  
    <?php $fwidgets_overlay_section_color = get_theme_mod('spiko_plus_fwidgets_overlay_section_color', 'rgba(0, 0, 0, 0.7)'); ?>
    <div class="overlay" <?php
    if (!empty(get_theme_mod('ftr_wgt_background_img'))) {
        if (get_theme_mod('spiko_plus_fwidgets_image_overlay', true) == true) {
            ?> style="background-color:<?php echo $fwidgets_overlay_section_color; ?>" <?php
             }
         }
         ?>>

        <?php if (get_theme_mod('ftr_widgets_enable', true) === true) { ?>
            <div class="container">
                <?php
                // Widget Layout
                spiko_plus_footer_widget_layout_section();
                ?>
            </div>
        <?php } ?>

        <?php
        // Footer bar
        spiko_plus_footer_bar_section();
        ?>
    </div>
</footer>
<?php spiko_plus_after_footer_section_trigger();
$client_bg_clr=get_theme_mod('clt_bg_color','#fff');
$ribon_enable = get_theme_mod('scrolltotop_setting_enable', true);
if ($ribon_enable == true) {
    ?>
    <div class="scroll-up custom <?php echo get_theme_mod('scroll_position_setting', 'right'); ?>"><a href="#totop"><i class="<?php echo get_theme_mod('spiko_plus_scroll_icon_class', 'fa fa-arrow-up'); ?>"></i></a></div>
<?php } ?>

<style type="text/css">
    .scroll-up {
        <?php echo get_theme_mod('scroll_position_setting', 'right'); ?>: 30px !important;
    }
    .scroll-up.left{right: auto;}
    .scroll-up.custom a {
        border-radius: <?php echo get_theme_mod('spiko_plus_scroll_border_radius', 3); ?>px;
    }  
    <?php if(get_theme_mod('apply_scrll_top_clr_enable',false)==true):?>
    .scroll-up.custom a {
    background: <?php echo get_theme_mod('spiko__scroll_bg_color','#35ac39');?>;
    color: <?php echo get_theme_mod('spiko__scroll_icon_color','#fff');?>;
    }
    .scroll-up.custom a:hover,
    .scroll-up.custom a:active {
        background: <?php echo get_theme_mod('spiko__scroll_bghover_color','#fff');?>;
        color: <?php echo get_theme_mod('spiko__scroll_iconhover_color','#35ac39');?>;
    }
<?php endif;?>  
</style>
<style type="text/css">
    .sponsors {
        background-color: <?php echo $client_bg_clr; ?>;
    }
    <?php
    if (get_theme_mod('testimonial_image_overlay', true) != false) {
        $theme = wp_get_theme();
        if('Spiko Dark' == $theme->name) {
           $testimonial_overlay_section_color = get_theme_mod('testimonial_overlay_section_color', 'rgba(0,0,0,0.8)');

        }
        else{
            $testimonial_overlay_section_color = get_theme_mod('testimonial_overlay_section_color', 'rgba(255,255,255,0.8)');
        }
        
        ?>
        .section-space.testimonial:before {
            background-color:<?php echo $testimonial_overlay_section_color; ?>;
        }
        
       
        .testi-4:before {
            background-color: <?php echo $testimonial_overlay_section_color; ?>;
        <?php } ?>
        </style>

    <style type="text/css">
            .site-footer {
            background-repeat:  <?php echo get_theme_mod('footer_widget_reapeat', 'no-repeat'); ?>;
            background-position: <?php echo get_theme_mod('footer_widget_position', 'left top'); ?>;
            background-size: <?php echo get_theme_mod('footer_widget_bg_size', 'cover'); ?>;
            background-attachment: <?php echo get_theme_mod('footer_widget_bg_attachment', 'scroll'); ?>;
        }
    </style>
<?php
$slidelayout=get_theme_mod('home_testimonial_design_layout',1);
$slideitem=get_theme_mod('home_testimonial_slide_item',1);
if($slidelayout==2 && $slideitem==3){?>
    <style type="text/css">
        #testimonial-carousel2 .testmonial-block, 
        .page-template-template-testimonial-6 .testmonial-block {
            padding: 15px;
            margin: 0 15px 15px;
        }
        #testimonial-carousel2 .avatar, 
        .page-template-template-testimonial-6 .avatar {
            position: relative;
        }
    </style>
    <?php
}
//Stick Header
    if (get_theme_mod('sticky_header_enable', false) == true):
        include_once(SPIKOP_PLUGIN_DIR.'/inc/inc/sticky-header/sticky-with' . get_theme_mod('sticky_header_animation', '') . '-animation.php');
    endif;
}

add_action('spiko_plus_footer_section_hook', 'spiko_plus_footer_section_fn');

function the_company_blog_meta() {
    if (get_theme_mod('spiko_enable_blog_author') == false) {
        $string = ' In ';
    } else {
        $string = ' in ';
    }
    return $string;
}

function the_company_single_meta() {
    if (get_theme_mod('spiko_plus_enable_single_post_admin') == false) {
        $string = ' In ';
    } else {
        $string = ' in ';
    }
    return $string;
}

add_action('spiko_plus_sticky_header_logo', 'spiko_plus_sticky_header_logo_fn');

function spiko_plus_sticky_header_logo_fn() {
    $custom_logo_id = get_theme_mod('custom_logo');
    $image = wp_get_attachment_image_src($custom_logo_id, 'full');
    if (get_theme_mod('sticky_header_device_enable', 'desktop') == 'desktop') {
        $sticky_header_logo_desktop = get_theme_mod('sticky_header_logo_desktop', '');
        if (!empty($sticky_header_logo_desktop)):
            ?>
            <a href="<?php echo esc_url(home_url('/')); ?>" class="navbar-brand sticky-logo" style="display: none;">
                <img src="<?php echo esc_url($sticky_header_logo_desktop); ?>" class="custom-logo"></a>
            <?php
        else:
            ?>
            <a href="<?php echo esc_url(home_url('/')); ?>" class="navbar-brand sticky-logo" style="display: none;">
                <img src="<?php echo esc_url($image[0]); ?>" class="custom-logo"></a>
            <?php
            endif;
        } elseif (get_theme_mod('sticky_header_device_enable', 'desktop') == 'mobile') {
            $sticky_header_logo_mbl = get_theme_mod('sticky_header_logo_mbl', '');
            if (!empty($sticky_header_logo_mbl)):
                ?>
            <a href="<?php echo esc_url(home_url('/')); ?>" class="navbar-brand sticky-logo-mbl" style="display: none;">
                <img width="280" height="48" src="<?php echo esc_url($sticky_header_logo_mbl); ?>" class="custom-logo"></a>
            <?php else:
            ?><a href="<?php echo esc_url(home_url('/')); ?>" class="navbar-brand sticky-logo-mbl" style="display: none;">
                <img width="280" height="48" src="<?php echo esc_url($image[0]); ?>" class="custom-logo"></a>
        <?php
            endif;
        } else {
            $sticky_header_logo_desktop = get_theme_mod('sticky_header_logo_desktop', '');
            if (!empty($sticky_header_logo_desktop)):
                ?>
            <a href="<?php echo esc_url(home_url('/')); ?>" class="navbar-brand sticky-logo" style="display: none;">
                <img src="<?php echo esc_url($sticky_header_logo_desktop); ?>" class="custom-logo"></a>
            <?php else:
            ?><a href="<?php echo esc_url(home_url('/')); ?>" class="navbar-brand sticky-logo" style="display: none;">
                <img src="<?php echo esc_url($image[0]); ?>" class="custom-logo"></a>
            <?php
            endif;

            $sticky_header_logo_mbl = get_theme_mod('sticky_header_logo_mbl', '');
            if (!empty($sticky_header_logo_mbl)):
                ?>
            <a href="<?php echo esc_url(home_url('/')); ?>" class="navbar-brand sticky-logo-mbl" style="display: none;">
                <img width="280" height="48" src="<?php echo esc_url($sticky_header_logo_mbl); ?>" class="custom-logo"></a>
            <?php else:
            ?><a href="<?php echo esc_url(home_url('/')); ?>" class="navbar-brand sticky-logo-mbl" style="display: none;">
                <img width="280" height="48" src="<?php echo esc_url($image[0]); ?>" class="custom-logo"></a>
        <?php
        endif;
    }
}

/* Spiko Contact template function * */

function content_contact_data($data) {
    $contact_data = get_theme_mod($data);
    if (empty($contact_data)) {
        $contact_data = json_encode(array(
            array(
                    'icon_value' => 'fa-paper-plane',
                    'choice' => 'customizer_repeater_icon',
                    'title' => esc_html__('Address', 'spiko-plus'),
                    'text' => esc_html__('06 Highley St, Victoria, Australia', 'spiko-plus'),
                    'id' => 'customizer_repeater_56d7pa7f40b56',
                ),
                array(
                    'icon_value' => 'fa-phone',
                    'choice' => 'customizer_repeater_icon',
                    'title' => esc_html__('Phone', 'spiko-plus'),
                    'text' => esc_html__('(91) 22 54215821, (91) 9876543210', 'spiko-plus'),
                    'id' => 'customizer_repeater_56d7da7f40b66',
                ),
                array(
                    'icon_value' => 'fa-envelope',
                    'choice' => 'customizer_repeater_icon',
                    'title' => esc_html__('Email', 'spiko-plus'),
                    'text' => esc_html__('contact@niterex.com office@niterex.com', 'spiko-plus'),
                    'id' => 'customizer_repeater_56d7op7f40b86',
                ),
                array(
                    'icon_value' => 'fa-regular fa-clock',
                    'choice' => 'customizer_repeater_icon',
                    'title' => esc_html__('Working Times', 'spiko-plus'),
                    'text' => esc_html__('Mon - Fri : 08:00 - 18:00 Sat - Sun : Holiday', 'spiko-plus'),
                    'id' => 'customizer_repeater_12d7op7f40b86',
                ),
        ));
    }
    return $contact_data;
}

// Add heder feature
add_action('spiko_plus_header_feaure_section_hook','spiko_plus_header_feaure_section_hook');
function spiko_plus_header_feaure_section_hook(){

    include_once(SPIKOP_PLUGIN_DIR.'/inc/inc/topbar-header.php');

    spiko_plus_before_header_section_trigger();
    //Header Preset
    if (get_theme_mod('header_logo_placing', 'left')):
        $header_logo_path= SPIKOP_PLUGIN_DIR.'/inc/inc/header-preset/menu-with-'.get_theme_mod('header_logo_placing','left').'-logo.php';
        include_once($header_logo_path);
    endif;
    
    if (get_theme_mod('search_effect_style_setting', 'toggle') != 'toggle'):?>
        <div id="searchbar_fullscreen" <?php if (get_theme_mod('search_effect_style_setting', 'popup_light') == 'popup_light'): ?> class="bg-light" <?php endif; ?>>
            <button type="button" class="close">×</button>
            <form method="get" id="searchform" autocomplete="off" class="search-form" action="<?php echo esc_url(home_url('/')); ?>"><label><input autofocus type="search" class="search-field" placeholder="Search Keyword" value="" name="s" id="s" autofocus></label><input type="submit" class="search-submit btn" value="<?php echo esc_html__('Search', 'spiko-plus'); ?>"></form>
        </div>
    <?php
    endif;
    spiko_plus_after_header_section_trigger();
}

//Container Setting For Page
function spiko_container()
{
  if(get_theme_mod('page_container_setting','default')=='default')
{
 $container_width= " container_default";
}
elseif(get_theme_mod('page_container_setting','default')=='full_width_fluid')
{
 $container_width= "-fluid";
}
else
{
  $container_width= "-fluid streached";
}
return $container_width;
}

//Container Setting For Blog Post
function spiko_blog_post_container()
{
  if(get_theme_mod('post_container_setting','default')=='default')
{
 $container_width= " container_default";
}
elseif(get_theme_mod('post_container_setting','default')=='full_width_fluid')
{
 $container_width= "-fluid";
}
else
{
  $container_width= "-fluid streached";
}
return $container_width;
}

//Conainer Setting For Single Post

function spiko_single_post_container()
{
  if(get_theme_mod('single_post_container_setting','default')=='default')
{
 $container_width= " container_default";
}
elseif(get_theme_mod('single_post_container_setting','default')=='full_width_fluid')
{
 $container_width= "-fluid";
}
else
{
  $container_width= "-fluid streached";
}
return $container_width;
}

//Preloader feature section function
function spiko_plus_preloader_feaure_section_fn(){
global $template;
$col=explode("-",basename($template));
if (array_key_exists(1,$col)){
$column=$col[1];
}
else{
$column='';
}
//Preloader
if(get_theme_mod('preloader_enable',false)==true && ($column!='portfolio')):
 include_once(SPIKOP_PLUGIN_DIR.'/inc/inc/preloader/preloader-'.get_theme_mod('preloader_style',1).'.php');
endif;
}
add_action('spiko_plus_preloader_feaure_section_hook','spiko_plus_preloader_feaure_section_fn');


/* SHORTCODE FOR THEME */


//SHORTCODE FOR SERVICE SECTION
function spiko_service_callback ( $atts ){
ob_start();    
extract(shortcode_atts( array('style' => '1','col' => '3', ), $atts ));
$service_style=$style;
if($service_style==1) { $services_id=''; } else { $services_id=$service_style; }
switch($col)
{   
    case '1': $col = 'col-md-12 col-sm-12 col-xs-12'; break;
    case '2': $col = 'col-md-6 col-sm-6 col-xs-12'; break; 
    case '3': $col = 'col-md-4 col-sm-6 col-xs-12'; break; 
    case '4': $col = 'col-md-3 col-sm-6 col-xs-12'; break; 
    default:$col = 'col-md-4 col-sm-6 col-xs-12'; break; 
} 
$service_data = get_theme_mod('spiko_service_content');
if (empty($service_data)) { $service_data = starter_service_json(); }
$spiko_service_section_title = get_theme_mod('home_service_section_title', __('Why Choose Us?', 'spiko-plus'));
$spiko_service_section_discription = get_theme_mod('home_service_section_discription', __('What We Do', 'spiko-plus'));        
?>
<section class="section-space services<?php echo $services_id;?> bg-default-color">
    <div class="spiko-service-container container">
        <?php if ($spiko_service_section_discription != '' || $spiko_service_section_title != '') {?>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="section-header">
                    <?php if ($spiko_service_section_discription != '') { ?>
                    <p class="section-subtitle"><?php echo $spiko_service_section_discription; ?></p>
                    <?php } ?>
                    <?php if ($spiko_service_section_title != '') { ?>
                    <h2 class="section-title"><?php echo $spiko_service_section_title; ?></h2>
                    <div class="section-separator border-center"></div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="row">
            <?php
            $service_data = json_decode($service_data);
            if (!empty($service_data)) {
            foreach ($service_data as $service_team) {
                    $service_icon = !empty($service_team->icon_value) ? apply_filters('spiko_translate_single_string', $service_team->icon_value, 'Service section') : '';
                    $service_image = !empty($service_team->image_url) ? apply_filters('spiko_translate_single_string', $service_team->image_url, 'Service section') : '';
                    $service_title = !empty($service_team->title) ? apply_filters('spiko_translate_single_string', $service_team->title, 'Service section') : '';
                    $service_desc = !empty($service_team->text) ? apply_filters('spiko_translate_single_string', $service_team->text, 'Service section') : '';
                    $service_link = !empty($service_team->link) ? apply_filters('spiko_translate_single_string', $service_team->link, 'Service section') : '';
                    ?>
            <div class="<?php echo $col;?>">
                <?php if($service_style==1) { ?>
                <div class="card">
                    <div class="card-body">
                        <?php
                        if ($service_team->choice == 'customizer_repeater_icon') {
                        if ($service_icon != '') { ?>
                        <p class="service-icon">
                            <?php if ($service_link != '') { ?>
                            <a <?php if ($service_team->open_new_tab == 'yes') { echo "target='_blank'"; } ?> href="<?php echo esc_url($service_link); ?>"><i class="fa <?php echo $service_icon; ?>"></i></a>
                            <?php } else { ?>
                            <a><i class="fa <?php echo $service_icon; ?>"></i></a>
                            <?php } ?>
                        </p>
                        <?php
                        }
                        } 
                        else if ($service_team->choice == 'customizer_repeater_image') {
                        if ($service_image != '') { ?>
                        <p class="service-icon"> 
                            <?php if ($service_link != '') { ?>
                            <a <?php if ($service_team->open_new_tab == 'yes') { echo "target='_blank'"; } ?> href="<?php echo esc_url($service_link); ?>">
                            <?php } ?><img class='card-img-top' src="<?php echo $service_image; ?>">
                            <?php if ($service_link != '') { ?>
                            </a>
                            <?php } ?>
                        </p>
                        <?php
                        }
                        }

                        if ($service_title != "") { ?>
                        <h4 class="entry-title">
                            <?php if ($service_link != '') { ?>
                            <a href="<?php echo esc_url($service_link); ?>" <?php if ($service_team->open_new_tab == 'yes') { echo "target='_blank'"; } ?> > 
                            <?php } echo $service_title;
                            if ($service_link != '') { ?>
                            </a>
                            <?php } ?>
                        </h4>
                        <?php
                        } 

                        if ($service_desc != "") { ?>
                        <p class="description"><?php echo $service_desc; ?></p>
                        <?php  
                        } ?>
                    </div>
                </div>
            <?php }
            else
            {
            ?>
            <article class="post <?php if($service_style==3) { ?>text-center <?php } ?>">
            <?php
            if ($service_team->choice == 'customizer_repeater_icon') {
                if ($service_icon != '') { ?>
                    <figure class="post-thumbnail"> 
                    <?php if ($service_link != '') { ?>
                        <a <?php if ($service_team->open_new_tab == 'yes') { echo "target='_blank'"; } ?> href="<?php echo esc_url($service_link); ?>">
                            <i class="fa <?php echo $service_icon; ?>"></i>
                         </a>
                    <?php } 
                    else { ?>
                        <a><i class="fa <?php echo $service_icon; ?>"></i></a>
                    <?php } ?>
                    </figure>
                <?php
                }
            } 
            else if ($service_team->choice == 'customizer_repeater_image') {
                if ($service_image != '') { ?>
                    <figure class="post-thumbnail"> 
                    <?php if ($service_link != '') { ?>
                        <a <?php if ($service_team->open_new_tab == 'yes') { echo "target='_blank'"; } ?> href="<?php echo esc_url($service_link); ?>">
                    <?php } ?>
                            <img class='card-img-top' src="<?php echo $service_image; ?>">
                     <?php if ($service_link != '') { ?>
                        </a>
                    <?php } ?>
                    </figure>
                <?php
                }
            }
            if ($service_title != "") { ?>
                <div class="entry-header">
                    <h4 class="entry-title">
                    <?php if ($service_link != '') { ?>
                        <a href="<?php echo esc_url($service_link); ?>" <?php if ($service_team->open_new_tab == 'yes') { echo "target='_blank'"; } ?>> 
                    <?php
                    } 
                        echo $service_title;
                    if ($service_link != '') { ?>
                        </a>
                    <?php } ?>
                    </h4>
                </div>
            <?php
            }
            if ($service_desc != "") { ?>
                <div class="entry-content"><p><?php echo $service_desc; ?></p></div>
            <?php  } ?>      
            </article>
            <?php    
            }
            ?>
            </div>
            <?php 
                }
            } ?>
        </div>
    </div>
</section>
<?php 
$stringa = ob_get_contents();
ob_end_clean();
return $stringa;   
}
add_shortcode( 'spiko_service', 'spiko_service_callback' );


//SHORTCODE FOR CTA

function spiko_cta_callback(){
ob_start();    
$home_cta2_title = get_theme_mod('home_cta2_title', __('Easy & Simple - No Coding Required!', 'spiko-plus'));
$home_cta2_desc = get_theme_mod('home_cta2_desc', __('It is a long established fact that a reader will be distracted by the readable content of a page when looking <br> at its layout. The point of using Lorem ipsum dolor sit amet elit.', 'spiko-plus'));
$home_cta2_btn1_text = get_theme_mod('home_cta2_btn1_text', __('Purchase Now', 'spiko-plus'));
$home_cta2_btn2_text = get_theme_mod('home_cta2_btn2_text', __('Get In Touch', 'spiko-plus'));
$home_cta2_btn1_link = get_theme_mod('home_cta2_btn1_link', '#');
$home_cta2_btn2_link = get_theme_mod('home_cta2_btn2_link', '#');
$home_cta2_btn1_link_target = get_theme_mod('home_cta2_btn1_link_target', false);
$home_cta2_btn2_link_target = get_theme_mod('home_cta2_btn2_link_target', false);
?>  
<!--Call to Action-->
<?php
$callout_cta2_background = get_theme_mod('callout_cta2_background', SPIKOP_PLUGIN_URL.'/inc/images/bg/cta-3.jpg');
if ($callout_cta2_background != '') {
    ?>
    <section class="section-space cta"  style="background-image:url('<?php echo esc_url($callout_cta2_background); ?>'); background-repeat: no-repeat; background-position: top left; width: 100%;
             background-size: cover;">
             <?php
         } else {
             ?>
        <section class="section-space cta" >
            <?php
        }
        $cta2_overlay_section_color = get_theme_mod('cta2_overlay_section_color', 'rgba(0, 11, 24, 0.80)');
        $cta2_image_overlay = get_theme_mod('cta2_image_overlay', true);
        ?>
        <div class="overlay" <?php if ($cta2_image_overlay != false) { ?> style="background-color:<?php echo $cta2_overlay_section_color; ?>"<?php } ?> >
        <?php if (!empty($home_cta2_title) || (!empty($home_cta2_desc)) || (!empty($home_cta2_btn1_text)) || (!empty($home_cta2_btn1_text)) || (!empty($home_cta2_btn2_text))): ?>
                <div class="spiko-cta2-container container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="cta-block text-center">
                                <?php if (!empty($home_cta2_title)): ?><h2 class="title text-white"><?php echo $home_cta2_title; ?></h2><?php endif; ?>
                                <?php if (!empty($home_cta2_desc)): ?><p class="text-white"><?php echo $home_cta2_desc; ?></p><?php endif; ?>
                                <?php if ($home_cta2_btn1_text != '' || $home_cta2_btn2_text != '') {
                                    ?>
                                    <div class="cta-btn">   
                                        <?php if ($home_cta2_btn1_text != '') { ?>
                                            <a class="btn-small btn-default"  href="<?php
                                            if ($home_cta2_btn1_link != '') {
                                                echo esc_url($home_cta2_btn1_link);
                                            }
                                            ?>"  <?php
                                               if ($home_cta2_btn1_link_target == true) {
                                                   echo "target='_blank'";
                                               }
                                               ?>><?php echo $home_cta2_btn1_text; ?><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                                           <?php } if ($home_cta2_btn2_text != '') { ?>
                                            <a class="btn-small btn-light" href="<?php
                                               if ($home_cta2_btn2_link != '') {
                                                   echo esc_url($home_cta2_btn2_link);
                                               }
                                               ?>" <?php
                                               if ($home_cta2_btn2_link_target == true) {
                                                   echo "target='_blank'";
                                               }
                                               ?>><?php echo $home_cta2_btn2_text; ?><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                                           <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>                  
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </section>
<?php
$stringa = ob_get_contents();
ob_end_clean();
return $stringa;
}
add_shortcode( 'spiko_cta', 'spiko_cta_callback' );



//SHORTCODE FOR Testimonial
function spiko_testimonial_callback( $atts ){
ob_start();    
ob_start();    
extract(shortcode_atts( array(
      'format' => 'slide',
      'style' => '1',
      'items' =>'1',
      'col' =>'3',
   ), $atts ));
if(strtolower($format)=='slide')
{
    $shortitem='shortitem-'.$items;
}
elseif(strtolower($format)=='grid'){
    $shortitem='shortitem-'.$col;
}else{
    $shortitem='';
}

    switch($col)
    {
        case '1': $col = 'col-md-12 col-sm-12 col-xs-12'; break;
        case '2': $col = 'col-md-6 col-sm-6 col-xs-12'; break; 
        case '3': $col = 'col-md-4 col-sm-6 col-xs-12'; break; 
        default: $col = 'col-md-4 col-sm-6 col-xs-12'; break; 
        
    }


    switch($items)
    {
        case '1': $items = 1; break;
        case '2': $items = 2; break; 
        case '3': $items = 3; break; 
        default: $items = 1 ; break; 
        
    }

$testimonial_style=$style;
$testimonial_options = get_theme_mod('spiko_testimonial_content');
if (empty($testimonial_options))
    {
        if (get_theme_mod('home_testimonial_title') != '' || get_theme_mod('home_testimonial_desc') != '' || get_theme_mod('home_testimonial_name') != '' || get_theme_mod('home_testimonial_thumb') != '')
        {
            $home_testimonial_title = get_theme_mod('home_testimonial_title');
            $home_testimonial_discription = get_theme_mod('home_testimonial_desc');
            $home_testimonial_client_name = get_theme_mod('home_testimonial_name');
            $home_testimonial_designation = get_theme_mod('home_testimonial_designation');
            $home_testimonial_star=get_theme_mod('home_testimonial_star');
            $home_testimonial_link = get_theme_mod('home_testimonial_link');
            $home_testimonial_image = get_theme_mod('home_testimonial_thumb');
            $testimonial_options = json_encode( array(
                                        array(
                                        'title' => !empty($home_testimonial_title) ? $home_testimonial_title : 'Exellent Theme & Very Fast Support',
                                        'text' => !empty($home_testimonial_discription) ? $home_testimonial_discription :'It is a long established fact that a reader will be distracted by the readable content of a page when<br> looking at its layout. The point of using Lorem ipsum dolor sit amet,<br> temp consectetur adipisicing elit.',
                                        'clientname' => !empty($home_testimonial_client_name) ? $home_testimonial_client_name : __('Amanda Smith', 'thecompany-plus'),
                                        'designation' => !empty($home_testimonial_designation) ? $home_testimonial_designation : __('Developer', 'thecompany-plus'),
                                        'home_testimonial_star' => !empty($home_testimonial_star) ? $home_testimonial_star : '4.5',
                                        'link' => !empty($home_testimonial_link) ? $home_testimonial_link : '#',
                                        'image_url' =>  !empty($home_testimonial_image) ? $home_testimonial_image : SPIKOP_PLUGIN_URL . '/inc/images/user/user1.jpg',
                                        'open_new_tab' => 'no',
                                        'id' => 'customizer_repeater_56d7ea7f40b96',
                                        'home_slider_caption' => 'customizer_repeater_star_',
                                        ),
                                    ));
        }
        else
        {
            $home_testimonial_section_title = get_theme_mod('home_testimonial_section_title');
            $home_testimonial_section_discription = get_theme_mod('home_testimonial_section_discription');
            $designation = get_theme_mod('designation');
            $home_testimonial_thumb = get_theme_mod('home_testimonial_thumb');
            $testimonial_options = json_encode(array(
                    array(
                        'title' => 'Exellent Theme & Very Fast Support',
                        'text' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem ipsum dolor sit amet,<br> temp consectetur adipisicing elit.',
                        'clientname' => __('Amanda Smith', 'spiko-plus'),
                        'designation' => __('Developer', 'spiko-plus'),
                        'home_testimonial_star' => '4.5',
                        'link' => '#',
                        'image_url' => SPIKOP_PLUGIN_URL . '/inc/images/user/user1.jpg',
                        'open_new_tab' => 'no',
                        'id' => 'customizer_repeater_77d7ea7f40b96',
                        'home_slider_caption' => 'customizer_repeater_star_4.5',
                    ),
                    array(
                        'title' => 'Exellent Theme & Very Fast Support',
                        'text' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem ipsum dolor sit amet,<br> temp consectetur adipisicing elit.',
                        'clientname' => __('Travis Cullan', 'spiko-plus'),
                        'designation' => __('Team Leader', 'spiko-plus'),
                        'home_testimonial_star' => '5',
                        'link' => '#',
                        'image_url' => SPIKOP_PLUGIN_URL . '/inc/images/user/user2.jpg',
                        'open_new_tab' => 'no',
                        'id' => 'customizer_repeater_88d7ea7f40b97',
                        'home_slider_caption' => 'customizer_repeater_star_5',
                    ),
                    array(
                        'title' => 'Exellent Theme & Very Fast Support',
                        'text' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem ipsum dolor sit amet,<br> temp consectetur adipisicing elit.',
                        'clientname' => __('Victoria Wills', 'spiko-plus'),
                        'designation' => __('Volunteer', 'spiko-plus'),
                        'home_testimonial_star' => '3.5',
                        'link' => '#',
                        'image_url' => SPIKOP_PLUGIN_URL . '/inc/images/user/user3.jpg',
                        'id' => 'customizer_repeater_11d7ea7f40b98',
                        'open_new_tab' => 'no',
                        'home_slider_caption' => 'customizer_repeater_star_3.5',
                    ),
                ));
        }
    }
if(strtolower($format)=="slide")
{    
$testimonial_animation_speed = get_theme_mod('testimonial_animation_speed', 3000);
$testimonial_smooth_speed = get_theme_mod('testimonial_smooth_speed', 1000);
$isRTL = (is_rtl()) ? (bool) true : (bool) false;

$slide_items = $items;
$testimonial_nav_style = get_theme_mod('testimonial_nav_style', 'bullets');
$testimonial_style=$style;
$designId='testimonial-carousel'.$testimonial_style;
$testimonial_settings = array('design_id' => '#' . $designId, 'slide_items' => $slide_items, 'animationSpeed' => $testimonial_animation_speed, 'smoothSpeed' => $testimonial_smooth_speed, 'testimonial_nav_style' => $testimonial_nav_style, 'rtl' => $isRTL);

wp_register_script('spiko-testimonial', SPIKOP_PLUGIN_URL . '/inc/js/front-page/testi.js', array('jquery'));
wp_localize_script('spiko-testimonial', 'testimonial_settings', $testimonial_settings);
wp_enqueue_script('spiko-testimonial');
}
$home_testimonial_section_title = get_theme_mod('home_testimonial_section_title', __('Our Testimonials', 'spiko-plus'));
$home_testimonial_section_discription = get_theme_mod('home_testimonial_section_discription', __('What Client Says About Us', 'spiko-plus'));

$testimonial_callout_background = get_theme_mod('testimonial_callout_background',SPIKOP_PLUGIN_URL.'/inc/images/bg/bg-img.jpg');
$testi_layout=$testimonial_style;
$callout_cta2_background = get_theme_mod('callout_cta2_background', SPIKOP_PLUGIN_URL.'/inc/images/bg/cta-3.jpg');
if ($testimonial_callout_background != '') { ?>
<section class="section-space testimonial slideitem-<?php echo get_theme_mod('home_testimonial_slide_item',1); ?> <?php echo $shortitem;?> <?php if($testimonial_style==2) {?> testi testi-2 <?php } ?> <?php if($testimonial_style==4) {?> testi testi-4 <?php } ?>"  style="background-image:url('<?php echo esc_url($testimonial_callout_background); ?>'); background-repeat: no-repeat; background-position: top left; width: 100%; background-size: cover;">
<?php } else { ?>
<section class="section-space testimonial <?php if($testimonial_style==2) {?> testi testi-2 <?php } ?> <?php if($testimonial_style==4) {?> testi testi-4 <?php } ?>" >
<?php } ?>
    <div class="spiko-tesi-container container">
    <?php if ($home_testimonial_section_title != '' || $home_testimonial_section_discription != '') { ?>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <div class="section-header">
                <?php if ($home_testimonial_section_discription != ''):?>
                    <p class="section-subtitle"><?php echo esc_attr($home_testimonial_section_discription); ?></p>
                <?php endif;?>
                <?php if($home_testimonial_section_title):?>
                    <h2 class="section-title"><?php echo esc_attr($home_testimonial_section_title); ?></h2>
                    <div class="section-separator border-center"></div>
                <?php endif;?>                                       
                </div>
            </div>
        </div>
    <?php if(strtolower($format)=="slide"):?> <div class="owl-carousel owl-theme" id="home"><?php endif;?>
    <?php } ?>
     
        <div class="row" <?php if(strtolower($format)=="grid"):?> id="testimonial-carousel<?php echo $testimonial_style;?>" <?php endif;?>>
        <?php if(strtolower($format)=="slide"):?><div class="col-md-12 <?php if($testimonial_style==4) {?> text-center <?php } ?>"  id="<?php echo $designId; ?>"> <?php endif;
            $testimonial_options = json_decode($testimonial_options);
            if ($testimonial_options != '') {
                $allowed_html = array(
                                        'br' => array(),
                                        'em' => array(),
                                        'strong' => array(),
                                        'b' => array(),
                                        'i' => array(),
                                    );
            foreach ($testimonial_options as $testimonial_iteam) 
            {
                $title = !empty($testimonial_iteam->title) ? apply_filters('spiko_translate_single_string', $testimonial_iteam->title, 'Testimonial section') : '';
                $test_desc = !empty($testimonial_iteam->text) ? apply_filters('spiko_translate_single_string', $testimonial_iteam->text, 'Testimonial section') : '';
                $test_link = $testimonial_iteam->link;
                $open_new_tab = $testimonial_iteam->open_new_tab;
                $clientname = !empty($testimonial_iteam->clientname) ? apply_filters('spiko_translate_single_string', $testimonial_iteam->clientname, 'Testimonial section') : '';
                $designation = !empty($testimonial_iteam->designation) ? apply_filters('spiko_translate_single_string', $testimonial_iteam->designation, 'Testimonial section') : '';
                $stars = !empty($testimonial_iteam->home_testimonial_star) ? apply_filters('spiko_translate_single_string', $testimonial_iteam->home_testimonial_star, 'Testimonial section') : '';
            ?>
                <div class="<?php if(strtolower($format)=="slide") { echo 'item';} else { echo $col.' testi-grid'; } ?>">

                    <?php
                    //Below Code will Run For Testimonial Design 1
                    if($testimonial_style==1) { ?>
                    <div class="testmonial-block">
                    <?php if ($testimonial_iteam->image_url != ''): ?>
                        <figure class="avatar"> <img src="<?php echo $testimonial_iteam->image_url; ?>" class="img-fluid" a> <span class="quotes-seprator"></span> </figure>
                    <?php endif; ?>
                    <?php if (($clientname != '' || $test_desc != '' )) { ?>
                            <div class="entry-content">
                                <?php if (!empty($clientname)): ?>
                                <h4 class="name">
                                    <a href="<?php if (empty($test_link)) { echo '#'; } else { echo esc_url($test_link); } ?>" <?php if ($open_new_tab == "yes") { ?> target="_blank"<?php } ?>>
                                        <?php echo $clientname; ?>
                                    </a>
                                </h4>
                                <?php endif; ?>
                                <?php if (!empty($test_desc)): ?>
                                <p><?php echo wp_kses(html_entity_decode($test_desc), $allowed_html); ?></p>
                                <?php endif; ?>
                            </div>
                    <?php } ?>
                    </div>
                <?php }
                //Below Code will Run For Testimonial Design 2
                elseif($testimonial_style==2 || $testimonial_style==3) {
                ?>
                <blockquote class="testmonial-block">
                    <?php if ($testimonial_iteam->image_url != ''): ?>
                    <figure class="avatar">
                        <img src="<?php echo $testimonial_iteam->image_url; ?>" class="img-fluid rounded-circle" >
                    </figure>
                     <?php endif; ?>
                     <?php if (!empty($test_desc)): ?>
                    <div class="entry-content">
                        <p><?php echo wp_kses(html_entity_decode($test_desc), $allowed_html); ?></p>
                    </div>      
                    <?php endif; 
                    if (($clientname != '' || $designation != '')) { ?>
                        <figcaption>
                          <?php if (!empty($clientname)): ?>
                            <a href="<?php if (empty($test_link)) { echo '#'; } else { esc_url($test_link); } ?>" <?php if ($open_new_tab == "yes") { ?> target="_blank"<?php } ?>>   
                        <cite class="name"><?php echo $clientname; ?></cite></a><?php endif; ?>
                        <?php if (!empty($designation)): ?><span class="designation"><?php echo $designation; ?></span><?php endif; ?>
                    </figcaption>
                    <?php } ?>
                    <?php if (!empty($stars)) { ?>
                     <div class="rating <?php if($testimonial_style==2){ echo 'text-left';}?>">
                        <?php
                        $star_arry=explode('_', $stars);
                        $stars_no_arr = end($star_arry);
                        $star_no = explode('_', $stars_no_arr);
                        $star_end = explode('.', end($star_no));
                        for ($i = 1; $i <= $star_end[0]; $i++) { ?>
                            <span class="fa fa-star"></span>
                        <?php } ?>
                        <?php if (array_keys($star_end)==range(0,1)) { ?>
                            <span class="fa fa-star-half-o"></span>
                        <?php } ?>
                    </div>
                    <?php } ?>
                </blockquote>
                <?php
                }
                //Below Script will run only for Testimonial 4
                else
                {
                ?>
                <blockquote class="testmonial-block">
                    <?php if ($testimonial_iteam->image_url != ''): ?>
                    <figure class="avatar">
                        <img src="<?php echo $testimonial_iteam->image_url; ?>" class="img-fluid rounded-circle" >
                    </figure>
                     <?php endif; ?>
                     <?php
                    if (($clientname != '' || $designation != '')) { ?>
                        <figcaption>
                          <?php if (!empty($clientname)): ?>
                            <a href="<?php if (empty($test_link)) { echo '#'; } else { esc_url($test_link); } ?>" <?php if ($open_new_tab == "yes") { ?> target="_blank"<?php } ?>>   
                        <cite class="name"><?php echo $clientname; ?></cite></a><?php endif; ?>
                        <?php if (!empty($designation)): ?><span class="designation"><?php echo $designation; ?></span><?php endif; ?>
                    </figcaption>
                    <?php } ?>
                     <?php if (!empty($test_desc)): ?>
                    <div class="entry-content">
                        <p><?php echo wp_kses(html_entity_decode($test_desc), $allowed_html); ?></p>
                    </div>      
                    <?php endif; ?>

                    <?php if (!empty($stars)) { ?>
                     <div class="rating <?php if($testimonial_style==2){ echo 'text-left';}?>">
                        <?php
                        $star_arry=explode('_', $stars);
                        $stars_no_arr = end($star_arry);
                        $star_no = explode('_', $stars_no_arr);
                        $star_end = explode('.', end($star_no));
                        for ($i = 1; $i <= $star_end[0]; $i++) { ?>
                            <span class="fa fa-star"></span>
                        <?php } ?>
                        <?php if (array_keys($star_end)==range(0,1)) { ?>
                            <span class="fa fa-star-half-o"></span>
                        <?php } ?>
                    </div>
                    <?php } ?>
                </blockquote>
                <?php    
                }    
                 ?>
                </div>
                    <?php 
                }
            }
            ?>
                <?php if(strtolower($format)=="slide"):?> </div> <?php endif;?>
            </div>
        <?php if(strtolower($format)=="slide"):?> </div> <?php endif;?>
    </div>
</section>
<?php
$stringa = ob_get_contents();
ob_end_clean();
return $stringa;
}
add_shortcode( 'spiko_testimonial', 'spiko_testimonial_callback' );


//SHORTCODE FOR TEAM SECTION
function spiko_team_callback( $atts ){
ob_start();    
extract(shortcode_atts( array(
      'format' => 'slide',
      'style' => '1',
      'items' =>'3',
      'col' =>'3',
   ), $atts ));

switch($col)
{
    case '1': $col = 'col-md-12 col-sm-12 col-xs-12'; break;
    case '2': $col = 'col-md-6 col-sm-6 col-xs-12'; break; 
    case '3': $col = 'col-md-4 col-sm-6 col-xs-12'; break; 
    case '4': $col = 'col-md-3 col-sm-6 col-xs-12'; break; 
    default: $col = 'col-md-4 col-sm-6 col-xs-12'; break; 
    
}

switch($items)
{
    case '1': $items = 1; break;
    case '2': $items = 2; break; 
    case '3': $items = 3; break; 
    case '4': $items = 4; break; 
    default: $items = 3 ; break; 
    
}

$team_options = get_theme_mod('spiko_team_content');
if (empty($team_options)) {
    $team_options = starter_team_json();
}
if(strtolower($format)=="slide"):
$team_animation_speed = get_theme_mod('team_animation_speed', 3000);
$team_smooth_speed = get_theme_mod('team_smooth_speed', 1000);
$team_nav_style = get_theme_mod('team_nav_style', 'bullets');
$isRTL = (is_rtl()) ? (bool) true : (bool) false;
$team_items=$items;
$teamsettings = array( 'team_items' => $team_items,'team_animation_speed' => $team_animation_speed, 'team_smooth_speed' => $team_smooth_speed, 'team_nav_style' => $team_nav_style, 'rtl' => $isRTL);
wp_register_script('spiko-team', SPIKOP_PLUGIN_URL . '/inc/js/front-page/team.js', array('jquery'));
wp_localize_script('spiko-team', 'team_settings', $teamsettings);
wp_enqueue_script('spiko-team');
endif;?>
<section class="section-space team <?php if(strtolower($style)=="2"):?> team-common team2 <?php endif;?> <?php if(strtolower($style)=="3"):?> team3 team-common <?php endif;?> <?php if(strtolower($style)=="4"):?> team4 team-common <?php endif;?> bg-default">
    <div class="spiko-team-container container">
        <?php
        $home_team_section_title = get_theme_mod('home_team_section_title', __('The Team', 'spiko-plus'));
        $home_team_section_discription = get_theme_mod('home_team_section_discription', __('Meet Our Experts', 'spiko-plus'));
        if (($home_team_section_title) || ($home_team_section_discription) != '') {?>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <div class="section-header">
                <?php
                if (!empty($home_team_section_discription)): ?>
                    <p class="section-subtitle"><?php echo $home_team_section_discription; ?></p>
                <?php
                endif;
                if (!empty($home_team_section_title)):?>
                    <h2 class="section-title"><?php echo $home_team_section_title; ?></h2>
                    <div class="section-separator border-center"></div>
                <?php endif; ?>
                </div>
            </div>                      
        </div>
        <?php } ?>
        <div class="row">
            <?php if(strtolower($format)=="slide"):?><div id="team-carousel" class="owl-carousel owl-theme col-lg-12"><?php endif;?>
                <?php
                $team_options = json_decode($team_options);
                if ($team_options != '') {
                    foreach ($team_options as $team_item) {
                        $image = !empty($team_item->image_url) ? apply_filters('spiko_translate_single_string', $team_item->image_url, 'Team section') : '';
                        $image2 = !empty($team_item->image_url2) ? apply_filters('spiko_translate_single_string', $team_item->image_url2, 'Team section') : '';
                        $title = !empty($team_item->membername) ? apply_filters('spiko_translate_single_string', $team_item->membername, 'Team section') : '';
                        $subtitle = !empty($team_item->designation) ? apply_filters('spiko_translate_single_string', $team_item->designation, 'Team section') : '';
                        $aboutme = !empty($team_item->text) ? apply_filters('spiko_translate_single_string', $team_item->text, 'Team section') : '';
                       // $link = !empty($team_item->link) ? apply_filters('spiko_translate_single_string', $team_item->link, 'Team section') : '';
                        $open_new_tab = $team_item->open_new_tab;
                        ?>
                <div class="<?php if(strtolower($format)=="slide") { echo 'item';} else { echo $col .' '.'team-grid-col'; } ?>">
                    <div class="team-grid text-center" tabindex="0" id="A1">

                        <?php if(strtolower($style)=="4") { ?>   
                            <div class="overlay">      
                        <?php } if(!empty($image)){ ?>
                        <div class="img-holder"> <img src="<?php echo esc_url($image); ?>" class="img-fluid"></div>
                        <?php } 
                        if(strtolower($style)=="4"){
                        //Below Script will run Only For Design 4   
                        $icons = html_entity_decode($team_item->social_repeater);
                            $icons_decoded = json_decode($icons, true);
                            $socails_counts = $icons_decoded;
                            if (!empty($socails_counts)) :
                                if (!empty($icons_decoded)) : ?>
                                    <ul class="list-inline list-unstyled ml-0 mt-3 mb-1" id="A2">
                                        <?php
                                        foreach ($icons_decoded as $value) 
                                        {
                                        $social_icon = !empty($value['icon']) ? apply_filters('spiko_translate_single_string', $value['icon'], 'Team section') : '';
                                        $social_link = !empty($value['link']) ? apply_filters('spiko_translate_single_string', $value['link'], 'Team section') : '';
                                            if (!empty($social_icon)) 
                                            { ?>                          
                                                <li class="list-inline-item"><a class="p-2 fa-lg fb-ic" <?php if ($open_new_tab == 'yes') { echo 'target="_blank"'; } ?> href="<?php echo esc_url($social_link); ?>" class="btn btn-just-icon btn-simple"><i class="fa <?php echo esc_attr($social_icon); ?> " aria-hidden="true"></i></a></li>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                            <?php
                                endif;
                            endif;   
                        }
                        if(strtolower($style)=="4") { ?>  </div> <?php } ?>
                        <div class="card-body">
                            <?php if (!empty($title)) : ?>
                            <h4 class="<?php if((strtolower($style)!="3") && (strtolower($style)!="4")):?>name<?php endif;?> mt-1 mb-2"><?php echo esc_html($title); ?></h4>
                            <?php endif; 
                            if (!empty($subtitle)) : ?>
                            <p class="mt-1 mb-2"><?php echo esc_html($subtitle); ?></p>
                            <?php endif;  
                            //Below Script will run apart Design 4
                            if(strtolower($style)!="4"){                         
                            $icons = html_entity_decode($team_item->social_repeater);
                            $icons_decoded = json_decode($icons, true);
                            $socails_counts = $icons_decoded;
                            if (!empty($socails_counts)) :
                                if (!empty($icons_decoded)) : ?>
                                    <ul class="list-inline list-unstyled ml-0 mt-3 mb-1" id="A2">
                                        <?php
                                        foreach ($icons_decoded as $value) 
                                        {
                                        $social_icon = !empty($value['icon']) ? apply_filters('spiko_translate_single_string', $value['icon'], 'Team section') : '';
                                        $social_link = !empty($value['link']) ? apply_filters('spiko_translate_single_string', $value['link'], 'Team section') : '';
                                            if (!empty($social_icon)) 
                                            { ?>                          
                                                <li class="list-inline-item"><a class="p-2 fa-lg fb-ic" <?php if ($open_new_tab == 'yes') { echo 'target="_blank"'; } ?> href="<?php echo esc_url($social_link); ?>" class="btn btn-just-icon btn-simple"><i class="fa <?php echo esc_attr($social_icon); ?> " aria-hidden="true"></i></a></li>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                            <?php
                                endif;
                            endif;
                        }
                            ?>        
                        </div>                      
                    </div>
                </div>  
                <?php 
                    }
                } ?>    
            <?php if(strtolower($format)=='slide'):?></div><?php endif;?>
        </div>
    </div>
</section>
<?php

$stringa = ob_get_contents();
ob_end_clean();
return $stringa;
}
add_shortcode( 'spiko_team', 'spiko_team_callback' );


//SHORTCODE FOR FUN FACT SECTION
function spiko_funfact_callback($atts){
    ob_start();
    extract(shortcode_atts( array(
      'col' => '4',
   ), $atts ));
    switch($col)
    {   
        case '1': $col = 'col-md-12 col-sm-12 col-xs-12'; break;
        case '2': $col = 'col-md-6 col-sm-6 col-xs-12'; break; 
        case '3': $col = 'col-md-4 col-sm-6 col-xs-12'; break; 
        case '4': $col = 'col-md-3 col-sm-6 col-xs-12'; break; 
        default:$col = 'col-md-3 col-sm-6 col-xs-12'; break; 
    } 
 
$home_fun_title = get_theme_mod('home_fun_section_title', __('<span>Doing the right thing,</span><br> at the right time.', 'spiko-plus'));
$funfact_data = get_theme_mod('spiko_funfact_content');
if (empty($funfact_data)) {
    $funfact_data = starter_funfact_json();
}    

$fun_callout_background = get_theme_mod('funfact_callout_background', SPIKOP_PLUGIN_URL .'inc/images/bg/funfact-bg.jpg');
if ($fun_callout_background != '') {
    ?>
    <section class="section-space funfact bg-default"  style="background-image:url('<?php echo esc_url($fun_callout_background); ?>'); background-repeat: no-repeat; width: 100%; background-size: cover;">
    <?php
    } else {
        ?>
        <section class="section-space funfact">
            <?php
        }
        ?>

         <?php 
            $funfact_overlay_section_color = get_theme_mod('funfact_overlay_section_color', 'rgba(0, 11, 24, 0.8)');
$funfact_image_overlay = get_theme_mod('funfact_image_overlay', true);
            if($funfact_image_overlay != false) 
        { ?>
                        <div class="overlay" style="background-color: <?php echo $funfact_overlay_section_color; ?>">
                <?php } else { ?>
    <div class="overlay">
    <?php } ?>
        <div class="spiko-fun-container container">
            <div class="row">
                <?php
                $funfact_data = json_decode($funfact_data);
                if (!empty($funfact_data)) {
                    $fun_id=1;
                    foreach ($funfact_data as $funfact_iteam) {
                        $funfact_title = !empty($funfact_iteam->title) ? apply_filters('spiko_translate_single_string', $funfact_iteam->title, 'Funfact section') : '';
                        $funfact_text = !empty($funfact_iteam->text) ? apply_filters('spiko_translate_single_string', $funfact_iteam->text, 'Funfact section') : '';
                        $funfact_icon = !empty($funfact_iteam->icon_value) ? apply_filters('spiko_translate_single_string', $funfact_iteam->icon_value, 'Funfact section') : '';?>
                            <div class="<?php echo $col;?>">
                                <div class="funfact-inner">
                                    <?php if ($funfact_title != '') { ?>
                                    <h4 class="funfact-title mb-0 count<?php echo $fun_id;?>" data-from="0" data-to="<?php echo $funfact_title; ?>" data-time="2000"><?php echo $funfact_title; ?></h4>
                                    <?php } ?>
                                    <?php if ($funfact_text != '') { ?>
                                    <p class="description text-uppercase font-weight-normal mb-1"><?php echo $funfact_text; ?></p>
                                    <?php } ?>
                                </div>
                            </div>
                <?php
                $fun_id++;
                        }
                    }
                ?>
            </div>
        </div>
    </div>
</section>
<?php 
$stringa = ob_get_contents();
ob_end_clean();
return $stringa; 
}
add_shortcode( 'spiko_funfact', 'spiko_funfact_callback' );


//SHORTCODE FOR CLIENT SECTION
function spiko_client_callback($atts){
ob_start();
extract(shortcode_atts( array(
  'items' => '5',
), $atts ));
switch($items)
{   
    case '1': $items = '1'; break;
    case '2': $items = '2'; break; 
    case '3': $items = '3'; break; 
    case '4': $items = '4'; break;
    case '5': $items = '5'; break; 

    default:$items = '5'; break; 
}  
$isRTL = (is_rtl()) ? (bool) true : (bool) false;
$sponsorssettings = array( 'client_items' => $items,'rtl' => $isRTL);
wp_register_script('spiko-sponsors', SPIKOP_PLUGIN_URL . '/inc/js/front-page/sponsors.js', array('jquery'));
wp_localize_script('spiko-sponsors', 'sponsorssettings', $sponsorssettings);
wp_enqueue_script('spiko-sponsors');

$client_options = get_theme_mod('spiko_clients_content');
$clients_title = get_theme_mod('home_client_section_title', __('We Work With The Best Clients', 'spiko-plus'));
$client_subtitle = get_theme_mod('home_client_section_discription', __('Our Sponcers', 'spiko-plus'));
$clt_bg_color = get_theme_mod('clt_bg_color', '');
if ($clt_bg_color != '') { ?>
<section class="section-space sponsors bg-default-color"  style="background-color: <?php echo $clt_bg_color; ?>">
<?php } 
else { ?>
<section class="section-space sponsors bg-default-color" >
<?php } ?>
    <div class="spiko-client-container container">
        <?php if (!empty($clients_title) || !empty($client_subtitle)): ?>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">     
                <div class="section-header">
                <?php if (!empty($client_subtitle)): ?>
                    <p class="section-subtitle"><?php echo $client_subtitle; ?></p>
                <?php
                endif;
                if (!empty($clients_title)):?>
                    <h2 class="section-title"><?php echo $clients_title; ?></h2>
                    <div class="section-separator border-center"></div>
                <?php endif; ?>
                </div>  
            </div>
        </div>
        <?php endif; ?>
        <div class="row">
            <div id="clients-carousel" class="owl-carousel owl-theme col-md-12">
                <?php
                $t = true;
                $client_options = json_decode($client_options);
                        if ($client_options != '') {
                            foreach ($client_options as $client_iteam) {
                                $client_image = !empty($client_iteam->image_url) ? apply_filters('spiko_translate_single_string', $client_iteam->image_url, 'Client section') : '';
                                $client_link = !empty($client_iteam->link) ? apply_filters('spiko_translate_single_string', $client_iteam->link, 'Client section') : '';
                                $open_new_tab = $client_iteam->open_new_tab;
                                ?>      
                                <div class="item">      
                                    <figure <?php if ($client_image != '') { ?>class="logo-scroll"<?php } ?>>
                                        <?php
                                        if (empty($client_link)) {
                                            ?>
                                            <img src="<?php echo $client_image; ?>" class="img-fluid" >
                                            <?php
                                        } else {
                                            ?>
                                            <a href="<?php echo esc_url($client_link); ?>" <?php
                                            if ($open_new_tab == 'yes') {
                                                echo 'target="_blank"';
                                            }
                                            ?>>
                                                <img src="<?php echo $client_image; ?>" class="img-fluid px-4" >
                                            </a>
                                            <?php
                                        }
                                        ?>
                                    </figure>
                                </div>      
                                <?php
                            }
                        }
                        else {
                            for ($i = 1; $i <= 6; $i++) {
                                ?>  
                                <div class="item">
                                    <figure class="logo-scroll">
                                        <a href="#"><img src="<?php
                                            echo SPIKOP_PLUGIN_URL . '/inc/images/sponsors/client-' . $i . '.png';
                                            ?>" class="img-fluid px-4" alt="Sponsors <?php echo $i; ?>"></a>
                                    </figure>
                                </div>
                                <?php
                            }
                        }
                        ?>
            </div>
        </div>
    </div>
</section>
<?php
$stringa = ob_get_contents();
ob_end_clean();
return $stringa; 
}
add_shortcode( 'spiko_client', 'spiko_client_callback' );


//SHORTCODE FOR Portfolio SECTION
function spiko_portfolio_shortcodes($atts){
ob_start();
extract(shortcode_atts( array(
    'filter' => 'on',
    'sidebar' => 'off',
    'margin' => 'on',
    'col' => '3',
    'pagination' => 'on',
    'projects_per_page' => '4',
), $atts ));
if(($col=='4')&&(strtolower($sidebar)!='off')){
    $port_class='port-4';
}else{
     $port_class='';
}
switch(($col))
    {   
        case '1': $col = 'col-md-12 col-sm-12 col-xs-12'; break;
        case '2': $col = 'col-md-6 col-sm-6 col-xs-12'; break; 
        case '3': $col = 'col-md-4 col-sm-6 col-xs-12'; break; 
        case '4': $col = 'col-md-3 col-sm-6 col-xs-12'; break; 
        default:$col = 'col-md-3 col-sm-6 col-xs-12'; break; 
    }  
switch(strtolower($sidebar))
    {   
        case 'left': $sidebar = 'left'; break;
        case 'right': $sidebar = 'right'; break;
        case 'off': $sidebar = 'off'; break;          
        default:$sidebar = 'off'; break; 
    }   
$post_type = 'spiko_portfolio';
$tax = 'portfolio_categories';
$term_args = array('hide_empty' => true, 'orderby' => 'id');
$posts_per_page = $projects_per_page;
$tax_terms = get_terms($tax, $term_args);
$defualt_tex_id = get_option('spiko_default_term_id');
$j = 1;$tab='';
if(isset($_GET['tab'])):
    $tab = $_GET['tab'];
endif;
if (isset($_GET['div'])) {
    $tab = $_GET['div'];
}
$porfolio_page_title = get_theme_mod('porfolio_page_title', __('Our Portfolio', 'spiko-plus'));
$porfolio_page_subtitle = get_theme_mod('porfolio_page_subtitle', __('Our Recent Works', 'spiko-plus'));
global $template;?>
<section class="section-space portfolio portfolio-page bg-default <?php echo $port_class; if(strtolower($margin)=='off'){ echo 'portfolio-gallery'; }?> ">
    <div class="container<?php echo esc_html(spiko_container());?>">
        <?php  if($sidebar=="left" && (basename($template)=='template-shortcode.php')) { ?> 
        <div class="row">
            <!--Sidebar-->
            <div class="col-lg-4 col-md-5 col-sm-12 sidebar">
                <div class="sidebar s-r-space">
                    <?php dynamic_sidebar('sidebar-1'); ?>
                </div>
            </div>
            <!--/Sidebar-->
            <div class="col-lg-8 col-md-7 col-sm-12">
        <?php }?>
        <?php  if($sidebar=="right" && (basename($template)=='template-shortcode.php')) { ?>  
        <div class="row">
            <div class="col-lg-8 col-md-7 col-sm-12">
        <?php } ?>
        <?php if (!empty($porfolio_page_title) || !empty($porfolio_page_subtitle)): ?>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="section-header">
                        <?php if (!empty($porfolio_page_subtitle)): ?><h5 class="section-subtitle"><?php echo $porfolio_page_subtitle; ?></h5><?php endif; ?>
                        <?php if (!empty($porfolio_page_title)): ?><h2 class="section-title"><?php echo $porfolio_page_title; ?></h2>
                        <div class="section-separator border-center"></div>
                        <?php endif; ?>
                        
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <!-- Portfolio Filter -->

        <?php if(strtolower($filter)=='on'){
        if ($tax_terms) : ?>
            <div class="row">
                <div class="col-12">
                    <ul id="tabs" class="nav md-pills flex-center flex-wrap mx-0" role="tablist">
                        <?php foreach ($tax_terms as $tax_term) { ?>
                            <li rel="tab" class="nav-item" ><span class="tab">
                                    <a id="tab-<?php echo rawurldecode($tax_term->slug); ?>" href="#<?php echo rawurldecode($tax_term->slug); ?>"  class="nav-link <?php
                                    if ($tab == '') {
                                        if ($j == 1) {
                                            echo 'active';
                                            $j = 2;
                                        }
                                    } else if ($tab == rawurldecode($tax_term->slug)) {
                                        echo 'active';
                                    }
                                    ?> text-uppercase" data-toggle="tab" role="tab"><?php echo $tax_term->name; ?></a></span>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        <?php endif; 
        }?>

        <div align="center" id="myDiv" style="display:none;">
            <img id="loading-image" width="120" src="<?php echo esc_url(SPIKOP_PLUGIN_URL.'/inc/images/loading.gif');?>"  />
            <p>Loading...</p>
        </div>

        <!--Tab panels-->
        
        <div id="content" class="tab-content <?php if(strtolower($filter)=='off'): echo 'pt-0'; endif ?>" role="tablist">
            <?php
            global $paged;
            $curpage = $paged ? $paged : 1;
            $is_active = true;
            if ($tax_terms) {
                foreach ($tax_terms as $tax_term) {
                    
                    $args = array(
                        'post_type' => $post_type,
                        'post_status' => 'publish',
                        'portfolio_categories' => $tax_term->slug,
                        'posts_per_page' => $posts_per_page,
                        'paged' => $curpage,
                        'orderby' => 'DESC',
                    );
                    $portfolio_query = null;
                    $portfolio_query = new WP_Query($args);
                    if ($portfolio_query->have_posts()):
                        ?>
                        <div id="<?php echo rawurldecode($tax_term->slug); ?>" class="tab-pane fade show in <?php
                        if ($tab == '') {
                            if ($is_active == true) {
                                echo 'active';
                            }$is_active = false;
                        } else if ($tab == rawurldecode($tax_term->slug)) {
                            echo 'active';
                        }
                        ?>" role="tabpanel" aria-labelledby="tab-<?php echo rawurldecode($tax_term->slug); ?>">
                            <div class="row portfolio-space">
                                <?php
                                while ($portfolio_query->have_posts()) : $portfolio_query->the_post();
                                    $portfolio_target = sanitize_text_field(get_post_meta(get_the_ID(), 'portfolio_target', true));
                                        $portfolio_description = get_the_content();
                                    if (get_post_meta(get_the_ID(), 'portfolio_link', true)) {
                                        $portfolio_link = get_post_meta(get_the_ID(), 'portfolio_link', true);
                                    } else {
                                        $portfolio_link = '';
                                    }
                                    echo '<div class="' . $col . '">';
                                    ?>
                                    <figure class="portfolio-thumbnail">
                                                    <?php
                                                    the_post_thumbnail('full', array(
                                                        'class' => 'card-img-top img-fluid',
                                                        'alt'   => get_the_title(),
                                                    ));
                                                    if (has_post_thumbnail()) {
                                                        $post_thumbnail_id = get_post_thumbnail_id();
                                                        $post_thumbnail_url = wp_get_attachment_url($post_thumbnail_id);
                                                    }

                                                    if (!empty($portfolio_link)) {
                                                        $tagt = (!empty($portfolio_target)) ? 'target="_blank"' : '';

                                                        $portlink = '<a href=' . "$portfolio_link" . ' title=' . get_the_title() . ' ' . $tagt . ' >' . get_the_title() . '</a>';
                                                    } else {
                                                        $tagt = (!empty($portfolio_target)) ? 'target="_blank"' : '';
                                                        $portlink = '<a href="#" title=' . get_the_title() . ' ' . $tagt . ' >' . get_the_title() . '</a>';
                                                    }
                                                    $modelId = get_the_ID() . '_model'.rawurldecode($tax_term->slug);
                                                    ?>
                                                    <figcaption>
                                                    <?php if(!empty($portlink)):?>
                                                        <div class="entry-header">
                                                            <h4 class="entry-title">
                                                                <?php echo $portlink; ?>
                                                            </h4>
                                                        </div>
                                                    <?php endif;
                                                    $tax_string=implode(" ",get_the_taxonomies());
                                                    $tax_cat=str_replace( array( 'Categories:'), ' ', $tax_string);?>
                                                        <p class="taxonomy-list"><?php  echo $tax_cat;?></p>
                                                    </figcaption>        
                                                    <a data-toggle="modal" data-target="#<?php echo $modelId; ?>"><i>+</i></a>

                                    </figure>   
                                    <?php echo '</div>';?>
                                    <div class="modal fade" id="<?php echo $modelId; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-body p-0">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>

                                                    <!-- Grid row -->
                                                    <div class="row">
                                                        <?php
                                                        if ($post_thumbnail_url) {
                                                            if (!$portfolio_description) {
                                                                $ImgGridColumn = 'col-md-12';
                                                            } else {
                                                                $ImgGridColumn = 'col-md-6';
                                                            }
                                                            if ($portfolio_description) {
                                                                ?>
                                                                <!-- Grid column -->
                                                                <div class="col-md-6 py-5 pl-5">

                                                                    <article class="post text-center">
                                                                        <div class="entry-header">
                                                                            <h2 class="entry-title">                                                                
                                                                                <?php echo $portlink; ?>
                                                                            </h2>

                                                                        </div>
                                                                        <div class="entry-content">
                                                                            <p><?php the_content(); ?></p>
                                                                        </div>
                                                                    </article>

                                                                </div>
                                                                <!-- Grid column -->
                                                            <?php } ?>
                                                            <!-- Grid column -->
                                                            <div class="<?php echo $ImgGridColumn; ?> port-view">

                                                                <div class="view rounded-right">
                                                                    <img class="img-fluid" src="<?php echo $post_thumbnail_url; ?>" alt="Sample image">
                                                                </div>

                                                            </div>
                                                            <!-- Grid column -->
                                                        <?php } ?>
                                                    </div>
                                                    <!-- Grid row -->

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endwhile; ?>
                             </div>
                             <?php
                             if(strtolower($pagination)=='on'){
                                $total = $portfolio_query->found_posts;
                                $Spiko_Portfolio_Page = new Spiko_Portfolio_Page();
                                $Spiko_Portfolio_Page->Spiko_Port_Page($curpage, $portfolio_query, $total, $posts_per_page);
                            }
                                wp_reset_query();?>
                            </div>
                        <?php
                        
                    else:
                        ?>
                        <div id="<?php echo rawurldecode($tax_term->slug); ?>" class="tab-pane fade in <?php
                             if ($tab == '') {
                                 if ($is_active == true) {
                                     echo 'active';
                                 }$is_active = false;
                             } else if ($tab == rawurldecode($tax_term->slug)) {
                                 echo 'active';
                             }
                             ?>"></div>
                         <?php
                         endif;
                     }
                 }
                 ?> 
    </div>      
    <?php  if($sidebar=="left" && (basename($template)=='template-shortcode.php')) { ?>
                </div>
            </div>
                <?php  } ?>
                <?php  if($sidebar=="right" && (basename($template)=='template-shortcode.php')) { ?>
                </div>
                    <!--Sidebar Widgets-->
                    <div class="col-lg-4 col-md-5 col-sm-12 sidebar">
                        <div class="sidebar s-r-space">
                            <?php dynamic_sidebar('sidebar-1');?>
                        </div>
                    </div>
                    <!--/Sidebar Widgets-->
            </div>
                <?php } ?>
        </div>      
</section>
<script type="text/javascript">
    jQuery('.lightbox').hide();
    jQuery('#lightbox').hide();
    jQuery(".tab .nav-link ").click(function (e) {
        jQuery("#lightbox").remove();
        var h = decodeURI(jQuery(this).attr('href').replace(/#/, ""));
        var tjk = "<?php the_title(); ?>";
        var str1 = tjk.replace(/\s+/g, '-').toLowerCase();
        var pageurl = "<?php $structure = get_option('permalink_structure');if ($structure == ''){echo get_permalink()."&tab=";}else{echo get_permalink()."?tab=";}?>"+h;
        jQuery.ajax({url: pageurl, beforeSend: function () {
                jQuery(".tab-content").hide();
                jQuery("#myDiv").show();
            }, success: function (data){
                jQuery(".tab-content").show();
                jQuery('.lightbox').remove();
                jQuery('#lightbox').remove();
                jQuery('#wrapper').html(data);
            }, complete: function (data) {
                jQuery("#myDiv").hide();
            }
        });
        if (pageurl != window.location) {
            window.history.pushState({path: pageurl}, '', pageurl);
        }
        return false;
    });
</script>
<?php
$stringa = ob_get_contents();
ob_end_clean();
return $stringa; 
}
add_shortcode( 'spiko_portfolio', 'spiko_portfolio_shortcodes' );

// Contact Style Function
function google_map(){?>
<div class="col-lg-12 col-md-12 col-sm-12"> 
    <div id="google-map">
       <?php echo do_shortcode(get_theme_mod('contact_google_map_shortcode'));?>
    </div>
</div>
<?php
}

function contact_form_shortcode(){?>
<div class="col-lg-12 col-md-12">
    <div class="contant-form">
     <?php echo do_shortcode(get_theme_mod('contact_form_shortcode'));?>
    </div>
</div>
<?php
}
function contact_info1(){
    $contact_dt_title = get_theme_mod('contact_dt_title', __("General Enquiries", 'spiko-plus'));
    if(!empty($contact_dt_title)):?>
      <div class="section-header text-left">
          <h2 class="section-title"><?php echo $contact_dt_title;?></h2>
      </div>
   <?php endif;?>
        <div class="cont-info">
            <?php 
            $contact_data=content_contact_data('spiko_plus_contact_content');
            $contact_data = json_decode($contact_data);
            if (!empty($contact_data))
            { 
                foreach($contact_data as $contact_item)
                { 
                    $contact_title = ! empty( $contact_item->title ) ? apply_filters( 'spiko_plus_translate_single_string', $contact_item->title, 'Contact section' ) : '';

                    $contact_text = ! empty( $contact_item->text ) ? apply_filters( 'spiko_plus_translate_single_string', $contact_item->text, 'Contact section' ) : '';

                    $contact_icon = ! empty( $contact_item->icon_value ) ? apply_filters( 'spiko_plus_translate_single_string', $contact_item->icon_value, 'Contact section' ) : '';?>
                    <aside class="contact-widget">
                        <div class="media">
                            <?php if($contact_icon):?>
                            <div class="contact-icon">
                                <i class="fa <?php echo $contact_icon; ?>" aria-hidden="true"></i>
                            </div>
                             <?php endif;?>
                            <div class="media-body">
                                <?php if($contact_title):?><h4 class="title"><?php echo $contact_title; ?></h4><?php endif;?>
                                <?php if($contact_text):?>
                                <address><?php echo $contact_text; ?></address>
                                <?php endif;?>
                            </div>
                        </div>
                    </aside>
                <?php }  
            }?>                    
         </div>
    <?php
         }
function contact_info2($style){
    $contact_dt_title = get_theme_mod('contact_dt_title', __("General Enquiries", 'spiko-plus'));
    if(!empty($contact_dt_title)):?>
        <div class="row">
           <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="section-header <?php if($style!=1  ){echo '';}else{ echo 'text-left';}?>">
                  <h2 class="section-title"><?php echo $contact_dt_title;?></h2>
               </div>
            </div>
        </div>
     <?php endif;?>
    <div class="row cont-info">
        <?php 
        $contact_data=content_contact_data('spiko_plus_contact_content');
        $contact_data = json_decode($contact_data);
        if (!empty($contact_data))
        { 
            foreach($contact_data as $contact_item)
            { 
                $contact_title = ! empty( $contact_item->title ) ? apply_filters( 'spiko_plus_translate_single_string', $contact_item->title, 'Contact section' ) : '';

                $contact_text = ! empty( $contact_item->text ) ? apply_filters( 'spiko_plus_translate_single_string', $contact_item->text, 'Contact section' ) : '';

                $contact_icon = ! empty( $contact_item->icon_value ) ? apply_filters( 'spiko_plus_translate_single_string', $contact_item->icon_value, 'Contact section' ) : '';?>
                <div class="col-lg-3 col-md-3">
                    <aside class="contact-widget">
                        <?php
                         if($contact_icon):?><i class="fa  <?php echo $contact_icon;?>" aria-hidden="true"></i><?php endif;
                         if($contact_title):?><h4 class="title"><?php echo $contact_title;?> </h4> <?php endif;
                         if($contact_text):?><address><?php echo $contact_text;?></address><?php endif;?>
                    </aside>
                </div>  
            <?php }  
        }?>                    
    </div>
    <?php
} 
//SHORTCODE FOR CONTACT SECTION
function spiko_contact_shortcodes($atts){
ob_start();
extract(shortcode_atts( array('style' => '1' ), $atts ));
switch(strtolower($style))
    {   
        case '1': $style = 1; break;
        case '2': $style = 2; break; 
        case '3': $style = 3; break; 
        case '4': $style = 4; break; 
        default:$style = 1; break; 
    }  
$contact_cf7_title = get_theme_mod('contact_cf7_title', __("Don't Hesitate To Contact Us", 'spiko-plus'));
$contact_dt_title = get_theme_mod('contact_dt_title', __("General Enquiries", 'spiko-plus'));


if($style==1  ){$contact_title_class='';}else{ $contact_title_class='text-left';}
$contact_title='<div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="section-header '.esc_attr($contact_title_class,'spiko-plus').'">
                        <h2 class="section-title">'.$contact_cf7_title.'</h2>
                    </div>
                </div>';


if($style==1):?>
<!-- Google Map Section -->
<?php if(get_theme_mod('contact_google_map_shortcode')):?>
<section class="contact-form-map">
    <div class="row">   
        <?php google_map();?>    
    </div>
</section>
<?php endif;?>
<!-- /End of Google Map Section -->

<section class="section-space contact-info bg-default">
            <div class="container<?php echo esc_html(spiko_container());?>">     
                 <div class="row">
                       <div class="col-lg-4 col-md-4">
                            <?php contact_info1();?>
                       </div>
                       <?php if(get_theme_mod('contact_form_shortcode')):?> 
                       <div class="col-lg-8 col-md-8">
                            <?php 
                            if(!empty($contact_cf7_title)):
                                echo $contact_title;
                            endif;?>
                            <div class="row">
                                  <?php contact_form_shortcode();?>
                            </div>
                       </div>
                       <?php endif;?>
                 </div>
            </div>
        </section>
<?php
elseif($style==2):?>
    <section class="section-space contact contact-1 bg-default">
             <div class="container<?php echo esc_html(spiko_container());?>">               
                <?php contact_info2($style);?>
            </div>
    </section>

    <section class="section-space contact-form contact1 bg-default">
        <div class="container<?php echo esc_html(spiko_container());?>">     
             <div class="row">
                   <div class="col-lg-6 col-md-6">
                        <!--Contact map section-->
                        <?php if(get_theme_mod('contact_google_map_shortcode')):?>
                        <section class="contact-form-map">
                            <div class="row">
                               <?php  google_map();?>
                             </div>
                       </section>
                       <?php endif;?>
                        <!--/Contact map section-->
                   </div>
                   <div class="col-lg-6 col-md-6">
                    <?php if(!empty($contact_cf7_title)):
                      echo $contact_title;  
                    endif;?>
                        <div class="row">
                              <?php contact_form_shortcode();?>
                        </div>
                   </div>
             </div>
        </div>
    </section>
<?php
elseif($style==3):?>   
    <section class="section-space contact-form contact2 bg-default">
        <div class="container<?php echo esc_html(spiko_container());?>">     
             <div class="row">               
                   <div class="col-lg-6 col-md-6">
                        <?php if(!empty($contact_cf7_title)):
                            echo $contact_title;
                        endif;?>
                        <div class="row">
                            <?php contact_form_shortcode();?>
                        </div>
                   </div>
                   <div class="col-lg-6 col-md-6">
                        <!--Contact map section-->
                        <?php if(get_theme_mod('contact_google_map_shortcode')):?>
                            <section class="contact-form-map">
                                <div class="row">
                                   <?php google_map();?>
                                </div>
                           </section>
                       <?php endif;?>
                        <!--/Contact map section-->
                   </div>
             </div>
        </div>
    </section>

    <section class="section-space contact contact-2 bg-default">
        <div class="container<?php echo esc_html(spiko_container());?>">        
            <?php contact_info2($style);?>
        </div>
    </section>   
<?php 
elseif($style==4):?>
    <section class="section-space contact contact-3 bg-default">
         <div class="container<?php echo esc_html(spiko_container());?>"> 
         <?php contact_info2($style);?>
         </div>
    </section>

     <section class="section-space contact-form contact2 bg-default">
        <div class="container<?php echo esc_html(spiko_container());?>">   
             <?php if(!empty($contact_cf7_title)):?>  
                    <div class="row">
                       <?php echo $contact_title;?>
                  </div>
            <?php endif;?>
            <div class="row">
                  <?php contact_form_shortcode();?>
            </div>
        </div>
    </section>   

    <?php if(get_theme_mod('contact_google_map_shortcode')):?>
        <section class="contact-form-map">
            <div class="row">
               <?php google_map();?>
             </div>
       </section>  
    <?php endif; 
endif; 
$stringa = ob_get_contents();
ob_end_clean();
return $stringa;
 
}
add_shortcode( 'spiko_contact', 'spiko_contact_shortcodes' );