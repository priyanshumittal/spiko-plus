<?php // Adding customizer layout manager settings

class WP_spiko_plus_layout_Customize_Control extends WP_Customize_Control {	

	public $type = 'new_menu';
	
	public function render_content() {
		
		$front_page = get_theme_mod('front_page_data','services,cta,portfolio,testimonial,team,news,fun,wooproduct,client');


		$data_enable = explode(",",$front_page);	
		$defaultenableddata=array('services','cta','portfolio','testimonial','team','news','fun','wooproduct','client');
		$layout_disable=array_diff($defaultenableddata,$data_enable);  
		?>
		
		<h3><?php esc_attr_e('Enable','spiko-plus'); ?></h3>
		  <ul class="sortable customizer_layout" id="enable">
		  <?php if( !empty($data_enable[0]) )    { foreach( $data_enable as $value ){ ?>
		  <li class="ui-state" id="<?php echo $value; ?>"><?php echo $value; ?></li>
		  <?php } } ?>
		  </ul>
  
  
		 <h3><?php esc_attr_e('Disable','spiko-plus'); ?></h3> 
		 <ul class="sortable customizer_layout" id="disable">
		 <?php if(!empty($layout_disable)){ foreach($layout_disable as $val){ ?>
		  <li class="ui-state" id="<?php echo $val; ?>"><?php echo $val; ?></li>
		  <?php } } ?>
		 </ul>
		 <div class="section">
		 <p> <b><?php esc_attr_e('Slider has fixed position on homepage','spiko-plus'); ?></b></p>
		 <p> <b><?php esc_attr_e('Note','spiko-plus'); ?> </b> <?php esc_attr_e('By default, all sections are enabled on homepage. If you wish not to display a section, just drag it onto the "disabled" box.','spiko-plus'); ?><p>
		</div>
<script>
jQuery(document).ready(function($) {
    $( ".sortable" ).sortable({
		connectWith: '.sortable'
	});
  });
   
jQuery(document).ready(function($){	
	
	// Get items id you can chose
	function spiko_Items(spicethemes)
	{
		var columns = [];
		$(spicethemes + ' #enable').each(function(){
			columns.push($(this).sortable('toArray').join(','));
		});
		return columns.join('|');
	}
	
	function spicethemesItems_disable(spicethemes)
	{
		var columns = [];
		$(spicethemes + ' #disable').each(function(){
			columns.push($(this).sortable('toArray').join(','));
		});
		return columns.join('|');
	}
	
	//onclick check id
	$('#enable .ui-state,#disable .ui-state').mouseleave(function(){ 
		var enable = spiko_Items('#customize-control-layout_manager');
		$("#customize-control-front_page_data input[type = 'text']").val(enable);
		$("#customize-control-front_page_data input[type = 'text']").change();		
	});

  });
</script>
<?php } }
	/* layout manager section */
	$wp_customize->add_section( 'frontpage_layout' , array(
		'title'      => esc_html__('Theme Layout Manager', 'spiko-plus'),
		'priority'       => 1000,
   	) );
	
	$wp_customize->add_setting(
		'layout_manager',
		array(
			'capability'     => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_text_field',
			
		)	
	);
	$wp_customize->add_control( new WP_spiko_plus_layout_Customize_Control( $wp_customize, 'layout_manager', array(
			'section' => 'frontpage_layout',
			'setting' => 'layout_manager',
		))
	);
	
	$wp_customize->add_setting(
		'front_page_data',
		array(
			'default'           =>'services,cta,portfolio,testimonial,team,news,fun,wooproduct,client',
			'capability'        =>  'edit_theme_options',
			'sanitize_callback' =>  'sanitize_text_field',
		)	
	);
	$wp_customize->add_control('front_page_data', array(
			'label' => esc_html__('Enable','spiko-plus'),
			'section' => 'frontpage_layout',
			'type'    =>  'text'
	));	 // enable textbox