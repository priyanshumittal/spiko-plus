<?php

// spiko slider content data
if (!function_exists('spiko_plus_slider_default_customize_register')) :
    add_action('customize_register', 'spiko_plus_slider_default_customize_register');

    function spiko_plus_slider_default_customize_register($wp_customize) {

        //spiko lite slider data
        if (get_theme_mod('home_slider_subtitle') != '' || get_theme_mod('home_slider_title') != '' || get_theme_mod('home_slider_discription') != '' || get_theme_mod('home_slider_image') != '') {

            $home_slider_title = get_theme_mod('home_slider_title');
            $home_slider_subtitle = get_theme_mod('home_slider_subtitle');
            $home_slider_discription = get_theme_mod('home_slider_discription');
            $home_slider_btn_target = get_theme_mod('home_slider_btn_target');
            $home_slider_btn_txt = get_theme_mod('home_slider_btn_txt');
            $home_slider_btn_link = get_theme_mod('home_slider_btn_link');
            $home_slider_btn_target2= get_theme_mod('home_slider_btn_target2');
            $home_slider_btn_txt2 = get_theme_mod('home_slider_btn_txt2');
            $home_slider_btn_link2 = get_theme_mod('home_slider_btn_link2');
            $home_slider_image = get_theme_mod('home_slider_image');
            $home_slider_align_split = get_theme_mod('slider_content_alignment');
            $home_slider_caption='customizer_repeater_slide_caption_'.$home_slider_align_split;


            if ($home_slider_btn_target == 1) {

                $home_slider_btn_target = true;
            }

            $spiko_plus_slider_content_control = $wp_customize->get_setting('spiko_plus_slider_content');
            if (!empty($spiko_plus_slider_content_control)) {
                $spiko_plus_slider_content_control->default = json_encode( array(
                            array(
                                'subtitle' => !empty($home_slider_subtitle) ? $home_slider_subtitle : 'THE WAY FORWORD',
                                'title'      => !empty($home_slider_title)? $home_slider_title:'We provide solutions to <br> grow your business',
                                'text'       => !empty($home_slider_discription)? $home_slider_discription :'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br> tempor incididunt ut labore et dolore magna aliqua.',
                                'abtsliderbutton_text' => !empty($home_slider_btn_txt)? $home_slider_btn_txt : 'Learn More',
                                'abtsliderlink' => !empty($home_slider_btn_link)? $home_slider_btn_link : '#',
                                'image_url'  => !empty($home_slider_image)? $home_slider_image :  SPIKOP_PLUGIN_URL .'/inc/images/slider/slide-1.jpg',
                                'abtslider_open_new_tab' => !empty($home_slider_btn_target)? $home_slider_btn_target : false,
                                'abtbutton_text' => !empty($home_slider_btn_txt2)? $home_slider_btn_txt2 :__('About Us', 'spiko-plus'),
                                'abtlink' => !empty($home_slider_btn_link2)? $home_slider_btn_link2 :'#',
                                'abtopen_new_tab' => !empty($home_slider_btn_target2)? $home_slider_btn_target2 : false,
                                'home_slider_caption' => !empty($home_slider_align_split)? $home_slider_caption : 'customizer_repeater_slide_caption_center',
                                'id'         => 'customizer_repeater_56d7ea7f40b50',
                                ),
                        ) );
            }
        } else {
            //spiko slider data
            $spiko_plus_slider_content_control = $wp_customize->get_setting('spiko_plus_slider_content');
            if (!empty($spiko_plus_slider_content_control)) {
                $spiko_plus_slider_content_control->default = json_encode(array(
                    array(
                        'subtitle' => __('THE WAY FORWORD', 'spiko-plus'),
                        'title' => __('We provide solutions to <br> <span>grow your business</span>', 'spiko-plus'),
                        'text' => __('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br> tempor incididunt ut labore et dolore magna aliqua.', 'spiko-plus'),
                        'abtsliderbutton_text' => __('Learn More', 'spiko-plus'),
                        'abtsliderlink' => '#',
                        'image_url' => SPIKOP_PLUGIN_URL . '/inc/images/slider/slide-1.jpg',
                        'abtslider_open_new_tab' => 'yes',
                        'abtbutton_text' => __('About Us', 'spiko-plus'),
                        'abtlink' => '#',
                        'abtopen_new_tab' => 'yes',
                        'home_slider_caption' => 'customizer_repeater_slide_caption_left',
                        'sidebar_check' => 'no',
                        'id' => 'customizer_repeater_56d7ea7f40b96',
                    ),
                    array(
                        'subtitle' => __('THE WAY FORWORD', 'spiko-plus'),
                        'title' => __('We create stunning <br><span>WordPress themes</span>', 'spiko-plus'),
                        'text' => __('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br> tempor incididunt ut labore et dolore magna aliqua.', 'spiko-plus'),
                        'abtsliderbutton_text' => __('Learn More', 'spiko-plus'),
                        'abtsliderlink' => '#',
                        'image_url' => SPIKOP_PLUGIN_URL . '/inc/images/slider/slide-2.jpg',
                        'abtslider_open_new_tab' => 'yes',
                        'abtbutton_text' => __('About Us', 'spiko-plus'),
                        'abtlink' => '#',
                        'abtopen_new_tab' => 'yes',
                        'home_slider_caption' => 'customizer_repeater_slide_caption_left',
                        'sidebar_check' => 'no',
                        'id' => 'customizer_repeater_56d7ea7f40b97',
                    ),
                    array(
                        'subtitle' => __('THE WAY FORWORD', 'spiko-plus'),
                        'title' => __('We provide solutions to <br> <span>grow your business</span>', 'spiko-plus'),
                        'text' => __('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br> tempor incididunt ut labore et dolore magna aliqua.', 'spiko-plus'),
                        'abtsliderbutton_text' => __('Learn More', 'spiko-plus'),
                        'abtsliderlink' => '#',
                        'image_url' => SPIKOP_PLUGIN_URL . '/inc/images/slider/slide-3.jpg',
                        'abtslider_open_new_tab' => 'yes',
                        'abtbutton_text' => __('About Us', 'spiko-plus'),
                        'abtlink' => '#',
                        'abtopen_new_tab' => 'yes',
                        'home_slider_caption' => 'customizer_repeater_slide_caption_left',
                        'sidebar_check' => 'no',
                        'id' => 'customizer_repeater_56d7ea7f40b98',
                    ),
                ));
            }
        }
    }

endif;

// spiko default service data
if (!function_exists('spiko_service_default_customize_register')) :
    function spiko_service_default_customize_register($wp_customize) {
        $spiko_service_content_control = $wp_customize->get_setting('spiko_service_content');
        if (!empty($spiko_service_content_control)) {
            $spiko_service_content_control->default = json_encode(array(
                array(
                    'icon_value' => 'fa-headphones',
                    'title' => esc_html__('Unlimited Support', 'spiko-plus'),
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                    'choice' => 'customizer_repeater_icon',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b56',
                ),
                array(
                    'icon_value' => 'fa-mobile',
                    'title' => esc_html__('Pixel Perfect Design', 'spiko-plus'),
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                    'choice' => 'customizer_repeater_icon',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b66',
                ),
                array(
                    'icon_value' => 'fa-cogs',
                    'title' => esc_html__('Powerful Options', 'spiko-plus'),
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                    'choice' => 'customizer_repeater_icon',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b86',
                ),
                array(
                    'icon_value' => 'fa-desktop',
                    'title' => esc_html__('Powerful Options', 'spiko-plus'),
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                    'choice' => 'customizer_repeater_icon',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_52d7ea8f40b86',
                ),
                array(
                    'icon_value' => 'fa-headphones',
                    'title' => esc_html__('Powerful Options', 'spiko-plus'),
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                    'choice' => 'customizer_repeater_icon',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_53d7ea9f40b86',
                ),
                array(
                    'icon_value' => 'fa-desktop',
                    'title' => esc_html__('Powerful Options', 'spiko-plus'),
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                    'choice' => 'customizer_repeater_icon',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_59d7ea4f40b86',
                ),
            ));
        }
    }
    add_action('customize_register', 'spiko_service_default_customize_register');
endif;

// spiko default Contact deatail
if (!function_exists('spiko_plus_contact_detail_default_customize_register')) :
    function spiko_plus_contact_detail_default_customize_register($wp_customize) {
        $spiko_plus_contact_detail_content_control = $wp_customize->get_setting('spiko_plus_contact_detail_content');
        if (!empty($spiko_plus_contact_detail_content_control)) {
            $spiko_plus_contact_detail_content_control->default = json_encode(array(
                array(
                    'icon_value' => 'fa-paper-plane',
                    'choice' => 'customizer_repeater_icon',
                    'title' => esc_html__('Address', 'spiko-plus'),
                    'text' => esc_html__('06 Highley St, Victoria, Australia', 'spiko-plus'),
                    'id' => 'customizer_repeater_56d7pa7f40b56',
                ),
                array(
                    'icon_value' => 'fa-solid fa-phone',
                    'choice' => 'customizer_repeater_icon',
                    'title' => esc_html__('Phone', 'spiko-plus'),
                    'text' => esc_html__('(91) 22 54215821, (91) 9876543210', 'spiko-plus'),
                    'id' => 'customizer_repeater_56d7da7f40b66',
                ),
                array(
                    'icon_value' => 'fa-solid fa-envelope',
                    'choice' => 'customizer_repeater_icon',
                    'title' => esc_html__('Email', 'spiko-plus'),
                    'text' => esc_html__('contact@niterex.com office@niterex.com', 'spiko-plus'),
                    'id' => 'customizer_repeater_56d7op7f40b86',
                ),
                array(
                    'icon_value' => 'fa-regular fa-clock',
                    'choice' => 'customizer_repeater_icon',
                    'title' => esc_html__('Working Times', 'spiko-plus'),
                    'text' => esc_html__('Mon - Fri : 08:00 - 18:00 Sat - Sun : Holiday', 'spiko-plus'),
                    'id' => 'customizer_repeater_12d7op7f40b86',
                ),
            ));
        }
    }
    add_action('customize_register', 'spiko_plus_contact_detail_default_customize_register');
endif;

// spiko default Funfact data
if (!function_exists('spiko_funfact_default_customize_register')) :

    function spiko_funfact_default_customize_register($wp_customize) {

        $spiko_funfact_content_control = $wp_customize->get_setting('spiko_funfact_content');
        if (!empty($spiko_funfact_content_control)) {
            $spiko_funfact_content_control->default = json_encode(array(
                array(
                    'icon_value' => 'fa-pie-chart',
                    'title' => '4050',
                    'text' => esc_html__('Satisfied Clients', 'spiko-plus'),
                    'id' => 'customizer_repeater_56d7ea7f40b56',
                ),
                array(
                    'icon_value' => 'fa-smile-o',
                    'title' => '150',
                    'text' => esc_html__('Finish Projects', 'spiko-plus'),
                    'id' => 'customizer_repeater_56d7ea7f40b66',
                ),
                array(
                    'icon_value' => 'fa-users',
                    'title' => '90%',
                    'text' => esc_html__('Business Growth', 'spiko-plus'),
                    'id' => 'customizer_repeater_56d7ea7f40b86',
                ),
                array(
                    'icon_value' => 'fa-coffee',
                    'title' => '27',
                    'text' => esc_html__('Consultants', 'spiko-plus'),
                    'id' => 'customizer_repeater_56d7ea7f40b87',
                ),
            ));
        }
    }

    add_action('customize_register', 'spiko_funfact_default_customize_register');

endif;

// spiko default Contact data
if (!function_exists('spiko_contact_default_customize_register')) :

    function spiko_contact_default_customize_register($wp_customize) {

        $spiko_plus_contact_content_control = $wp_customize->get_setting('spiko_plus_contact_content');
        if (!empty($spiko_plus_contact_content_control)) {
            $spiko_plus_contact_content_control->default = json_encode(array(
                array(
                    'icon_value' => 'fa-paper-plane',
                    'choice' => 'customizer_repeater_icon',
                    'title' => esc_html__('Address', 'spiko-plus'),
                    'text' => esc_html__('06 Highley St, Victoria, Australia', 'spiko-plus'),
                    'id' => 'customizer_repeater_56d7pa7f40b56',
                ),
                array(
                    'icon_value' => 'fa-phone',
                    'choice' => 'customizer_repeater_icon',
                    'title' => esc_html__('Phone', 'spiko-plus'),
                    'text' => esc_html__('(91) 22 54215821, (91) 9876543210', 'spiko-plus'),
                    'id' => 'customizer_repeater_56d7da7f40b66',
                ),
                array(
                    'icon_value' => 'fa-envelope',
                    'choice' => 'customizer_repeater_icon',
                    'title' => esc_html__('Email', 'spiko-plus'),
                    'text' => esc_html__('contact@niterex.com office@niterex.com', 'spiko-plus'),
                    'id' => 'customizer_repeater_56d7op7f40b86',
                ),
                array(
                    'icon_value' => 'fa-regular fa-clock',
                    'choice' => 'customizer_repeater_icon',
                    'title' => esc_html__('Working Times', 'spiko-plus'),
                    'text' => esc_html__('Mon - Fri : 08:00 - 18:00 Sat - Sun : Holiday', 'spiko-plus'),
                    'id' => 'customizer_repeater_12d7op7f40b86',
                ),
            ));
        }
    }

    add_action('customize_register', 'spiko_contact_default_customize_register');

endif;

// spiko default Contact data
if (!function_exists('spiko_social_links_default_customize_register')) :

    function spiko_social_links_default_customize_register($wp_customize) {

        $spiko_social_links_control = $wp_customize->get_setting('spiko_social_links');
        if (!empty($spiko_social_links_control)) {
            $spiko_social_links_control->default = json_encode(array(
                array(
                    'icon_value' => 'fa-brands fa-facebook-f',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b76',
                ),
                array(
                    'icon_value' => 'fa-brands fa-x-twitter',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b77',
                ),
                array(
                    'icon_value' => 'fa-brands fa-linkedin',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b78',
                ),
                array(
                    'icon_value' => 'fa-brands fa-youtube',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b79',
                ),
                array(
                    'icon_value' => 'fa-brands fa-instagram',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b80',
                ),
            ));
        }
    }

    add_action('customize_register', 'spiko_social_links_default_customize_register');

endif;


// spiko default Ribon data
if (!function_exists('spiko_ribon_default_customize_register')) :

    function spiko_ribon_default_customize_register($wp_customize) {

        $spiko_ribon_content_control = $wp_customize->get_setting('spiko_ribon_content');
        if (!empty($spiko_ribon_content_control)) {
            $spiko_ribon_content_control->default = json_encode(array(
                array(
                    'icon_value' => 'fa-brands fa-facebook-f',
                    'title' => esc_html__('Facebook', 'spiko-plus'),
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b76',
                ),
                array(
                    'icon_value' => 'fa-brands fa-x-twitter',
                    'title' => esc_html__('Twitter', 'spiko-plus'),
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b77',
                ),
                array(
                    'icon_value' => 'fa-brands fa-linkedin',
                    'title' => esc_html__('LinkedIn', 'spiko-plus'),
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b78',
                ),
                array(
                    'icon_value' => 'fa fa-google-plus',
                    'title' => esc_html__('Google', 'spiko-plus'),
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b79',
                ),
                array(
                    'icon_value' => 'fa-brands fa-instagram',
                    'title' => esc_html__('Instagram', 'spiko-plus'),
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b80',
                ),
            ));
        }
    }

    add_action('customize_register', 'spiko_ribon_default_customize_register');

endif;


// Spiko Testimonial content data
if (!function_exists('spiko_testimonial_default_customize_register')) :
    add_action('customize_register', 'spiko_testimonial_default_customize_register');

    function spiko_testimonial_default_customize_register($wp_customize) {

        //spiko default testimonial data.
        $spiko_testimonial_content_control = $wp_customize->get_setting('spiko_testimonial_content');
        if (!empty($spiko_testimonial_content_control)) {
            $spiko_testimonial_content_control->default = json_encode(array(
                array(
                    'title' => 'Exellent Theme & Very Fast Support',
                    'text' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem ipsum dolor sit amet,<br> temp consectetur adipisicing elit.',
                    'clientname' => __('Amanda Smith', 'spiko-plus'),
                    'designation' => __('Developer', 'spiko-plus'),
                    'home_testimonial_star' => '4.5',
                    'link' => '#',
                    'image_url' => SPIKOP_PLUGIN_URL . '/inc/images/user/user1.jpg',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_77d7ea7f40b96',
                    'home_slider_caption' => 'customizer_repeater_star_4.5',
                ),
                array(
                    'title' => 'Exellent Theme & Very Fast Support',
                    'text' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem ipsum dolor sit amet,<br> temp consectetur adipisicing elit.',
                    'clientname' => __('Travis Cullan', 'spiko-plus'),
                    'designation' => __('Team Leader', 'spiko-plus'),
                    'home_testimonial_star' => '5',
                    'link' => '#',
                    'image_url' => SPIKOP_PLUGIN_URL . '/inc/images/user/user2.jpg',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_88d7ea7f40b97',
                    'home_slider_caption' => 'customizer_repeater_star_5',
                ),
                array(
                    'title' => 'Exellent Theme & Very Fast Support',
                    'text' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem ipsum dolor sit amet,<br> temp consectetur adipisicing elit.',
                    'clientname' => __('Victoria Wills', 'spiko-plus'),
                    'designation' => __('Volunteer', 'spiko-plus'),
                    'home_testimonial_star' => '3.5',
                    'link' => '#',
                    'image_url' => SPIKOP_PLUGIN_URL . '/inc/images/user/user3.jpg',
                    'id' => 'customizer_repeater_11d7ea7f40b98',
                    'open_new_tab' => 'no',
                    'home_slider_caption' => 'customizer_repeater_star_3.5',
                ),
            ));
        }
    }

endif;


// Spiko Team content data
if (!function_exists('spiko_team_default_customize_register')) :
    add_action('customize_register', 'spiko_team_default_customize_register');

    function spiko_team_default_customize_register($wp_customize) {
        //spiko default team data.
        $spiko_team_content_control = $wp_customize->get_setting('spiko_team_content');
        if (!empty($spiko_team_content_control)) {
            $spiko_team_content_control->default = json_encode(array(
                array(
                    'image_url' => SPIKOP_PLUGIN_URL . '/inc/images/team/item05.jpg',
                    'image_url2' => SPIKOP_PLUGIN_URL . '/inc/images/portfolio/project-20.jpg',
                    'membername' => 'Danial Wilson',
                    'designation' => esc_html__('Senior Manager', 'spiko-plus'),
                    'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime quae, dolores dicta. Blanditiis rem amet repellat, dolores nihil quae in mollitia asperiores ut rerum repellendus, voluptatum eum, officia laudantium quaerat?',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_26d7ea7f40c56',
                    'social_repeater' => json_encode(
                            array(
                                array(
                                    'id' => 'customizer-repeater-social-repeater-37fb908374e06',
                                    'link' => 'facebook.com',
                                    'icon' => 'fa-brands fa-facebook-f',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-47fb9144530fc',
                                    'link' => 'twitter.com',
                                    'icon' => 'fa-brands fa-x-twitter',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9750e1e09',
                                    'link' => 'linkedin.com',
                                    'icon' => 'fa-brands fa-linkedin',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-67fb0150e1e256',
                                    'link' => 'behance.com',
                                    'icon' => 'fa-brands fa-behance',
                                ),
                            )
                    ),
                ),
                array(
                    'image_url' => SPIKOP_PLUGIN_URL . '/inc/images/team/item06.jpg',
                    'image_url2' => SPIKOP_PLUGIN_URL . '/inc/images/about/about-8.jpg',
                    'membername' => 'Amanda Smith',
                    'designation' => esc_html__('Founder & CEO', 'spiko-plus'),
                    'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime quae, dolores dicta. Blanditiis rem amet repellat, dolores nihil quae in mollitia asperiores ut rerum repellendus, voluptatum eum, officia laudantium quaerat?',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d1ea2f40c66',
                    'social_repeater' => json_encode(
                            array(
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9133a7772',
                                    'link' => 'facebook.com',
                                    'icon' => 'fa-brands fa-facebook-f',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9160rt683',
                                    'link' => 'twitter.com',
                                    'icon' => 'fa-brands fa-x-twitter',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb916zzooc9',
                                    'link' => 'linkedin.com',
                                    'icon' => 'fa-brands fa-linkedin',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb916qqwwc784',
                                    'link' => 'behance.com',
                                    'icon' => 'fa-brands fa-behance',
                                ),
                            )
                    ),
                ),
                array(
                    'image_url' => SPIKOP_PLUGIN_URL . '/inc/images/team/item07.jpg',
                    'image_url2' => SPIKOP_PLUGIN_URL . '/inc/images/portfolio/project-11.jpg',
                    'membername' => 'Victoria Wills',
                    'designation' => esc_html__('Web Master', 'spiko-plus'),
                    'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime quae, dolores dicta. Blanditiis rem amet repellat, dolores nihil quae in mollitia asperiores ut rerum repellendus, voluptatum eum, officia laudantium quaerat?',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40c76',
                    'social_repeater' => json_encode(
                            array(
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb917e4c69e',
                                    'link' => 'facebook.com',
                                    'icon' => 'fa-brands fa-facebook-f',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb91830825c',
                                    'link' => 'twitter.com',
                                    'icon' => 'fa-brands fa-x-twitter',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb918d65f2e',
                                    'link' => 'linkedin.com',
                                    'icon' => 'fa-brands fa-linkedin',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb918d65f2e8',
                                    'link' => 'behance.com',
                                    'icon' => 'fa-brands fa-behance',
                                ),
                            )
                    ),
                ),
                array(
                    'image_url' => SPIKOP_PLUGIN_URL . '/inc/images/team/item08.jpg',
                    'image_url2' => SPIKOP_PLUGIN_URL . '/inc/images/about/about-8.jpg',
                    'membername' => 'Travis Marcus',
                    'designation' => esc_html__('UI Developer', 'spiko-plus'),
                    'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime quae, dolores dicta. Blanditiis rem amet repellat, dolores nihil quae in mollitia asperiores ut rerum repellendus, voluptatum eum, officia laudantium quaerat?',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40c86',
                    'social_repeater' => json_encode(
                            array(
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb925cedcb2',
                                    'link' => 'facebook.com',
                                    'icon' => 'fa-brands fa-facebook-f',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb92615f030',
                                    'link' => 'twitter.com',
                                    'icon' => 'fa-brands fa-x-twitter',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9266c223a',
                                    'link' => 'linkedin.com',
                                    'icon' => 'fa-brands fa-linkedin',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9266c223a',
                                    'link' => 'behance.com',
                                    'icon' => 'fa-brands fa-behance',
                                ),
                            )
                    ),
                ),
            ));
        }
    }

endif;

//Client section
if (!function_exists('spiko_client_default_customize_register')) :
    add_action('customize_register', 'spiko_client_default_customize_register');

    function spiko_client_default_customize_register($wp_customize) {
        //spiko default client data.
        $spiko_client_content_control = $wp_customize->get_setting('spiko_clients_content');
        if (!empty($spiko_client_content_control)) {
            $spiko_client_content_control->default = json_encode(array(
                array(
                    'link' => '#',
                    'image_url' => SPIKOP_PLUGIN_URL . '/inc/images/sponsors/client-1.png',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40b96',
                ),
                array(
                    'link' => '#',
                    'image_url' => SPIKOP_PLUGIN_URL . '/inc/images/sponsors/client-2.png',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40b97',
                ),
                array(
                    'link' => '#',
                    'image_url' => SPIKOP_PLUGIN_URL . '/inc/images/sponsors/client-3.png',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40b98',
                ),
                array(
                    'link' => '#',
                    'image_url' => SPIKOP_PLUGIN_URL . '/inc/images/sponsors/client-4.png',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40b99',
                ),
                array(
                    'link' => '#',
                    'image_url' => SPIKOP_PLUGIN_URL . '/inc/images/sponsors/client-5.png',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40b100',
                ),
                array(
                    'link' => '#',
                    'image_url' => SPIKOP_PLUGIN_URL . '/inc/images/sponsors/client-6.png',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40b96',
                ),
            ));
        }
    }

endif;