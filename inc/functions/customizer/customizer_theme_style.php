<?php
$theme = wp_get_theme();
if('Spiko Dark' == $theme->name) {
    $sd_theme_skin='dark';$sd_theme_clr='ruddy-brown.css';
}
else{
    $sd_theme_skin='light';$sd_theme_clr='default.css';
}
// Adding customizer home page setting
//$wp_customize->remove_control('header_textcolor');
//Image Background image
class WP_spiko_plus_pre_Customize_Control extends WP_Customize_Control {

    public $type = 'new_menu';

    function render_content() {
        echo '<h3>' . __('Predefined default background', 'spiko-plus') . '</h3>';
        $name = '_customize-image-radio-' . $this->id;
        $i = 1;
        foreach ($this->choices as $key => $value) {
            ?>
            <label>
                <input type="radio" value="<?php echo $key; ?>" name="<?php echo esc_attr($name); ?>" data-customize-setting-link="<?php echo esc_attr($this->id); ?>" <?php if ($this->value() == $key) {
                echo 'checked';
            } ?>>
                <img <?php if ($this->value() == $key) {
                echo 'class="color_scheem_active"';
            } ?> src="<?php echo SPIKOP_PLUGIN_URL.'/inc/images/bg-pattern/'.$value; ?>" alt="<?php echo esc_attr($value); ?>" />
            </label>
            <?php
            if ($i == 4) {
                echo '<p></p>';
                $i = 0;
            }
            $i++;
        }
        ?>
        <h3><?php esc_attr_e('Background Image', 'spiko-plus'); ?></h3>
        <p><?php esc_attr_e('Go to', 'spiko-plus'); ?> => <?php esc_attr_e('Appearance', 'spiko-plus'); ?> => <?php esc_attr_e('Customize', 'spiko-plus'); ?> => <?php esc_attr_e('Colors & Background', 'spiko-plus'); ?> => <?php esc_attr_e('Background Image', 'spiko-plus'); ?></p><br/>
        <h3><?php esc_attr_e('Background Color', 'spiko-plus'); ?></h3>
        <p> <?php esc_attr_e('Go to', 'spiko-plus'); ?> => <?php esc_attr_e('Appearance', 'spiko-plus'); ?> => <?php esc_attr_e('Customize', 'spiko-plus'); ?> => <?php esc_attr_e('Colors & Background', 'spiko-plus'); ?> => <?php esc_attr_e('Background Color', 'spiko-plus'); ?> </p>
        <script>
            jQuery(document).ready(function ($) {
                $("#customize-control-predefined_back_image label img").click(function () {
                    $("#customize-control-predefined_back_image label img").removeClass("color_scheem_active");
                    $(this).addClass("color_scheem_active");
                });
            });
        </script>
        <?php
    }

}

//Layout Style
class WP_spiko_plus_style_layout_Customize_Control extends WP_Customize_Control {

    public $type = 'new_menu';

    function render_content() {
        echo '<h3>', __('Theme Layout', 'spiko-plus') . '</h3>';
        $name = '_customize-layout-radio-' . $this->id;
        foreach ($this->choices as $key => $value) {
            ?>
            <label>
                <input type="radio" value="<?php echo $key; ?>" name="<?php echo esc_attr($name); ?>" data-customize-setting-link="<?php echo esc_attr($this->id); ?>" <?php if ($this->value() == $key) {
                echo 'checked';
            } ?>>
                <img <?php if ($this->value() == $key) {
                echo 'class="color_scheem_active"';
            } ?> src="<?php echo SPIKOP_PLUGIN_URL.'/inc/images/bg-pattern/'.$value;?>" alt="<?php echo esc_attr($value); ?>" />
            </label>

            <?php }
        ?>
        <script>
            jQuery(document).ready(function ($) {
                $("#customize-control-spiko_layout_style label img").click(function () {
                    $("#customize-control-spiko_layout_style label img").removeClass("color_scheem_active");
                    $(this).addClass("color_scheem_active");
                });
            });
        </script>
        <?php
    }

}

// Theme color
class WP_spiko_plus_color_Customize_Control extends WP_Customize_Control {

    public $type = 'new_menu';

    function render_content() {
        echo '<h3>' . __('Predefined Colors', 'spiko-plus') . '</h3>';
        $name = '_customize-color-radio-' . $this->id;
        foreach ($this->choices as $key => $value) {
            ?>
            <label>
                <input type="radio" value="<?php echo $key; ?>" name="<?php echo esc_attr($name); ?>" data-customize-setting-link="<?php echo esc_attr($this->id); ?>" <?php if ($this->value() == $key) {
                echo 'checked="checked"';
            } ?>>
                <img <?php if ($this->value() == $key) {
                echo 'class="color_scheem_active"';
            } ?> src="<?php echo SPIKOP_PLUGIN_URL.'/inc/images/bg-pattern/'.$value; ?>" alt="<?php echo esc_attr($value); ?>" />
            </label>

            <?php
        }
        ?>
        <script>
            jQuery(document).ready(function ($) {
                $("#customize-control-theme_color label img").click(function () {
                    $("#customize-control-theme_color label img").removeClass("color_scheem_active");
                    $(this).addClass("color_scheem_active");
                });
            });
        </script>
        <?php
    }

}

/* Theme Style settings */
$wp_customize->add_section('theme_style', array(
    'title' => __('Theme Style Settings', 'spiko-plus'),
    'priority' => 110,
));

// Theme Color Scheme
$wp_customize->add_setting(
        'theme_color', array(
    'default' => $sd_theme_clr,
    'capability' => 'edit_theme_options',
));
$wp_customize->add_control(new WP_spiko_plus_color_Customize_Control($wp_customize, 'theme_color',
                array(
            'label' => __('Predefined colors', 'spiko-plus'),
            'section' => 'theme_style',
            'type' => 'radio',
            'choices' => array(
                'default.css' => 'default.png',
                'orange.css' => 'orange.png',
                'green.css' => 'green.png',
                'red.css' => 'red.png',
                'purple.css' => 'purple.png',
                'blue.css' => 'wordpress.png',
                'yellow.css' => 'yellow.png',
                'ruddy-brown.css'=> 'ruddy-brown.png',
        ))));

// enable / disable custom color settings 
$wp_customize->add_setting(
        'custom_color_enable',
        array('capability' => 'edit_theme_options',
            'default' => false,
));
$wp_customize->add_control(
        'custom_color_enable',
        array(
            'type' => 'checkbox',
            'label' => __('Enable custom color skin', 'spiko-plus'),
            'section' => 'theme_style',
        )
);

// link color settings
$wp_customize->add_setting(
        'link_color', array(
    'capability' => 'edit_theme_options',
    'default' => '#35ac39'
));

$wp_customize->add_control(
        new WP_Customize_Color_Control(
                $wp_customize,
                'link_color',
                array(
            'label' => __('Skin Color', 'spiko-plus'),
            'section' => 'theme_style',
            'settings' => 'link_color',
        )));

//Theme Layout
$wp_customize->add_setting( 'spiko_color_skin', array( 'default' => $sd_theme_skin,) );
$wp_customize->add_control( 'spiko_color_skin', 
    array(
        'label'    => __( 'Theme Skin', 'spiko-plus' ),
        'section'  => 'theme_style',
        'type'     => 'select',
        'choices'=>array(
            'light'=>__('Light', 'spiko-plus'),
            'dark'=>__('Dark', 'spiko-plus'),
            )
));

//Theme Layout
$wp_customize->add_setting(
        'spiko_layout_style', array(
    'default' => 'wide.jpg',
    'capability' => 'edit_theme_options',
));
$wp_customize->add_control(new WP_spiko_plus_style_layout_Customize_Control($wp_customize, 'spiko_layout_style',
                array(
            'label' => __('Layout style', 'spiko-plus'),
            'section' => 'theme_style',
            'type' => 'radio',
            'choices' => array(
                'wide' => 'wide.png',
                'boxed' => 'boxed.png',
            )
        )));


//Predefined Background image
$wp_customize->add_setting(
        'predefined_back_image', array(
    'default' => 'bg-img1.png',
    'capability' => 'edit_theme_options',
));
$wp_customize->add_control(new WP_spiko_plus_pre_Customize_Control($wp_customize, 'predefined_back_image',
                array(
            'label' => __('Predefined default background', 'spiko-plus'),
            'section' => 'theme_style',
            'type' => 'radio',
            'choices' => array(
                'bg-img0.png' => 'sm0.png',
                'bg-img1.png' => 'sm1.png',
                'bg-img2.png' => 'sm2.png',
                'bg-img3.png' => 'sm3.png',
                'bg-img4.png' => 'sm4.png',
                'bg-img5.png' => 'sm5.png',
                'bg-img8.jpg' => 'sm8.jpg',
                'bg-img9.jpg' => 'sm9.jpg',
                'bg-img10.jpg' => 'sm10.jpg',
        ))));