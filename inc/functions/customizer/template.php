<?php

/**
 * Template Options Customizer
 *
 * @package spiko
 */
function spiko_plus_template_customizer($wp_customize) {
    $wp_customize->add_panel('spiko_template_settings',
            array(
                'priority' => 920,
                'capability' => 'edit_theme_options',
                'title' => __('Page Settings', 'spiko-plus')
            )
    );

 // add section to manage About us page settings
    $wp_customize->add_section(
            'about_page_section',
            array(
                'title' => __('About Us Page Settings', 'spiko-plus'),
                'panel' => 'spiko_template_settings',
                'priority' => 100,
            )
    );

    // Add section to manage Portfolio page settings
    $wp_customize->add_section(
            'porfolio_page_section',
            array(
                'title' => __('Portfolio Page Settings', 'spiko-plus'),
                'panel' => 'spiko_template_settings',
                'priority' => 600,
            )
    );

    // Portfolio page subtitle
    $wp_customize->add_setting(
            'porfolio_page_subtitle', array(
        'default' => __('Our Recent Works', 'spiko-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'porfolio_page_subtitle',
            array(
                'type' => 'text',
                'label' => __('Sub Title', 'spiko-plus'),
                'section' => 'porfolio_page_section',
            )
    );
    
    // Portfolio page title
    $wp_customize->add_setting(
            'porfolio_page_title', array(
        'default' => __('Our Portfolio', 'spiko-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'porfolio_page_title',
            array(
                'type' => 'text',
                'label' => __('Title', 'spiko-plus'),
                'section' => 'porfolio_page_section',
            )
    );
    
    // Add section to manage Portfolio category page settings
    $wp_customize->add_section(
            'porfolio_category_page_section',
            array(
                'title' => __('Portfolio Category Page Settings', 'spiko-plus'),
                'panel' => 'spiko_template_settings',
                'priority' => 700,
            )
    );

    // Portfolio category page subtitle
    $wp_customize->add_setting(
            'porfolio_category_page_desc', array(
        'default' => __('Morbi facilisis, ipsum ut ullamcorper venenatis, nisi diam placerat turpis, at auctor nisi magna cursus arcu.', 'spiko-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));
    $wp_customize->add_control(
            'porfolio_category_page_desc',
            array(
                'type' => 'text',
                'label' => __('Description', 'spiko-plus'),
                'section' => 'porfolio_category_page_section',
            )
    );
    
    // Portfolio category page title
    $wp_customize->add_setting(
            'porfolio_category_page_title', array(
        'default' => __('Portfolio Category Title', 'spiko-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));
    $wp_customize->add_control(
            'porfolio_category_page_title',
            array(
                'type' => 'text',
                'label' => __('Title', 'spiko-plus'),
                'section' => 'porfolio_category_page_section',
            )
    );
    
    // Number of portfolio column layout
    $wp_customize->add_setting('portfolio_cat_column_layout', array(
        'default' => 3,
        'sanitize_callback' => 'sanitize_text_field',
    ));
    $wp_customize->add_control('portfolio_cat_column_layout', array(
        'type' => 'select',
        'label' => __('Select column layout', 'spiko-plus'),
        'section' => 'porfolio_category_page_section',
        'choices' => array(2 => 2, 3 => 3, 4 => 4),
    ));

    // Add section to manage Contact page settings
    $wp_customize->add_section(
            'contact_page_section',
            array(
                'title' => __('Contact Page Settings', 'spiko-plus'),
                'panel' => 'spiko_template_settings',
                'priority' => 800,
            )
    );

    // Contact form title
    $wp_customize->add_setting(
            'contact_cf7_title', array(
        'default' => __("Don't Hesitate To Contact Us", 'spiko-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'contact_cf7_title',
            array(
                'type' => 'text',
                'label' => __('Contact Form Title', 'spiko-plus'),
                'section' => 'contact_page_section',
            )
    );

    // Contact Detail Title
    $wp_customize->add_setting(
            'contact_dt_title', array(
        'default' => __("General Enquiries", 'spiko-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'contact_dt_title',
            array(
                'type' => 'text',
                'label' => __('Contact Detail Title', 'spiko-plus'),
                'section' => 'contact_page_section',
            )
    );

    $wp_customize->add_control(
            'contact_info_title',
            array(
                'type' => 'text',
                'label' => __('Contact Title', 'spiko-plus'),
                'section' => 'contact_page_section',
            )
    );

    if (class_exists('Spiko_Plus_Repeater')) {
        $wp_customize->add_setting('spiko_plus_contact_content', array());

        $wp_customize->add_control(new Spiko_Plus_Repeater($wp_customize, 'spiko_plus_contact_content', array(
                    'label' => esc_html__('Contact Details', 'spiko-plus'),
                    'section' => 'contact_page_section',
                    'priority' => 10,
                    'add_field_label' => esc_html__('Add new Contact Info', 'spiko-plus'),
                    'item_name' => esc_html__('Contact', 'spiko-plus'),
                    'customizer_repeater_icon_control' => true,
                    'customizer_repeater_title_control' => true,
                    'customizer_repeater_text_control' => true,
        )));
    }

    // google map shortcode
    $wp_customize->add_setting('contact_google_map_shortcode', array(
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
    ));
    $wp_customize->add_control('contact_google_map_shortcode', array(
        'label' => __('Google Map Shortcode', 'spiko-plus'),
        'section' => 'contact_page_section',
        'type' => 'textarea',
    ));

    // Contact form 7 shortcode
    $wp_customize->add_setting('contact_form_shortcode', array(
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
    ));
    $wp_customize->add_control('contact_form_shortcode', array(
        'label' => esc_html__('Contact Form Shortcode', 'spiko-plus'),
        'section' => 'contact_page_section',
        'type' => 'textarea',
    ));

     // Contact form Client Section
    // $wp_customize->add_setting('contact_client_detail_enable',
    //         array(
    //             'default' => true,
    //             'sanitize_callback' => 'spiko_sanitize_checkbox'
    // ));

    // $wp_customize->add_control(new Spiko_Toggle_Control($wp_customize, 'contact_client_detail_enable',
    //                 array(
    //             'label' => __('Enable/Disable Client Detail Section', 'spiko-plus'),
    //             'section' => 'contact_page_section',
    //             'type' => 'toggle',
    //                 )
    // ));
    
    $wp_customize->selective_refresh->add_partial('contact_cf7_title', array(
        'selector' => '.section-space.contact-form h2',
        'settings' => 'contact_cf7_title',
        'render_callback' => 'contact_cf7_title_render_callback'
    ));

    $wp_customize->selective_refresh->add_partial('porfolio_category_page_title', array(
        'selector' => '.portfolio-cat-page h2.section-title',
        'settings' => 'porfolio_category_page_title',
        'render_callback' => 'porfolio_category_page_title_render_callback'
    ));

    $wp_customize->selective_refresh->add_partial('porfolio_category_page_desc', array(
        'selector' => '.portfolio-cat-page h5.section-subtitle ',
        'settings' => 'porfolio_category_page_desc',
        'render_callback' => 'porfolio_category_page_desc_render_callback'
    ));

    $wp_customize->selective_refresh->add_partial('porfolio_page_title', array(
        'selector' => '.portfolio-page h2.section-title',
        'settings' => 'porfolio_page_title',
        'render_callback' => 'porfolio_page_title_render_callback'
    ));

    $wp_customize->selective_refresh->add_partial('porfolio_page_subtitle', array(
        'selector' => '.portfolio-page h5.section-subtitle',
        'settings' => 'porfolio_page_subtitle',
        'render_callback' => 'porfolio_page_subtitle_render_callback'
    ));

    function contact_cf7_title_render_callback() {
        return get_theme_mod('contact_cf7_title');
    }

    function contact_info_title_render_callback() {
        return get_theme_mod('contact_info_title');
    }

    function porfolio_category_page_title_render_callback() {
        return get_theme_mod('porfolio_category_page_title');
    }

    function porfolio_category_page_desc_render_callback() {
        return get_theme_mod('porfolio_category_page_desc');
    }

    function porfolio_page_title_render_callback() {
        return get_theme_mod('porfolio_page_title');
    }

    function porfolio_page_subtitle_render_callback() {
        return get_theme_mod('porfolio_page_subtitle');
    }

}

add_action('customize_register', 'spiko_plus_template_customizer');
?>