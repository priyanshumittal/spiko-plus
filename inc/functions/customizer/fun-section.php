<?php

/* funfact section */
$wp_customize->add_setting('funfact_section_enabled', array(
    'default' => true,
    'sanitize_callback' => 'spiko_sanitize_checkbox'
));

$wp_customize->add_control(new Spiko_Toggle_Control($wp_customize, 'funfact_section_enabled',
                array(
            'label' => __('Enable Funfact on homepage', 'spiko-plus'),
            'type' => 'toggle',
            'section' => 'funfact_section',
                )
));

$wp_customize->add_section('funfact_section', array(
    'title' => __('Funfact Settings', 'spiko-plus'),
    'panel' => 'section_settings',
    'priority' => 17,
));

if (class_exists('Spiko_Plus_Repeater')) {
    $wp_customize->add_setting('spiko_funfact_content', array());

    $wp_customize->add_control(new Spiko_Plus_Repeater($wp_customize, 'spiko_funfact_content', array(
                'label' => esc_html__('Funfact content', 'spiko-plus'),
                'section' => 'funfact_section',
                'priority' => 10,
                'add_field_label' => esc_html__('Add new Funfact', 'spiko-plus'),
                'item_name' => esc_html__('Funfact', 'spiko-plus'),
                //'customizer_repeater_icon_control' => true,
                'customizer_repeater_title_control' => true,
                'customizer_repeater_text_control' => true,
                'active_callback' => 'spiko_plus_funfact_callback'
    )));
}


//FunFact Background Image
$wp_customize->add_setting('funfact_callout_background', array(
    'default' => SPIKOP_PLUGIN_URL .'inc/images/bg/funfact-bg.jpg',
    'sanitize_callback' => 'esc_url_raw',
));

$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'funfact_callout_background', array(
            'label' => esc_html__('Background Image', 'spiko-plus'),
            'section' => 'funfact_section',
            'settings' => 'funfact_callout_background',
            'active_callback' => 'spiko_plus_funfact_callback'
        )));

// Image overlay
$wp_customize->add_setting('funfact_image_overlay', array(
    'default' => true,
    'sanitize_callback' => 'sanitize_text_field',
));

$wp_customize->add_control('funfact_image_overlay', array(
    'label' => esc_html__('Enable Funfact image overlay', 'spiko-plus'),
    'section' => 'funfact_section',
    'type' => 'checkbox',
    'active_callback' => 'spiko_plus_funfact_callback'
));

//Funfact Background Overlay Color
$wp_customize->add_setting('funfact_overlay_section_color', array(
    'sanitize_callback' => 'sanitize_text_field',
    'default' => 'rgba(0, 11, 24, 0.8)',
));

$wp_customize->add_control(new Spiko_Plus_Customize_Alpha_Color_Control($wp_customize, 'funfact_overlay_section_color', array(
            'label' => esc_html__('Funfact image overlay color', 'spiko-plus'),
            'palette' => true,
            'active_callback' => 'spiko_plus_funfact_callback',
            'section' => 'funfact_section')
));

/**
 * Add selective refresh for Front page funfact section controls.
 */
$wp_customize->selective_refresh->add_partial('home_fun_section_title', array(
    'selector' => '.funfact .title',
    'settings' => 'home_fun_section_title',
    'render_callback' => 'spiko_plus_home_fun_section_title_render_callback'
));

function spiko_plus_home_fun_section_title_render_callback() {
    return get_theme_mod('home_fun_section_title');
}

?>