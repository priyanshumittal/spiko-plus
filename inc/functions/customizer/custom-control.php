<?php

if (!function_exists('spiko_plus_register_custom_controls')) :

    /**
     * Register Custom Controls
     */
    function spiko_plus_register_custom_controls($wp_customize) {
        require_once SPIKOP_PLUGIN_DIR . '/inc/inc/customizer/repeater/class-repeater-setting.php';
        require_once SPIKOP_PLUGIN_DIR . '/inc/inc/customizer/repeater/class-control-repeater.php';
        require_once SPIKOP_PLUGIN_DIR . '/inc/inc/customizer/sortable/class-sortable-control.php';
        require_once SPIKOP_PLUGIN_DIR . '/inc/inc/customizer/slider/class-slider-control.php';
        require_once SPIKOP_PLUGIN_DIR . '/inc/inc/customizer/slider/class-opacity-control.php';
        $wp_customize->register_control_type('Spiko_Plus_Control_Sortable');
        $wp_customize->register_control_type('Spiko_Plus_Slider_Control');
        $wp_customize->register_control_type( 'Spiko_Plus_Opacity_Control' );
    }

endif;
add_action('customize_register', 'spiko_plus_register_custom_controls');