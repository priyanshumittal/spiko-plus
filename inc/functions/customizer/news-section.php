<?php
$theme = wp_get_theme();
if('Spiko Dark' == $theme->name) {
    $sd_newz_design=3;
    $sd_newz_col_design=6;
}
else{
    $sd_newz_design=1;
    $sd_newz_col_design=4;
}
//Latest News Section
$wp_customize->add_section('spiko_latest_news_section', array(
    'title' => __('Latest News Settings', 'spiko-plus'),
    'panel' => 'section_settings',
    'priority' =>16,
));


// Enable news section
$wp_customize->add_setting('latest_news_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'spiko_plus_sanitize_checkbox'
));

$wp_customize->add_control(new Spiko_Toggle_Control($wp_customize, 'latest_news_section_enable',
                array(
            'label' => __('Enable Home News section', 'spiko-plus'),
            'type' => 'toggle',
            'section' => 'spiko_latest_news_section',
                )
));

//News section subtitle
$wp_customize->add_setting('home_news_section_discription', array(
    'default' => __('From our blog', 'spiko-plus'),
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_news_section_discription', array(
    'label' => __('Sub Title', 'spiko-plus'),
    'section' => 'spiko_latest_news_section',
    'type' => 'text',
    'active_callback' => 'spiko_plus_news_callback'
));

// News section title
$wp_customize->add_setting('home_news_section_title', array(
    'capability' => 'edit_theme_options',
    'default' => __('Our Latest News', 'spiko-plus'),
    'sanitize_callback' => 'spiko_plus_home_page_sanitize_text',
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_news_section_title', array(
    'label' => __('Title', 'spiko-plus'),
    'section' => 'spiko_latest_news_section',
    'type' => 'text',
    'active_callback' => 'spiko_plus_news_callback'
));



$wp_customize->add_setting('home_news_design_layout', array('default' => $sd_newz_design,));
$wp_customize->add_control('home_news_design_layout',
        array(
            'label' => __('Design Style', 'spiko-plus'),
            'section' => 'spiko_latest_news_section',
            'active_callback' => 'spiko_plus_news_callback',
            'type' => 'select',
            'choices' => array(
                1 => __('Grid Style', 'spiko-plus'),
                2 => __('List Style', 'spiko-plus'),
                3 => __('Masonry Style', 'spiko-plus'),
                4 => __('Carousel Style', 'spiko-plus'),
            )
));

/* * ****************** Blog Content ****************************** */

$wp_customize->add_setting('spiko_homeblog_layout',
        array(
            'default' => $sd_newz_col_design,
            'sanitize_callback' => 'spiko_sanitize_select'
        )
);

$wp_customize->add_control('spiko_homeblog_layout',
        array(
            'label' => esc_html__('Column Layout', 'spiko-plus'),
            'section' => 'spiko_latest_news_section',
            'type' => 'select',
            'active_callback' => 'spiko_column_callback',
            'active_callback' => function($control) {
                return (
                        spiko_plus_news_callback($control) &&
                        spiko_column_callback($control)
                        );
            },
            'choices' => array(
                6 => esc_html__('2 Column', 'spiko-plus'),
                4 => esc_html__('3 Column', 'spiko-plus'),
                3 => esc_html__('4 Column', 'spiko-plus'),
            )
        )
);

$wp_customize->add_setting('spiko_homeblog_counts',
        array(
            'default' => 3,
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'spiko_sanitize_number_range',
        )
);
$wp_customize->add_control('spiko_homeblog_counts',
        array(
            'label' => esc_html__('No of Post', 'spiko-plus'),
            'section' => 'spiko_latest_news_section',
            'type' => 'number',
            'input_attrs' => array('min' => 2, 'max' => 20, 'step' => 1, 'style' => 'width: 100%;'),
            'active_callback' => 'spiko_plus_news_callback'
        )
);


// Read More Button
$wp_customize->add_setting('home_news_button_title', array(
    'capability' => 'edit_theme_options',
    'default' => __('Read More', 'spiko-plus'),
    'sanitize_callback' => 'spikop_home_page_sanitize_text',
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_news_button_title', array(
    'label' => __('Read More Text', 'spiko-plus'),
    'section' => 'spiko_latest_news_section',
    'type' => 'text',
    'active_callback' => 'spiko_plus_news_callback'
));

// enable/disable meta section 
$wp_customize->add_setting(
        'home_meta_section_settings',
        array('capability' => 'edit_theme_options',
            'default' => true,
));
$wp_customize->add_control(
        'home_meta_section_settings',
        array(
            'type' => 'checkbox',
            'label' => __('Enable post meta in blog section', 'spiko-plus'),
            'section' => 'spiko_latest_news_section',
            'active_callback' => 'spiko_plus_news_callback'
        )
);




$wp_customize->add_setting(
        'home_blog_more_btn',
        array(
            'default' => __('View More', 'spiko-plus'),
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field',
            'transport' => $selective_refresh,
        )
);

$wp_customize->add_control(
        'home_blog_more_btn',
        array(
            'label' => __('View More Button Text', 'spiko-plus'),
            'section' => 'spiko_latest_news_section',
            'type' => 'text',
            'active_callback' => 'spiko_plus_news_callback'
));

$wp_customize->add_setting(
        'home_blog_more_btn_link',
        array(
            'default' => '#',
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field',
            'transport' => $selective_refresh,
));


$wp_customize->add_control(
        'home_blog_more_btn_link',
        array(
            'label' => __('View More Button Link', 'spiko-plus'),
            'section' => 'spiko_latest_news_section',
            'type' => 'text',
            'active_callback' => 'spiko_plus_news_callback'
));

$wp_customize->add_setting(
        'home_blog_more_btn_link_target',
        array('sanitize_callback' => 'sanitize_text_field',
            'transport' => $selective_refresh,
));

$wp_customize->add_control(
        'home_blog_more_btn_link_target',
        array(
            'type' => 'checkbox',
            'label' => __('Open link in new tab', 'spiko-plus'),
            'section' => 'spiko_latest_news_section',
            'active_callback' => 'spiko_plus_news_callback'
        )
);

/**
 * Add selective refresh for Front page news section controls.
 */
$wp_customize->selective_refresh->add_partial('home_news_section_title', array(
    'selector' => '.home-blog .section-header h2',
    'settings' => 'home_news_section_title',
    'render_callback' => 'spiko_plus_home_news_section_title_render_callback',
));

$wp_customize->selective_refresh->add_partial('home_news_section_discription', array(
    'selector' => '.home-blog .section-header h5',
    'settings' => 'home_news_section_discription',
    'render_callback' => 'spiko_plus_home_news_section_discription_render_callback',
));

$wp_customize->selective_refresh->add_partial('home_blog_more_btn', array(
    'selector' => '.home-blog .business-view-more-post',
    'settings' => 'home_blog_more_btn',
    'render_callback' => 'spiko_plus_home_blog_more_btn_render_callback',
));

$wp_customize->selective_refresh->add_partial('home_news_button_title', array(
    'selector' => '.home-blog a.more-link',
    'settings' => 'home_news_button_title',
    'render_callback' => 'spiko_plus_home_news_button_title_render_callback',
));

function spiko_plus_home_news_section_title_render_callback() {
    return get_theme_mod('home_news_section_title');
}

function spiko_plus_home_news_section_discription_render_callback() {
    return get_theme_mod('home_news_section_discription');
}

function spiko_plus_home_blog_more_btn_render_callback() {
    return get_theme_mod('home_blog_more_btn');
}

function spiko_plus_home_news_button_title_render_callback() {
    return get_theme_mod('home_news_button_title');
}

function spiko_plus_column_callback($control) {
    if ($control->manager->get_setting('home_news_design_layout')->value() == '2') {
        return false;
    }
    return true;
}