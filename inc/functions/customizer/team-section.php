<?php
$theme = wp_get_theme();
if('Spiko Dark' == $theme->name) {
    $sd_team_design=2;
}
else{
    $sd_team_design=1;
}
//Team Section
$wp_customize->add_section('spiko_team_section', array(
    'title' => __('Team Settings', 'spiko-plus'),
    'panel' => 'section_settings',
    'priority' => 15,
));

$wp_customize->add_setting('team_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'spiko_sanitize_checkbox'
));

$wp_customize->add_control(new Spiko_Toggle_Control($wp_customize, 'team_section_enable',
                array(
            'label' => __('Enable Home Team section', 'spiko-plus'),
            'type' => 'toggle',
            'section' => 'spiko_team_section',
                )
));

//Team section discription
$wp_customize->add_setting('home_team_section_discription', array(
    'default' => __('Meet Our Experts', 'spiko-plus'),
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_team_section_discription', array(
    'label' => __('Sub Title', 'spiko-plus'),
    'section' => 'spiko_team_section',
    'type' => 'text',
    'active_callback' => 'spiko_plus_team_callback'
));

// Team section title
$wp_customize->add_setting('home_team_section_title', array(
    'default' => __('The Team', 'spiko-plus'),
    'sanitize_callback' => 'spikop_home_page_sanitize_text',
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_team_section_title', array(
    'label' => __('Title', 'spiko-plus'),
    'section' => 'spiko_team_section',
    'type' => 'text',
    'active_callback' => 'spiko_plus_team_callback'
));



//Style Design
$wp_customize->add_setting('home_team_design_layout', array('default' => $sd_team_design,));
$wp_customize->add_control('home_team_design_layout',
        array(
            'label' => __('Design Style', 'spiko-plus'),
            'section' => 'spiko_team_section',
            'type' => 'select',
            'choices' => array(
                1 => __('Design 1', 'spiko-plus'),
                2 => __('Design 2', 'spiko-plus'),
                3 => __('Design 3', 'spiko-plus'),
                4 => __('Design 4', 'spiko-plus')
            ),
            'active_callback' => 'spiko_plus_team_callback',
));


if (class_exists('Spiko_Plus_Repeater')) {
    $wp_customize->add_setting(
            'spiko_team_content', array(
            )
    );

    $wp_customize->add_control(
            new Spiko_Plus_Repeater(
                    $wp_customize, 'spiko_team_content', array(
                'label' => esc_html__('Team content', 'spiko-plus'),
                'section' => 'spiko_team_section',
                'priority' => 15,
                'add_field_label' => esc_html__('Add new Team Member', 'spiko-plus'),
                'item_name' => esc_html__('Team Member', 'spiko-plus'),
                'customizer_repeater_member_name_control' => true,
                //'customizer_repeater_text_control' => true,
                'customizer_repeater_designation_control' => true,
                'customizer_repeater_image_control' => true,
                'customizer_repeater_image_control2' => true,
                //'customizer_repeater_link_control' => true,
                // 'customizer_repeater_checkbox_control' => true,
                'customizer_repeater_repeater_control' => true,
                'active_callback' => 'spiko_plus_team_callback'
                    )
            )
    );
}

// animation speed
$wp_customize->add_setting('team_animation_speed', array('default' => 3000));
$wp_customize->add_control('team_animation_speed',
        array(
            'label' => __('Animation speed', 'spiko-plus'),
            'section' => 'spiko_team_section',
            'type' => 'select',
            'priority' => 53,
            'choices' => array(
                '2000' => '2.0',
                '3000' => '3.0',
                '4000' => '4.0',
                '5000' => '5.0',
                '6000' => '6.0',
            ),
            'active_callback' => 'spiko_plus_team_callback'
));

//Navigation Type
$wp_customize->add_setting('team_nav_style', array('default' => 'bullets'));
$wp_customize->add_control('team_nav_style', array(
    'label' => __('Navigation Style', 'spiko-plus'),
    'section' => 'spiko_team_section',
    'type' => 'radio',
    'priority' => 17,
    'choices' => array(
        'bullets' => __('Bullets', 'spiko-plus'),
        'navigation' => __('Navigation', 'spiko-plus'),
        'both' => __('Both', 'spiko-plus'),
    ),
    'active_callback' => 'spiko_plus_team_callback'
));


// smooth speed
$wp_customize->add_setting('team_smooth_speed', array('default' => 1000));
$wp_customize->add_control('team_smooth_speed',
        array(
            'label' => __('Smooth speed', 'spiko-plus'),
            'section' => 'spiko_team_section',
            'type' => 'select',
            'priority' => 17,
            'active_callback' => 'spiko_plus_team_callback',
            'choices' => array('500' => '0.5',
                '1000' => '1.0',
                '1500' => '1.5',
                '2000' => '2.0',
                '2500' => '2.5',
                '3000' => '3.0')
));

/**
 * Add selective refresh for Front page team section controls.
 */
$wp_customize->selective_refresh->add_partial('home_team_section_title', array(
    'selector' => '.section-space.team .section-title',
    'settings' => 'home_team_section_title',
    'render_callback' => 'spiko_plus_home_team_section_title_render_callback',
));

$wp_customize->selective_refresh->add_partial('home_team_section_discription', array(
    'selector' => '.section-space.team .section-subtitle',
    'settings' => 'home_team_section_discription',
    'render_callback' => 'spiko_plus_home_team_section_discription_render_callback',
));

function spiko_plus_home_team_section_title_render_callback() {
    return get_theme_mod('home_team_section_title');
}

function spiko_plus_home_team_section_discription_render_callback() {
    return get_theme_mod('home_team_section_discription');
}

?>