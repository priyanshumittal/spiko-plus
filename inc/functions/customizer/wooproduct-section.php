<?php

//Shop Section
$wp_customize->add_section('spiko_shop_section', array(
    'title' => __('Home Shop Settings', 'spiko-plus'),
    'panel' => 'section_settings',
    'priority' => 21,
));

$wp_customize->add_setting('shop_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'spiko_sanitize_checkbox'
    ));

$wp_customize->add_control(new Spiko_Toggle_Control($wp_customize, 'shop_section_enable',
                array(
            'label' => __('Enable Home Shop section', 'spiko-plus'),
            'type' => 'toggle',
            'section' => 'spiko_shop_section',
                )
));

//Shop section discription
$wp_customize->add_setting('home_shop_section_discription', array(
    'default' => __('Our amazing products', 'spiko-plus'),
));
$wp_customize->add_control('home_shop_section_discription', array(
    'label' => __('Sub Title', 'spiko-plus'),
    'section' => 'spiko_shop_section',
    'type' => 'textarea',
    'active_callback' => 'spiko_plus_wooproduct_callback'
));

// Shop section title
$wp_customize->add_setting('home_shop_section_title', array(
    'default' => __('Featured Products', 'spiko-plus'),
    'sanitize_callback' => 'spikop_home_page_sanitize_text',
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_shop_section_title', array(
    'label' => __('Title', 'spiko-plus'),
    'section' => 'spiko_shop_section',
    'type' => 'text',
    'active_callback' => 'spiko_plus_wooproduct_callback'
));



//Navigation Type
$wp_customize->add_setting('shop_nav_style', array('default' => 'bullets'));
$wp_customize->add_control('shop_nav_style', array(
    'label' => __('Navigation Style', 'spiko-plus'),
    'section' => 'spiko_shop_section',
    'type' => 'radio',
    'priority' => 12,
    'choices' => array(
        'bullets' => __('Bullets', 'spiko-plus'),
        'navigation' => __('Navigation', 'spiko-plus'),
        'both' => __('Both', 'spiko-plus'),
    ),
    'active_callback' => 'spiko_plus_wooproduct_callback'
));

// animation speed
$wp_customize->add_setting('shop_animation_speed', array('default' => 3000));
$wp_customize->add_control('shop_animation_speed',
        array(
            'label' => __('Animation speed', 'spiko-plus'),
            'section' => 'spiko_shop_section',
            'type' => 'select',
            'choices' => array(
                '2000' => '2.0',
                '3000' => '3.0',
                '4000' => '4.0',
                '5000' => '5.0',
                '6000' => '6.0',
            ),
            'active_callback' => 'spiko_plus_wooproduct_callback'
));

// smooth speed
$wp_customize->add_setting('shop_smooth_speed', array('default' => 1000));
$wp_customize->add_control('shop_smooth_speed',
        array(
            'label' => __('Smooth speed', 'spiko-plus'),
            'section' => 'spiko_shop_section',
            'type' => 'select',
            'choices' => array('500' => '0.5',
                '1000' => '1.0',
                '1500' => '1.5',
                '2000' => '2.0',
                '2500' => '2.5',
                '3000' => '3.0'),
            'active_callback' => 'spiko_plus_wooproduct_callback'
));

$wp_customize->selective_refresh->add_partial('home_shop_section_title', array(
    'selector' => '.shop .section-header h2',
    'settings' => 'home_shop_section_title',
    'render_callback' => 'spiko_plus_home_shop_section_title_render_callback',
));

$wp_customize->selective_refresh->add_partial('home_shop_section_discription', array(
    'selector' => '.shop .section-subtitle',
    'settings' => 'home_shop_section_discription',
    'render_callback' => 'spiko_plus_home_shop_section_discription_render_callback',
));

function spiko_plus_home_shop_section_title_render_callback() {
    return get_theme_mod('home_shop_section_title');
}

function spiko_plus_home_shop_section_discription_render_callback() {
    return get_theme_mod('home_shop_section_discription');
}
?>