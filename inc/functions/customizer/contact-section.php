<?php

$wp_customize->add_section('contact_section', array(
    'title' => esc_html__('Contact Settings', 'spiko-plus'),
    'panel' => 'section_settings',
//    'priority' => 2,
));


//Contact Section
$wp_customize->add_setting('home_contact_section_enabled', array(
    'default' => true,
    'sanitize_callback' => 'spiko_sanitize_checkbox'
));

$wp_customize->add_control(new Spiko_Toggle_Control($wp_customize, 'home_contact_section_enabled',
                array(
            'label' => esc_html__('Enable Contact on homepage', 'spiko-plus'),
            'type' => 'toggle',
            'section' => 'contact_section',
                )
));

//Contact section title
$wp_customize->add_setting('home_contact_section_title', array(
    'capability' => 'edit_theme_options',
    'default' => esc_html__('Stay in touch with us', 'spiko-plus'),
    'sanitize_callback' => 'spiko_home_page_sanitize_text',
    'transport' => $selective_refresh,
));

$wp_customize->add_control('home_contact_section_title', array(
    'label' => esc_html__('Title', 'spiko-plus'),
    'section' => 'contact_section',
    'type' => 'text',
    'active_callback' => 'spiko_plus_contact_callback'
));

//Background Overlay Color
$wp_customize->add_setting('contact_bg_color', array(
    'sanitize_callback' => 'sanitize_text_field',
    'default' => '#ffffff',
));

$wp_customize->add_control(new Spiko_Plus_Customize_Alpha_Color_Control($wp_customize, 'contact_bg_color', array(
            'label' => esc_html__('Background Color', 'spiko-plus'),
            'palette' => true,
            'active_callback' => 'spiko_plus_contact_callback',
            'section' => 'contact_section')
));


if (class_exists('Spiko_Plus_Repeater')) {
    $wp_customize->add_setting('spiko_plus_contact_detail_content', array());
    $wp_customize->add_control(new Spiko_Plus_Repeater($wp_customize, 'spiko_plus_contact_detail_content', array(
                'label' => esc_html__('Contact content', 'spiko-plus'),
                'section' => 'contact_section',
                'priority' => 10,
                'add_field_label' => esc_html__('Add new Contact', 'spiko-plus'),
                'item_name' => esc_html__('Contact', 'spiko-plus'),
                'customizer_repeater_icon_control' => true,
                'customizer_repeater_title_control' => true,
                'customizer_repeater_text_control' => true,
                'customizer_repeater_image_control' => true,
                'active_callback' => 'spiko_plus_contact_callback'
    )));
}

$wp_customize->selective_refresh->add_partial('home_contact_section_title', array(
    'selector' => '.section-space.contact-detail h2.section-title',
    'settings' => 'home_contact_section_title',
    'render_callback' => 'home_contact_section_title_render_callback'
));

function home_contact_section_title_render_callback() {
    return get_theme_mod('home_contact_section_title');
}