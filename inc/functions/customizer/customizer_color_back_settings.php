<?php

function spiko_plus_color_back_settings_customizer($wp_customize) {

    $selective_refresh = isset($wp_customize->selective_refresh) ? 'postMessage' : 'refresh';

    /* Home Page Panel */
    $wp_customize->add_panel('colors_back_settings', array(
        'priority' => 110,
        'capability' => 'edit_theme_options',
        'title' => esc_html__('Colors & Background', 'spiko-plus'),
    ));

    $wp_customize->add_section("background_image", array(
        'title' => esc_html__('Background Image', 'spiko-plus'),
        'panel' => 'colors_back_settings',
    ));

    $wp_customize->add_section("colors", array(
        'title' => esc_html__('Background Color', 'spiko-plus'),
        'panel' => 'colors_back_settings',
    ));

    /* ------------------------------------------------ */
    /* Header Color Settings */
    $wp_customize->add_section('header_color_settings', array(
        'title' => esc_html__('Header', 'spiko-plus'),
        'panel' => 'colors_back_settings',
    ));
    
    // Header enable/disable 
    $wp_customize->add_setting(
            'header_clr_enable',
            array('capability' => 'edit_theme_options',
                'default' => false,
    ));

    $wp_customize->add_control(
            'header_clr_enable',
            array(
                'type' => 'checkbox',
                'label' => esc_html__('Click here to apply these settings', 'spiko-plus'),
                'section' => 'header_color_settings',
            )
    );

    //Header background color
    $wp_customize->add_setting('header_background_color', array(
        'default' => '#ffffff',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'header_background_color', array(
                'label' => esc_html__('Header Background Color', 'spiko-plus'),
                'description' => esc_html__('This will apply with Header Preset 5, 6, 7', 'spiko-plus'),
                'section' => 'header_color_settings',
                'settings' => 'header_background_color',)
    ));

    class WP_Sitetitle_Color_Customize_Control extends WP_Customize_Control {

        public $type = 'new_menu';

        /**
         * Render the control's content.
         */
        public function render_content() {
            ?>
            <h3><?php esc_attr_e('Site Title', 'spiko-plus'); ?></h3>
            <?php
        }

    }

    $wp_customize->add_setting(
            'site_title_color',
            array(
                'capability' => 'edit_theme_options',
                'sanitize_callback' => 'sanitize_text_field',
            )
    );
    $wp_customize->add_control(new WP_Sitetitle_Color_Customize_Control($wp_customize, 'site_title_color', array(
                'section' => 'header_color_settings',
                'setting' => 'site_title_color',
                    ))
    );

    //Site title link color
    $wp_customize->add_setting('site_title_link_color', array(
        'default' => '#ffffff',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'site_title_link_color', array(
                'label' => esc_html__('Link Color', 'spiko-plus'),
                'section' => 'header_color_settings',
                'settings' => 'site_title_link_color',)
    ));

    //Site title link hover color
    $wp_customize->add_setting('site_title_link_hover_color', array(
        'default' => '#35ac39',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'site_title_link_hover_color', array(
                'label' => esc_html__('Link Hover Color', 'spiko-plus'),
                'section' => 'header_color_settings',
                'settings' => 'site_title_link_hover_color',)
    ));

    class WP_Sitetagline_Color_Customize_Control extends WP_Customize_Control {

        public $type = 'new_menu';

        /**
         * Render the control's content.
         */
        public function render_content() {
            ?>
            <h3><?php esc_attr_e('Site Tagline', 'spiko-plus'); ?></h3>
            <?php
        }

    }

    $wp_customize->add_setting(
            'site_tagline_color',
            array(
                'capability' => 'edit_theme_options',
                'sanitize_callback' => 'sanitize_text_field',
            )
    );
    $wp_customize->add_control(new WP_Sitetagline_Color_Customize_Control($wp_customize, 'site_tagline_color', array(
                'section' => 'header_color_settings',
                'setting' => 'site_tagline_color',
                    ))
    );

    //Site tagline text color
    $wp_customize->add_setting('site_tagline_text_color', array(
        'default' => '#acacac',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'site_tagline_text_color', array(
                'label' => esc_html__('Text Color', 'spiko-plus'),
                'section' => 'header_color_settings',
                'settings' => 'site_tagline_text_color',)
    ));
    
    /* ------------------------------------------------ */
    /* Sticky Header Color Settings */
    $wp_customize->add_section('sticky_header_color_settings', array(
        'title' => esc_html__('Sticky Header', 'spiko-plus'),
        'panel' => 'colors_back_settings',
    ));


    // Sticky Header enable/disable 
    $wp_customize->add_setting(
            'sticky_header_clr_enable',
            array('capability' => 'edit_theme_options',
                'default' => false,
    ));

    $wp_customize->add_control(
            'sticky_header_clr_enable',
            array(
                'type' => 'checkbox',
                'label' => esc_html__('Click here to apply these settings', 'spiko-plus'),
                'section' => 'sticky_header_color_settings',
            )
    );

    //Sticky Header Title Color
    $wp_customize->add_setting('sticky_header_title_color', array(
        'default' => '#21202e',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sticky_header_title_color', array(
                'label' => esc_html__('Site Title Color', 'spiko-plus'),
                'section' => 'sticky_header_color_settings',
                'settings' => 'sticky_header_title_color',)
    ));

    //Sticky Header SubTitlte Color
    $wp_customize->add_setting('sticky_header_subtitle_color', array(
        'default' => '#21202e',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sticky_header_subtitle_color', array(
                'label' => esc_html__('Site Tagline Color', 'spiko-plus'),
                'section' => 'sticky_header_color_settings',
                'settings' => 'sticky_header_subtitle_color',)
    ));

    class WP_Sticky_Header_Mainmenus_Color_Customize_Control extends WP_Customize_Control {

        public $type = 'new_menu';

        /**
         * Render the control's content.
         */
        public function render_content() {
            ?>
            <h3><?php esc_attr_e('Menus', 'spiko-plus'); ?></h3>
            <?php
        }

    }

    $wp_customize->add_setting(
            'sticky_header_mainmenus_color',
            array(
                'capability' => 'edit_theme_options',
                'sanitize_callback' => 'sanitize_text_field',
            )
    );
    $wp_customize->add_control(new WP_Sticky_Header_Mainmenus_Color_Customize_Control($wp_customize, 'sticky_header_mainmenus_color', array(
                'section' => 'sticky_header_color_settings',
                'setting' => 'sticky_header_mainmenus_color',
                    ))
    );
    //Sticky Header Backgound Color
    $wp_customize->add_setting('sticky_header_bg_color', array(
        'default' => '#f8f8f8',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sticky_header_bg_color', array(
                'label' => esc_html__('Backgound Color', 'spiko-plus'),
                'section' => 'sticky_header_color_settings',
                'settings' => 'sticky_header_bg_color',)
    ));

    //Sticky Header Menus text/link color
    $wp_customize->add_setting('sticky_header_menus_link_color', array(
        'default' => '#21202e',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sticky_header_menus_link_color', array(
                'label' => esc_html__('Text/Link Color', 'spiko-plus'),
                'section' => 'sticky_header_color_settings',
                'settings' => 'sticky_header_menus_link_color',)
    ));

    //Sticky Header Menus text/link hover color
    $wp_customize->add_setting('sticky_header_menus_link_hover_color', array(
        'default' => '#35ac39',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sticky_header_menus_link_hover_color', array(
                'label' => esc_html__('Link Hover Color', 'spiko-plus'),
                'section' => 'sticky_header_color_settings',
                'settings' => 'sticky_header_menus_link_hover_color',)
    ));

    //Sticky Header Menus text/link active color
    $wp_customize->add_setting('sticky_header_menus_link_active_color', array(
        'default' => '#ffffff',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sticky_header_menus_link_active_color', array(
                'label' => esc_html__('Active Link Color', 'spiko-plus'),
                'section' => 'sticky_header_color_settings',
                'settings' => 'sticky_header_menus_link_active_color',)
    ));

    class WP_Sticky_Header_Submenus_Color_Customize_Control extends WP_Customize_Control {

        public $type = 'new_menu';

        /**
         * Render the control's content.
         */
        public function render_content() {
            ?>
            <h3><?php esc_attr_e('Submenus', 'spiko-plus'); ?></h3>
            <?php
        }

    }

    $wp_customize->add_setting(
            'sticky_header_submenus_color',
            array(
                'capability' => 'edit_theme_options',
                'sanitize_callback' => 'sanitize_text_field',
            )
    );
    $wp_customize->add_control(new WP_Sticky_Header_Submenus_Color_Customize_Control($wp_customize, 'sticky_header_submenus_color', array(
                'section' => 'sticky_header_color_settings',
                'setting' => 'sticky_header_submenus_color',
                    ))
    );

    //Sticky Header Submenus Background color
    $wp_customize->add_setting('sticky_header_submenus_background_color', array(
        'default' => '#fff ',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sticky_header_submenus_background_color', array(
                'label' => esc_html__('Background Color', 'spiko-plus'),
                'section' => 'sticky_header_color_settings',
                'settings' => 'sticky_header_submenus_background_color',)
    ));

    //Sticky Header Submenus text/link color
    $wp_customize->add_setting('sticky_header_submenus_link_color', array(
        'default' => '#212529',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sticky_header_submenus_link_color', array(
                'label' => esc_html__('Text/Link Color', 'spiko-plus'),
                'section' => 'sticky_header_color_settings',
                'settings' => 'sticky_header_submenus_link_color',)
    ));

    //Sticky Header Submenus text/link hover color
    $wp_customize->add_setting('sticky_header_submenus_link_hover_color', array(
        'default' => '#35ac39',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sticky_header_submenus_link_hover_color', array(
                'label' => esc_html__('Link Hover Color', 'spiko-plus'),
                'section' => 'sticky_header_color_settings',
                'settings' => 'sticky_header_submenus_link_hover_color',)
    ));


    /* ------------------------------------------------ */
    /* Primary Menu Color Settings */
    $wp_customize->add_section('primary_menu_color_settings', array(
        'title' => esc_html__('Primary Menu', 'spiko-plus'),
        'panel' => 'colors_back_settings',
    ));


      // enable/disable 
    $wp_customize->add_setting(
            'apply_menu_clr_enable',
            array('capability' => 'edit_theme_options',
                'default' => false,
    ));

    $wp_customize->add_control(
            'apply_menu_clr_enable',
            array(
                'type' => 'checkbox',
                'label' => esc_html__('Click here to apply these settings', 'spiko-plus'),
                'section' => 'primary_menu_color_settings',
            )
    );
    
    class WP_Menus_Color_Customize_Control extends WP_Customize_Control {

        public $type = 'new_menu';

        /**
         * Render the control's content.
         */
        public function render_content() {
            ?>
            <h3><?php esc_attr_e('Menus', 'spiko-plus'); ?></h3>
            <?php
        }

    }

    $wp_customize->add_setting(
            'menus_color',
            array(
                'capability' => 'edit_theme_options',
                'sanitize_callback' => 'sanitize_text_field',
            )
    );
    $wp_customize->add_control(new WP_Menus_Color_Customize_Control($wp_customize, 'menus_color', array(
                'section' => 'primary_menu_color_settings',
                'setting' => 'menus_color',
                    ))
    );


    //Menus text/link color
    $wp_customize->add_setting('menus_link_color', array(
        'default' => '#ffffff',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'menus_link_color', array(
                'label' => esc_html__('Text/Link Color', 'spiko-plus'),
                'section' => 'primary_menu_color_settings',
                'settings' => 'menus_link_color',)
    ));

    //Menus text/link hover color
    $wp_customize->add_setting('menus_link_hover_color', array(
        'default' => '#35ac39',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'menus_link_hover_color', array(
                'label' => esc_html__('Link Hover Color', 'spiko-plus'),
                'section' => 'primary_menu_color_settings',
                'settings' => 'menus_link_hover_color',)
    ));

    //Menus text/link active color
    $wp_customize->add_setting('menus_link_active_color', array(
        'default' => '#ffffff',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'menus_link_active_color', array(
                'label' => esc_html__('Active Link Color', 'spiko-plus'),
                'section' => 'primary_menu_color_settings',
                'settings' => 'menus_link_active_color',)
    ));

    class WP_Submenus_Color_Customize_Control extends WP_Customize_Control {

        public $type = 'new_menu';

        /**
         * Render the control's content.
         */
        public function render_content() {
            ?>
            <h3><?php esc_attr_e('Submenus', 'spiko-plus'); ?></h3>
            <?php
        }

    }

    $wp_customize->add_setting(
            'submenus_color',
            array(
                'capability' => 'edit_theme_options',
                'sanitize_callback' => 'sanitize_text_field',
            )
    );
    $wp_customize->add_control(new WP_Submenus_Color_Customize_Control($wp_customize, 'submenus_color', array(
                'section' => 'primary_menu_color_settings',
                'setting' => 'submenus_color',
                    ))
    );

    //Submenus Background color
    $wp_customize->add_setting('submenus_background_color', array(
        'default' => '#000000',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'submenus_background_color', array(
                'label' => esc_html__('Background Color', 'spiko-plus'),
                'section' => 'primary_menu_color_settings',
                'settings' => 'submenus_background_color',)
    ));

    //Submenus text/link color
    $wp_customize->add_setting('submenus_link_color', array(
        'default' => '#ffffff',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'submenus_link_color', array(
                'label' => esc_html__('Text/Link Color', 'spiko-plus'),
                'section' => 'primary_menu_color_settings',
                'settings' => 'submenus_link_color',)
    ));

    //Submenus text/link hover color
    $wp_customize->add_setting('submenus_link_hover_color', array(
        'default' => '#35ac39',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'submenus_link_hover_color', array(
                'label' => esc_html__('Link Hover Color', 'spiko-plus'),
                'section' => 'primary_menu_color_settings',
                'settings' => 'submenus_link_hover_color',)
    ));
    
/* ------------------------------------------------ */
    //After Menu Button

    /* After Menu Button CLR Settings */
    $wp_customize->add_section( 'after_menu_btn_color_settings', array(
        'title' => esc_html__('After Menu Button', 'spiko-plus'),
        'panel' => 'colors_back_settings',
    ) );        

    // Enable After Menu Button Color Setting
    $wp_customize->add_setting( 'enable_after_menu_btn_clr_setting', array(
            'default' => false,
            'sanitize_callback' => 'sanitize_text_field',
        ) );
        
        $wp_customize->add_control('enable_after_menu_btn_clr_setting', array(
            'label'    => __('Enable to apply below color settings', 'spiko-plus' ),
            'section'  => 'after_menu_btn_color_settings',
            'type' => 'checkbox',
        ) );
    
    //After Menu Button BG color
    $wp_customize->add_setting('after_menu_btn_bg_clr', array(
    'default' => '#000',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );
    
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'after_menu_btn_bg_clr', array(
        'label'      => esc_html__('Background Color', 'spiko-plus' ),
        'section'    => 'after_menu_btn_color_settings',
        'settings'   => 'after_menu_btn_bg_clr',) 
    ) );

    //After Menu Button Text color
    $wp_customize->add_setting('after_menu_btn_txt_clr', array(
    'default' => '#35ac39',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );
    
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'after_menu_btn_txt_clr', array(
        'label'      => esc_html__('Text Color', 'spiko-plus' ),
        'section'    => 'after_menu_btn_color_settings',
        'settings'   => 'after_menu_btn_txt_clr',) 
    ) );

    //After Menu Button Hover color
    $wp_customize->add_setting('after_menu_btn_hover_clr', array(
    'default' => '#35ac39',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );
    
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'after_menu_btn_hover_clr', array(
        'label'      => esc_html__('Button Hover Color', 'spiko-plus' ),
        'section'    => 'after_menu_btn_color_settings',
        'settings'   => 'after_menu_btn_hover_clr',) 
    ) );

    $wp_customize->add_setting('after_menu_btn_text_hover_clr', array(
    'default' => '#fff',
    'sanitize_callback' => 'sanitize_hex_color',
    ) );
    
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'after_menu_btn_text_hover_clr', array(
        'label'      => esc_html__('Button Hover Color', 'spiko-plus' ),
        'section'    => 'after_menu_btn_color_settings',
        'settings'   => 'after_menu_btn_text_hover_clr',) 
    ) );

    /* ------------------------------------------------ */
    /* Banner Color Settings */
    $wp_customize->add_section('banner_color_settings', array(
        'title' => esc_html__('Banner', 'spiko-plus'),
        'panel' => 'colors_back_settings',
    ));

    class WP_Banner_Color_Customize_Control extends WP_Customize_Control {

        public $type = 'new_menu';

        /**
         * Render the control's content.
         */
        public function render_content() {
            ?>
            <h3><?php esc_attr_e('Banner Title', 'spiko-plus'); ?></h3>
            <?php
        }

    }

    $wp_customize->add_setting(
            'banner_title_color',
            array(
                'capability' => 'edit_theme_options',
                'sanitize_callback' => 'sanitize_text_field',
            )
    );
    $wp_customize->add_control(new WP_Banner_Color_Customize_Control($wp_customize, 'banner_title_color', array(
                'section' => 'banner_color_settings',
                'setting' => 'banner_title_color',
                    ))
    );

    //Banner title color
    $wp_customize->add_setting('banner_text_color', array(
        'default' => '#fff',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'banner_text_color', array(
                'label' => esc_html__('Title Color', 'spiko-plus'),
                'section' => 'banner_color_settings',
                'settings' => 'banner_text_color',)
    ));

    class WP_Breadcrumb_Color_Customize_Control extends WP_Customize_Control {

        public $type = 'new_menu';

        /**
         * Render the control's content.
         */
        public function render_content() {
            ?>
            <h3><?php esc_attr_e('Breadcrumb Title', 'spiko-plus'); ?></h3>
            <?php
        }

    }

    // Image overlay
    $wp_customize->add_setting('enable_brd_link_clr_setting', array(
        'default' => false,
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control('enable_brd_link_clr_setting', array(
        'label' => esc_html__('Enable to apply below color settings', 'spiko-plus'),
        'section' => 'banner_color_settings',
        'type' => 'checkbox',
    ));
    $wp_customize->add_setting(
            'breadcrumb_title_color',
            array(
                'capability' => 'edit_theme_options',
                'sanitize_callback' => 'sanitize_text_field',
            )
    );
    $wp_customize->add_control(new WP_Breadcrumb_Color_Customize_Control($wp_customize, 'breadcrumb_title_color', array(
                'section' => 'banner_color_settings',
                'setting' => 'breadcrumb_title_color',
                    ))
    );

    //Breadcrumb title link color
    $wp_customize->add_setting('breadcrumb_title_link_color', array(
        'default' => '#ffffff',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'breadcrumb_title_link_color', array(
                'label' => esc_html__('Link Color', 'spiko-plus'),
                'section' => 'banner_color_settings',
                'settings' => 'breadcrumb_title_link_color',)
    ));

    //Breadcrumb title link hover color
    $wp_customize->add_setting('breadcrumb_title_link_hover_color', array(
        'default' => '#35ac39',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'breadcrumb_title_link_hover_color', array(
                'label' => esc_html__('Link Hover Color', 'spiko-plus'),
                'section' => 'banner_color_settings',
                'settings' => 'breadcrumb_title_link_hover_color',)
    ));


    /* ------------------------------------------------ */
    /* Content Color Settings */
    $wp_customize->add_section('content_color_settings', array(
        'title' => esc_html__('Content', 'spiko-plus'),
        'panel' => 'colors_back_settings',
    ));

    // Content enable/disable 
    $wp_customize->add_setting(
            'content_clr_enable',
            array('capability' => 'edit_theme_options',
                'default' => false,
    ));

    $wp_customize->add_control(
            'content_clr_enable',
            array(
                'type' => 'checkbox',
                'label' => esc_html__('Click here to apply these settings', 'spiko-plus'),
                'section' => 'content_color_settings',
            )
    );

    //H1 color
    $wp_customize->add_setting('h1_color', array(
        'default' => '#0a0a0a',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'h1_color', array(
                'label' => esc_html__('H1 Color', 'spiko-plus'),
                'section' => 'content_color_settings',
                'settings' => 'h1_color',)
    ));

    //H2 color
    $wp_customize->add_setting('h2_color', array(
        'default' => '#0a0a0a',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'h2_color', array(
                'label' => esc_html__('H2 Color', 'spiko-plus'),
                'section' => 'content_color_settings',
                'settings' => 'h2_color',)
    ));

    //H3 color
    $wp_customize->add_setting('h3_color', array(
        'default' => '#0a0a0a',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'h3_color', array(
                'label' => esc_html__('H3 Color', 'spiko-plus'),
                'section' => 'content_color_settings',
                'settings' => 'h3_color',)
    ));

    //H4 color
    $wp_customize->add_setting('h4_color', array(
        'default' => '#0a0a0a',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'h4_color', array(
                'label' => esc_html__('H4 Color', 'spiko-plus'),
                'section' => 'content_color_settings',
                'settings' => 'h4_color',)
    ));

    //H5 color
    $wp_customize->add_setting('h5_color', array(
        'default' => '#0a0a0a',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'h5_color', array(
                'label' => esc_html__('H5 Color', 'spiko-plus'),
                'section' => 'content_color_settings',
                'settings' => 'h5_color',)
    ));

    //H6 color
    $wp_customize->add_setting('h6_color', array(
        'default' => '#0a0a0a',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'h6_color', array(
                'label' => esc_html__('H6 Color', 'spiko-plus'),
                'section' => 'content_color_settings',
                'settings' => 'h6_color',)
    ));

    //P color
    $wp_customize->add_setting('p_color', array(
        'default' => '#888888',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'p_color', array(
                'label' => esc_html__('Paragraph Text Color', 'spiko-plus'),
                'section' => 'content_color_settings',
                'settings' => 'p_color',)
    ));



    /* ------------------------------------------------ */
    /* Slider Color Settings */
    $wp_customize->add_section('home_slider_page_color_settings', array(
        'title' => esc_html__('Slider Section', 'spiko-plus'),
        'panel' => 'colors_back_settings',
    ));

    // enable/disable 
    $wp_customize->add_setting(
            'apply_slider_clr_enable',
            array('capability' => 'edit_theme_options',
                'default' => false,
    ));

    $wp_customize->add_control(
            'apply_slider_clr_enable',
            array(
                'type' => 'checkbox',
                'label' => esc_html__('Click here to apply these settings', 'spiko-plus'),
                'section' => 'home_slider_page_color_settings',
            )
    );

    //Slider Subtitle color
    $wp_customize->add_setting('home_slider_subtitle_color', array(
        'default' => '#ffffff',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'home_slider_subtitle_color', array(
                'label' => esc_html__('Slider Sub Title Color', 'spiko-plus'),
                'section' => 'home_slider_page_color_settings',
                'settings' => 'home_slider_subtitle_color',)
    ));
    //Slider title color
    $wp_customize->add_setting('home_slider_title_color', array(
        'default' => '#ffffff',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'home_slider_title_color', array(
                'label' => esc_html__('Slider Title Color', 'spiko-plus'),
                'section' => 'home_slider_page_color_settings',
                'settings' => 'home_slider_title_color',)
    ));

    //Slider Description color
    $wp_customize->add_setting('home_slider_description_color', array(
        'default' => '#ffffff',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'home_slider_description_color', array(
                'label' => esc_html__('Slider Description Color', 'spiko-plus'),
                'section' => 'home_slider_page_color_settings',
                'settings' => 'home_slider_description_color',)
    ));

    //SLider Button 1 color
    $wp_customize->add_setting('slider_btn1_color', array(
        'default' => '#35ac39',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'slider_btn1_color', array(
                'label' => esc_html__('Button 1 Color', 'spiko-plus'),
                'section' => 'home_slider_page_color_settings',
                'settings' => 'slider_btn1_color',)
    ));

    //SLider Button 1 Hover color
    $wp_customize->add_setting('slider_btn1_hover_color', array(
        'default' => '#ffffff',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'slider_btn1_hover_color', array(
                'label' => esc_html__('Button 1 Hover Color', 'spiko-plus'),
                'section' => 'home_slider_page_color_settings',
                'settings' => 'slider_btn1_hover_color',)
    ));

    // Button 2 color enable/disable 
    $wp_customize->add_setting(
            'slider_btn2_color_enable',
            array('capability' => 'edit_theme_options',
                'default' => false,
    ));

    $wp_customize->add_control(
            'slider_btn2_color_enable',
            array(
                'type' => 'checkbox',
                'label' => esc_html__('Click here to apply these settings', 'spiko-plus'),
                'section' => 'home_slider_page_color_settings',
            )
    );

    //SLider Button 2 color
    $wp_customize->add_setting('slider_btn2_color', array(
        'default' => '#35ac39',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'slider_btn2_color', array(
                'label' => esc_html__('Button 2 Color', 'spiko-plus'),
                'section' => 'home_slider_page_color_settings',
                'settings' => 'slider_btn2_color',)
    ));

    //SLider Button 2 Hover color
    $wp_customize->add_setting('slider_btn2_hover_color', array(
        'default' => '#35ac39',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'slider_btn2_hover_color', array(
                'label' => esc_html__('Button 2 Hover Color', 'spiko-plus'),
                'section' => 'home_slider_page_color_settings',
                'settings' => 'slider_btn2_hover_color',)
    ));

    
    $wp_customize->add_setting(
            'spiko_scroll_iconhover_color', array(
        'capability' => 'edit_theme_options',
        'default' => '#00BFFF'
    ));

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    $wp_customize,
                    'spiko_plus_scroll_iconhover_color',
                    array(
                'label' => esc_html__('Icon Hover color', 'spiko-plus'),
                'section' => 'scrolltotop_color_setting_section',
                'settings' => 'spiko_plus_scroll_iconhover_color',
    )));
    /* Scroll Color Settings */

    /* ------------------------------------------------ */
    /* Testimonial Color Settings */
    $wp_customize->add_section('testimonial_page_color_settings', array(
        'title' => esc_html__('Testimonial Section', 'spiko-plus'),
        'panel' => 'colors_back_settings',
    ));

    // enable/disable 
    $wp_customize->add_setting(
            'apply_testimonial_clr_enable',
            array('capability' => 'edit_theme_options',
                'default' => false,
    ));

    $wp_customize->add_control(
            'apply_testimonial_clr_enable',
            array(
                'type' => 'checkbox',
                'label' => esc_html__('Enable these color settings', 'spiko-plus'),
                'section' => 'testimonial_page_color_settings',
            )
    );

    //Testimonial title color
    $wp_customize->add_setting('home_testi_title_color', array(
        'default' => '#333333',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'home_testi_title_color', array(
                'label' => esc_html__('Testimonial Title', 'spiko-plus'),
                'section' => 'testimonial_page_color_settings',
                'settings' => 'home_testi_title_color',)
    ));

    //Testimonial Subtitle color
    $wp_customize->add_setting('home_testi_subtitle_color', array(
        'default' => '#727272',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'home_testi_subtitle_color', array(
                'label' => esc_html__('Testimonial Subtitle', 'spiko-plus'),
                'section' => 'testimonial_page_color_settings',
                'settings' => 'home_testi_subtitle_color',)
    ));



    //Testimonial Description color
    $wp_customize->add_setting('testimonial_description_color', array(
        'default' => '#727272',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'testimonial_description_color', array(
                'label' => esc_html__('Testimonial Description', 'spiko-plus'),
                'section' => 'testimonial_page_color_settings',
                'settings' => 'testimonial_description_color',)
    ));

    //Testimonial Client Name Hover color
    $wp_customize->add_setting('testi_clients_name_color', array(
        'default' => '#21202e',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'testi_clients_name_color', array(
                'label' => esc_html__('Testimonial Name', 'spiko-plus'),
                'section' => 'testimonial_page_color_settings',
                'settings' => 'testi_clients_name_color',)
    ));

    //Testimonial Designation color
    $wp_customize->add_setting('testi_clients_designation_color', array(
        'default' => '#727272',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'testi_clients_designation_color', array(
                'label' => esc_html__('Testimonial Designation', 'spiko-plus'),
                'section' => 'testimonial_page_color_settings',
                'settings' => 'testi_clients_designation_color',)
    ));

    /* ------------------------------------------------ */


    /* ------------------------------------------------ */
    /* CTA 2 Color Settings */
    $wp_customize->add_section('cta_page2_color_settings', array(
        'title' => esc_html__('CTA Section', 'spiko-plus'),
        'panel' => 'colors_back_settings',
    ));

    // enable/disable 
    $wp_customize->add_setting(
            'apply_cta2_clr_enable',
            array('capability' => 'edit_theme_options',
                'default' => false,
    ));

    $wp_customize->add_control(
            'apply_cta2_clr_enable',
            array(
                'type' => 'checkbox',
                'label' => esc_html__('Enable these color settings', 'spiko-plus'),
                'section' => 'cta_page2_color_settings',
            )
    );

    //CTA 2 title color
    $wp_customize->add_setting('home_cta2_title_color', array(
        'default' => '#ffffff',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'home_cta2_title_color', array(
                'label' => esc_html__('CTA Title', 'spiko-plus'),
                'section' => 'cta_page2_color_settings',
                'settings' => 'home_cta2_title_color',)
    ));

    //CTA2 Subtitle color
    $wp_customize->add_setting('home_cta2_subtitle_color', array(
        'default' => '#ffffff',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'home_cta2_subtitle_color', array(
                'label' => esc_html__('CTA Subtitle', 'spiko-plus'),
                'section' => 'cta_page2_color_settings',
                'settings' => 'home_cta2_subtitle_color',)
    ));

    //CTA2 Button 1 color
    $wp_customize->add_setting('home_cta2_btn1_color', array(
        'default' => '#35ac39',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'home_cta2_btn1_color', array(
                'label' => esc_html__('Button 1 Color', 'spiko-plus'),
                'section' => 'cta_page2_color_settings',
                'settings' => 'home_cta2_btn1_color',)
    ));

   //CTA2 Button 1 Hover color
    $wp_customize->add_setting('home_cta2_btn1_hover_color', array(
        'default' => '#ffffff',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'home_cta2_btn1_hover_color', array(
                'label' => esc_html__('Button 1 Hover Color', 'spiko-plus'),
                'section' => 'cta_page2_color_settings',
                'settings' => 'home_cta2_btn1_hover_color',)
    ));

    // Button 2 color enable/disable 
    $wp_customize->add_setting(
            'home_cta2_btn2_color_enable',
            array('capability' => 'edit_theme_options',
                'default' => false,
    ));

    $wp_customize->add_control(
            'home_cta2_btn2_color_enable',
            array(
                'type' => 'checkbox',
                'label' => esc_html__('Click here to apply below settings', 'spiko-plus'),
                'section' => 'cta_page2_color_settings',
            )
    );

    //CTA2 Button 2 color
    $wp_customize->add_setting('home_cta2_btn2_color', array(
        'default' => '#35ac39',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'home_cta2_btn2_color', array(
                'label' => esc_html__('Button 2 Color', 'spiko-plus'),
                'section' => 'cta_page2_color_settings',
                'settings' => 'home_cta2_btn2_color',)
    ));

    //CTA2 Button 2 Hover color
    $wp_customize->add_setting('home_cta2_btn2_hover_color', array(
        'default' => '#35ac39',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'home_cta2_btn2_hover_color', array(
                'label' => esc_html__('Button 2 Hover Color', 'spiko-plus'),
                'section' => 'cta_page2_color_settings',
                'settings' => 'home_cta2_btn2_hover_color',)
    ));
    /* ------------------------------------------------ */
    /* CTA 2 Color Settings END */




    /* ------------------------------------------------ */
    /* FunFact Color Settings */
    $wp_customize->add_section('funfact_color_settings', array(
        'title' => esc_html__('Fun Fact Section', 'spiko-plus'),
        'panel' => 'colors_back_settings',
    ));

    // enable/disable 
    $wp_customize->add_setting(
            'apply_funfact_clr_enable',
            array('capability' => 'edit_theme_options',
                'default' => false,
    ));

    $wp_customize->add_control(
            'apply_funfact_clr_enable',
            array(
                'type' => 'checkbox',
                'label' => esc_html__('Enable these color settings', 'spiko-plus'),
                'section' => 'funfact_color_settings',
            )
    );
    //    Funfact Count color
    $wp_customize->add_setting('funfact_count_color', array(
        'default' => '#ffffff',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'funfact_count_color', array(
                'label' => esc_html__('Fun Facts Title Color', 'spiko-plus'),
                'section' => 'funfact_color_settings',
                'settings' => 'funfact_count_color',)
    ));
    //    Funfact Count color
    $wp_customize->add_setting('funfact_count_desc_color', array(
        'default' => '#ffffff',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'funfact_count_desc_color', array(
                'label' => esc_html__('Fun Facts Subtitle Color', 'spiko-plus'),
                'section' => 'funfact_color_settings',
                'settings' => 'funfact_count_desc_color',)
    ));


    /* ------------------------------------------------ */
    /* FunFact Color Settings END */


    /* ------------------------------------------------ */
    /* Blog Page/Archive Color Settings */
    $wp_customize->add_section('blog_post_page_color_settings', array(
        'title' => esc_html__('Blog Page/Archive', 'spiko-plus'),
        'panel' => 'colors_back_settings',
    ));


    // enable/disable Callout section from service page
    $wp_customize->add_setting(
            'apply_blg_clr_enable',
            array('capability' => 'edit_theme_options',
                'default' => false,
    ));

    $wp_customize->add_control(
            'apply_blg_clr_enable',
            array(
                'type' => 'checkbox',
                'label' => esc_html__('Enable these color settings', 'spiko-plus'),
                'section' => 'blog_post_page_color_settings',
            )
    );


    //Blog Post/Page title color
    $wp_customize->add_setting('blog_post_page_title_color', array(
        'default' => '#333333',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'blog_post_page_title_color', array(
                'label' => esc_html__('Title Color', 'spiko-plus'),
                'section' => 'blog_post_page_color_settings',
                'settings' => 'blog_post_page_title_color',)
    ));

    //Blog Post/Page title hover color
    $wp_customize->add_setting('blog_post_page_title_hover_color', array(
        'default' => '#35ac39',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'blog_post_page_title_hover_color', array(
                'label' => esc_html__('Title Hover Color', 'spiko-plus'),
                'section' => 'blog_post_page_color_settings',
                'settings' => 'blog_post_page_title_hover_color',)
    ));

    //Blog Post Meta link color
    $wp_customize->add_setting('blog_post_page_meta_link_color', array(
        'default' => '#727272',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'blog_post_page_meta_link_color', array(
                'label' => esc_html__('Meta Link Color', 'spiko-plus'),
                'section' => 'blog_post_page_color_settings',
                'settings' => 'blog_post_page_meta_link_color',)
    ));

    //Blog Post Meta link hover color
    $wp_customize->add_setting('blog_post_page_meta_link_hover_color', array(
        'default' => '#35ac39',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'blog_post_page_meta_link_hover_color', array(
                'label' => esc_html__('Meta Link Hover Color', 'spiko-plus'),
                'section' => 'blog_post_page_color_settings',
                'settings' => 'blog_post_page_meta_link_hover_color',)
    ));


    /* ------------------------------------------------ */
    /* Single Post/Page Color Settings */
    $wp_customize->add_section('single_post_page_color_settings', array(
        'title' => esc_html__('Single Post', 'spiko-plus'),
        'panel' => 'colors_back_settings',
    ));

    // enable/disable Callout section from service page
    $wp_customize->add_setting(
            'apply_blg_single_clr_enable',
            array('capability' => 'edit_theme_options',
                'default' => false,
    ));

    $wp_customize->add_control(
            'apply_blg_single_clr_enable',
            array(
                'type' => 'checkbox',
                'label' => esc_html__('Enable these color settings', 'spiko-plus'),
                'section' => 'single_post_page_color_settings',
            )
    );

    //Single Post/Page title color
    $wp_customize->add_setting('single_post_page_title_color', array(
        'default' => '#333333',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'single_post_page_title_color', array(
                'label' => esc_html__('Title Color', 'spiko-plus'),
                'section' => 'single_post_page_color_settings',
                'settings' => 'single_post_page_title_color',)
    ));

    //Single Post Meta link color
    $wp_customize->add_setting('single_post_page_meta_link_color', array(
        'default' => '#727272',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'single_post_page_meta_link_color', array(
                'label' => esc_html__('Meta Link Color', 'spiko-plus'),
                'section' => 'single_post_page_color_settings',
                'settings' => 'single_post_page_meta_link_color',)
    ));

    //Single Post Meta link hover color
    $wp_customize->add_setting('single_post_page_meta_link_hover_color', array(
        'default' => '#35ac39',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'single_post_page_meta_link_hover_color', array(
                'label' => esc_html__('Meta Link Hover Color', 'spiko-plus'),
                'section' => 'single_post_page_color_settings',
                'settings' => 'single_post_page_meta_link_hover_color',)
    ));


    /* ------------------------------------------------ */
    /* Sidebar Widget Color Settings */
    $wp_customize->add_section('sidebar_widget_color_settings', array(
        'title' => esc_html__('Sidebar', 'spiko-plus'),
        'panel' => 'colors_back_settings',
    ));

    // enable/disable Callout section from service page
    $wp_customize->add_setting(
            'apply_sibar_link_hover_clr_enable',
            array('capability' => 'edit_theme_options',
                'default' => false,
    ));

    $wp_customize->add_control(
            'apply_sibar_link_hover_clr_enable',
            array(
                'type' => 'checkbox',
                'label' => esc_html__('Enable these color settings', 'spiko-plus'),
                'section' => 'sidebar_widget_color_settings',
            )
    );

    //Sidebar widget title color
    $wp_customize->add_setting('sidebar_widget_title_color', array(
        'default' => '#35ac39',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sidebar_widget_title_color', array(
                'label' => esc_html__('Title Color', 'spiko-plus'),
                'section' => 'sidebar_widget_color_settings',
                'settings' => 'sidebar_widget_title_color',)
    ));

    //Sidebar widget text color
    $wp_customize->add_setting('sidebar_widget_text_color', array(
        'default' => '#727272',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sidebar_widget_text_color', array(
                'label' => esc_html__('Text Color', 'spiko-plus'),
                'section' => 'sidebar_widget_color_settings',
                'settings' => 'sidebar_widget_text_color',)
    ));

    //Sidebar widget link color
    $wp_customize->add_setting('sidebar_widget_link_color', array(
        'default' => '#0a0a0a',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sidebar_widget_link_color', array(
                'label' => esc_html__('Link Color', 'spiko-plus'),
                'section' => 'sidebar_widget_color_settings',
                'settings' => 'sidebar_widget_link_color',)
    ));

    //Sidebar widget link hover color
    $wp_customize->add_setting('sidebar_widget_link_hover_color', array(
        'default' => '#35ac39',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'sidebar_widget_link_hover_color', array(
                'label' => esc_html__('Link Hover Color', 'spiko-plus'),
                'section' => 'sidebar_widget_color_settings',
                'settings' => 'sidebar_widget_link_hover_color',)
    ));


    /* ------------------------------------------------ */
    /* Footer Widget Color Settings */
    $wp_customize->add_section('footer_widget_color_settings', array(
        'title' => esc_html__('Footer Widget', 'spiko-plus'),
        'panel' => 'colors_back_settings',
    ));

    // enable/disable Callout section from service page
    $wp_customize->add_setting(
            'apply_ftrsibar_link_hover_clr_enable',
            array('capability' => 'edit_theme_options',
                'default' => false,
    ));

    $wp_customize->add_control(
            'apply_ftrsibar_link_hover_clr_enable',
            array(
                'type' => 'checkbox',
                'label' => esc_html__('Enable these color settings', 'spiko-plus'),
                'section' => 'footer_widget_color_settings',
            )
    );

    //Footer widget background color
    $wp_customize->add_setting('footer_widget_background_color', array(
        'default' => '#21202e',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'footer_widget_background_color', array(
                'label' => esc_html__('Background Color', 'spiko-plus'),
                'section' => 'footer_widget_color_settings',
                'settings' => 'footer_widget_background_color',)
    ));

    //Footer widget title color
    $wp_customize->add_setting('footer_widget_title_color', array(
        'default' => '#fff',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'footer_widget_title_color', array(
                'label' => esc_html__('Title Color', 'spiko-plus'),
                'section' => 'footer_widget_color_settings',
                'settings' => 'footer_widget_title_color',)
    ));

    //Footer widget text color
    $wp_customize->add_setting('footer_widget_text_color', array(
        'default' => '#fff',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'footer_widget_text_color', array(
                'label' => esc_html__('Text Color', 'spiko-plus'),
                'section' => 'footer_widget_color_settings',
                'settings' => 'footer_widget_text_color',)
    ));

    //Footer widget link color
    $wp_customize->add_setting('footer_widget_link_color', array(
        'default' => '#ffffff',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'footer_widget_link_color', array(
                'label' => esc_html__('Link Color', 'spiko-plus'),
                'section' => 'footer_widget_color_settings',
                'settings' => 'footer_widget_link_color',)
    ));

    //Footer widget link hover color
    $wp_customize->add_setting('footer_widget_link_hover_color', array(
        'default' => '#35ac39',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'footer_widget_link_hover_color', array(
                'label' => esc_html__('Link Hover Color', 'spiko-plus'),
                'section' => 'footer_widget_color_settings',
                'settings' => 'footer_widget_link_hover_color',)
    ));


    /* ------------------------------------------------ */
    /* Footer Bar Color Settings */
    $wp_customize->add_section('footer_bar_color_settings', array(
        'title' => esc_html__('Footer Bar', 'spiko-plus'),
        'panel' => 'colors_back_settings',
    ));

    // enable/disable 
    $wp_customize->add_setting(
            'apply_foot_hover_anchor_enable',
            array('capability' => 'edit_theme_options',
                'default' => false,
    ));

    $wp_customize->add_control(
            'apply_foot_hover_anchor_enable',
            array(
                'type' => 'checkbox',
                'label' => esc_html__('Enable these color settings', 'spiko-plus'),
                'section' => 'footer_bar_color_settings',
            )
    );

    //Footer bar background color
    $wp_customize->add_setting('footer_bar_background_color', array(
        'default' => '#000000',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'footer_bar_background_color', array(
                'label' => esc_html__('Background Color', 'spiko-plus'),
                'section' => 'footer_bar_color_settings',
                'settings' => 'footer_bar_background_color',)
    ));
    
    	//Footer bar widget title color
	$wp_customize->add_setting('advance_footer_bar_title_color', array(
	'default' => '#fff',
	'sanitize_callback' => 'sanitize_hex_color',
	) );
	
	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize,'advance_footer_bar_title_color', array(
		'label'      => esc_html__('Title Color', 'spiko-plus' ),
		'section'    => 'footer_bar_color_settings',
		'settings'   => 'advance_footer_bar_title_color',) 
	) );

    //Footer bar text color
    $wp_customize->add_setting('footer_bar_text_color', array(
        'default' => '#ffffff',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'footer_bar_text_color', array(
                'label' => esc_html__('Text Color', 'spiko-plus'),
                'section' => 'footer_bar_color_settings',
                'settings' => 'footer_bar_text_color',)
    ));

    //Footer bar link color
    $wp_customize->add_setting('footer_bar_link_color', array(
        'default' => '#ffffff',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'footer_bar_link_color', array(
                'label' => esc_html__('Link Color', 'spiko-plus'),
                'section' => 'footer_bar_color_settings',
                'settings' => 'footer_bar_link_color',)
    ));



    //Footer bar link hover color
    $wp_customize->add_setting('footer_bar_link_hover_color', array(
        'default' => '#35ac39',
        'sanitize_callback' => 'sanitize_hex_color',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'footer_bar_link_hover_color', array(
                'label' => esc_html__('Link Hover Color', 'spiko-plus'),
                'section' => 'footer_bar_color_settings',
                'settings' => 'footer_bar_link_hover_color',)
    ));
}

add_action('customize_register', 'spiko_plus_color_back_settings_customizer');