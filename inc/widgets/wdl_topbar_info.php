<?php
//error_reporting(0);

// Register and load the widget
function spiko_header_topbar_info_widget() {
    register_widget('spiko_header_topbar_info_widget');
}

add_action('widgets_init', 'spiko_header_topbar_info_widget');

// Creating the widget
class spiko_header_topbar_info_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
                'spiko_header_topbar_info_widget', // Base ID
                esc_html__('Spiko: Header info widget', 'spiko-plus' ), // Widget Name
                array(
                    'classname' => 'spiko_header_topbar_info_widget',
                    'phone_number' => esc_html__('Topbar header info widget.', 'spiko-plus' ),
                ),
                array(
                    'width' => 600,
                )
        );
    }

    public function widget($args, $instance) {

        //echo $args['before_widget']; 
       echo $args['before_widget'];
        ?>
        <ul class="head-contact-info">
            <li class="phone">
                <?php if (!empty($instance['spiko_phone_icon'])) { ?>
                    <i class="fa <?php echo esc_attr($instance['spiko_phone_icon']); ?>"></i>
                <?php } else { ?> 
                    <i class="fa-solid fa-phone"></i>
                <?php } ?>	
                <?php
                if (!empty($instance['phone_number'])) {
                    echo esc_html($instance['phone_number']);
                } else {
                    echo esc_html($instance['phone_number']);
                }
                ?>
            </li>
            <li class="envelope">
                <?php if (!empty($instance['spiko_email_icon'])) { ?>
                    <i class="fa <?php echo esc_attr($instance['spiko_email_icon']); ?>"></i>
                <?php } else { ?> 
                    <i class="fa-solid fa-envelope"></i>
                <?php } ?>	
                <a href="mailto:abc@example.com"> <?php
                    if (!empty($instance['spiko_email_id'])) {
                        echo esc_html($instance['spiko_email_id']);
                    }
                    ?></a>
            </li>
            <li class="address-info">
                <?php if (!empty($instance['spiko_location_icon'])) { ?>
                    <i class="fa <?php echo esc_attr($instance['spiko_location_icon']); ?>"></i>
                <?php } else { ?> 
                    <i class="fa-solid fa-location-dot"></i>
                <?php } ?>	
                <?php
                if (!empty($instance['spiko_location_text'])) {
                    echo esc_html($instance['spiko_location_text']);
                }
                ?>
            </li>
        </ul>
        <?php
        echo $args['after_widget'];
    }

    // Widget Backend
    public function form($instance) {

        if (isset($instance['spiko_phone_icon'])) {
            $spiko_phone_icon = $instance['spiko_phone_icon'];
        } else {
            $spiko_phone_icon = esc_html__('fa-solid fa-phone', 'spiko-plus' );
        }

        if (isset($instance['phone_number'])) {
            $phone_number = $instance['phone_number'];
        } else {
            $phone_number = esc_html__('+99 999-999-9999', 'spiko-plus' );
        }

        if (isset($instance['spiko_email_icon'])) {
            $spiko_email_icon = $instance['spiko_email_icon'];
        } else {
            $spiko_email_icon = esc_html__('fa-solid fa-envelope', 'spiko-plus' );
        }

        if (isset($instance['spiko_email_id'])) {
            $spiko_email_id = $instance['spiko_email_id'];
        } else {
            $spiko_email_id = esc_html__('abc@example.com', 'spiko-plus' );
        }

        if (isset($instance['spiko_location_icon'])) {
            $spiko_location_icon = $instance['spiko_location_icon'];
        } else {
            $spiko_location_icon = esc_html__('fa-solid fa-location-dot', 'spiko-plus' );
        }

        if (isset($instance['spiko_location_text'])) {
            $spiko_location_text = $instance['spiko_location_text'];
        } else {
            $spiko_location_text = esc_html__('9999 Nemo Enim Ipsam, Voluptatem', 'spiko-plus' );
        }

        // Widget admin form
        ?>

        <label for="<?php echo esc_attr($this->get_field_id('spiko_phone_icon')); ?>"><?php esc_html_e('Font Awesome icon', 'spiko-plus' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('spiko_phone_icon')); ?>" name="<?php echo esc_attr($this->get_field_name('spiko_phone_icon')); ?>" type="text" value="<?php
        if ($spiko_phone_icon)
            echo esc_attr($spiko_phone_icon);
        else
            echo esc_attr('fa fa-phone', 'spiko-plus' );
        ?>" />
        <span><?php esc_html_e('Link to get Font Awesome icons', 'spiko-plus' ); ?><a href="<?php echo esc_url('https://fontawesome.com/v4/icons/'); ?>" target="_blank" ><?php esc_html_e('fa-icon', 'spiko-plus' ); ?></a></span>
        <br><br>

        <label for="<?php echo esc_attr($this->get_field_id('phone_number')); ?>"><?php esc_html_e('Phone', 'spiko-plus' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('phone_number')); ?>" name="<?php echo esc_attr($this->get_field_name('phone_number')); ?>" type="text" value="<?php
        if ($phone_number)
            echo esc_attr($phone_number);
        else
            esc_html_e('+99 999-999-9999', 'spiko-plus' );
        ?>" /><br><br>

        <label for="<?php echo esc_attr($this->get_field_id('spiko_email_icon')); ?>"><?php esc_html_e('Font Awesome icon', 'spiko-plus' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('spiko_email_icon')); ?>" name="<?php echo esc_attr($this->get_field_name('spiko_email_icon')); ?>" type="text" value="<?php
        if ($spiko_email_icon)
            echo esc_attr($spiko_email_icon);
        else
            echo esc_attr('fa fa-phone','spiko-plus' );
        ?>" />
        <span><?php esc_html_e('Link to get Font Awesome icons', 'spiko-plus' ); ?><a href="<?php echo esc_url('https://fontawesome.com/v4/icons/'); ?>" target="_blank" ><?php esc_html_e('fa-icon', 'spiko-plus' ); ?></a></span><br><br>

        <label for="<?php echo esc_attr($this->get_field_id('spiko_email_id')); ?>"><?php esc_html_e('Email', 'spiko-plus' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('spiko_email_id')); ?>" name="<?php echo esc_attr($this->get_field_name('spiko_email_id')); ?>" type="text" value="<?php
        if ($spiko_email_id)
            echo esc_attr($spiko_email_id);
        else
            esc_html_e('abc@example.com', 'spiko-plus' );
        ?>" /><br><br>

        <label for="<?php echo esc_attr($this->get_field_id('spiko_location_icon')); ?>"><?php esc_html_e('Font Awesome icon', 'spiko-plus' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('spiko_location_icon')); ?>" name="<?php echo esc_attr($this->get_field_name('spiko_location_icon')); ?>" type="text" value="<?php
        if ($spiko_location_icon)
            echo esc_attr($spiko_location_icon);
        else
            echo esc_attr('fa fa-map-marker','spiko-plus' );
        ?>" />
        <span><?php esc_html_e('Link to get Font Awesome icons', 'spiko-plus' ); ?><a href="<?php echo esc_url('https://fontawesome.com/v4/icons/'); ?>" target="_blank" ><?php esc_html_e('fa-icon', 'spiko-plus' ); ?></a></span><br><br>

        <label for="<?php echo esc_attr($this->get_field_id('spiko_location_text')); ?>"><?php esc_html_e('Address', 'spiko-plus' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('spiko_location_text')); ?>" name="<?php echo esc_attr($this->get_field_name('spiko_location_text')); ?>" type="text" value="<?php
               if ($spiko_location_text)
                   echo esc_attr($spiko_location_text);
               else
                   esc_html_e('9999 Nemo Enim Ipsam, Voluptatem', 'spiko-plus' );
               ?>" /><br><br>



        <?php
    }

    // Updating widget replacing old instances with new
    public function update($new_instance, $old_instance) {

        $instance = array();
        $instance['spiko_phone_icon'] = (!empty($new_instance['spiko_phone_icon']) ) ? spiko_sanitize_text($new_instance['spiko_phone_icon']) : '';
        $instance['phone_number'] = (!empty($new_instance['phone_number']) ) ? spiko_sanitize_text($new_instance['phone_number']) : '';
        $instance['spiko_email_icon'] = (!empty($new_instance['spiko_email_icon']) ) ? spiko_sanitize_text($new_instance['spiko_email_icon']) : '';
        $instance['spiko_email_id'] = (!empty($new_instance['spiko_email_id']) ) ? $new_instance['spiko_email_id'] : '';
        $instance['spiko_location_icon'] = (!empty($new_instance['spiko_location_icon']) ) ? spiko_sanitize_text($new_instance['spiko_location_icon']) : '';
        $instance['spiko_location_text'] = (!empty($new_instance['spiko_location_text']) ) ? spiko_sanitize_text($new_instance['spiko_location_text']) : '';

        return $instance;
    }

}